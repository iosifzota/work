#include <iostream>
#include <list>
#include <vector>

int main()
{
    std::cout << "cpp:\n\n";

    std::list<unsigned> l;

    std::vector< std::list<unsigned> > v;

    v.push_back(l);
    std::cout << v.size() << '\n';

    l.push_back(43);

    return 0;
}
