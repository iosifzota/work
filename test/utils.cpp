#include "utils.hh"

#include <string.h>
#include <stdio.h>

static void print_red_err();

#define skip_chars(str, step) \
    for(; off >= 0 && strchr(str, err[off]); off += (step)) { ; }

#define skip_until_chars(str, step) \
    for(; off >= 0 && !strchr(str, err[off]); off += (step)) { ; }

void print_error(const char* err)
{
    printf("\n[ ");
    print_red_err();
    printf(" ] False assertion: ");

    int off, str_b, str_e;

    off = 0;

    /* Go to the end of the string. */
    skip_until_chars("\0", 1);

    /* Skip \0. */
    --off;

    /* Find end of error message. */
    skip_until_chars("\"", -1);

    if (off < 0) {
        printf(err);
        return;
    }

    /* Set the end of error message. */
    str_e = off - 1;

    /* Skip " */
    --off;

    /* Find beginning of error message. */
    skip_until_chars("\"", -1);

    /* Set the beginning of error message. */
    str_b = off + 1;

    /* Skip " */
    --off;

    /* Skip whitespace. */
    skip_chars(" (", -1);

    char cast[] = "!(bool)";
    int cast_index = 6;

    /* Match err[--off] to =cast=. */
    for (; off >= 0 && err[off] == cast[cast_index]; --off, --cast_index) { ; }

    /* If no match return. */
    if (cast_index >= 0) {
        printf(err);
        return;
    }

    /* Skip whitespace. */
    skip_chars(" ", -1);

    if (off < 0 || err[off] != '|') {
        printf(err);
        return;
    }

    /* Skip || */
    off -= 2;

    /* Skip whitespace. */
    skip_chars(" ", -1);

    /* Print the =cond=. */
    for (int i = 0; i <= off; ++i) {
        putchar(err[i]);
    }

    printf(".  // ");

    /* Print error message. */
    for (int i = str_b; i <= str_e; ++i) {
        putchar(err[i]);
    }
}


#define RED     "\x1b[31m"
#define GREEN   "\x1b[32m"
#define YELLOW  "\x1b[33m"
#define BLUE    "\x1b[34m"
#define MAGENTA "\x1b[35m"
#define CYAN    "\x1b[36m"
#define RESET   "\x1b[0m"

#ifdef _WIN32
#include <windows.h>
#endif

void print_red_err()
{
#ifdef _WIN32
    HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
    CONSOLE_SCREEN_BUFFER_INFO consoleInfo;
    WORD saved_attributes;

    /* Save current attributes */
    GetConsoleScreenBufferInfo(hConsole, &consoleInfo);
    saved_attributes = consoleInfo.wAttributes;

    SetConsoleTextAttribute(hConsole, FOREGROUND_RED);
    printf("Err");

    /* Restore original attributes */
    SetConsoleTextAttribute(hConsole, saved_attributes);

#else
    printf(RED "Err" RESET);
#endif
}
