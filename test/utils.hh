#ifndef __utils_hh
#define __utils_hh

#include <vector>
#include <istream>
#include <ostream>
#include <functional>
#include <iostream>

#define nl std::cout << std::endl
#define nll nl; nl;
#define nlll nll; nl;
#define nllll nll; nll;

#define nn(n) for (int i = 0; i < n; ++i) std::cout << std::endl;

/* Assert macro. Fails gracefully. ;) */
#define _req(cond) __req(cond)

#define __req(cond)                                      \
    if (!(cond)) {                                      \
        print_error(#cond);                             \
        std::cerr << "\t@" << __LINE__ << std::endl;    \
        exit(-1);                                       \
    }


#define req(cond) req_(cond)

/* Assert expression. */
#define req_(cond)                                          \
    ((cond) ? (NULL) :                                      \
        (                                                   \
            print_error(#cond),                             \
            std::cerr << "\t@" << __LINE__ << std::endl,    \
            exit(-1),                                       \
            NULL                                            \
        ),                                                  \
        NULL                                                \
    )

/* Used for passing a msg to req. */
#define _msg || !(bool)

void print_error(const char* err);


template<typename T>
void map(std::vector<T>& v, void (*fn)(T&))
{
    for (auto& itr : v) {
        (*fn)(itr);
    }
}

template<typename T>
void map(const std::vector<T>& v, void (*fn)(T&))
{
    for (auto& itr : v) {
        (*fn)(itr);
    }
}

template<class Container, typename T>
void map(const Container& container, std::function<void(const T&)> fn)
{
	for (const auto& itr : container) {
		fn(itr);
	}
}

template<class Container, typename T>
void map(Container& container, std::function<void(T&)> fn)
{
	for (auto& itr : container) {
		fn(itr);
	}
}

template<typename T>
void map(const std::vector<T>& v, void (*fn)(const T&))
{
    for (const auto& itr : v) {
        (*fn)(itr);
    }
}

template<typename T>
std::istream& operator >> (std::istream& in, std::vector<T>& v)
{
    T data;

    while (in >> data) {
        v.push_back(data);
    }

    return in;
}

template<typename T>
std::ostream& operator << (std::ostream& out, const std::vector<T>& v)
{
    for (const auto& itr : v) {
        out << itr << ' ';
    }
    out << '\n';

    return out;
}

template<typename T>
void swap (std::vector<T>& v, unsigned i, unsigned j)
{
    T aux;
    aux = v[i];
    v[i] = v[j];
    v[j] = aux;
}

#endif
