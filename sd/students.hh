#ifndef __students_hh
#define __students_hh

#include "req.hh"


#include <istream>
#include <ostream>
#include <iostream>

class StudentGrades
{
private:
        unsigned m_studentId, m_gradesLength;
        float *m_grades;

        static const unsigned d_id, d_grades_size;
        static float *d_grades;

public:

        StudentGrades(unsigned studentId = d_id, unsigned gradesLength = d_grades_size, float *grades = d_grades);
        StudentGrades(const StudentGrades&);
        ~StudentGrades();


        const StudentGrades& operator += (const StudentGrades&);

        float operator ~ () const;
        
        friend std::istream& operator >> (std::istream&, StudentGrades&);
        friend std::ostream& operator << (std::ostream&, const StudentGrades&);
};

const unsigned StudentGrades::d_id = 0;
const unsigned StudentGrades::d_grades_size = 0;
float * StudentGrades::d_grades = nullptr;

const StudentGrades& StudentGrades::operator += (const StudentGrades& student)
{
        if (m_studentId == student.m_studentId && student.m_gradesLength) {
                float *new_grades;
                unsigned new_gradesLength, i;

                new_gradesLength = m_gradesLength + student.m_gradesLength;
                new_grades = new float[new_gradesLength];

                for (i = 0; i < m_gradesLength; ++i) {
                        new_grades[i] = m_grades[i];
                }

                for (unsigned j = 0; j < student.m_gradesLength; ++j, ++i) {
                        new_grades[i] = student.m_grades[j];
                }

                if (m_grades)
                        delete[] m_grades;
                
                m_grades = new_grades;
                m_gradesLength = new_gradesLength;

                std::cout << std::endl << std::endl << std::endl << m_gradesLength << std::endl << std::endl << std::endl;
        }

        return *this;
}

float StudentGrades::operator ~ () const
{
        float total;
        for (unsigned i = 0; i < m_gradesLength; ++i) {
                total += m_grades[i];
        }

        return total / m_gradesLength;
}

StudentGrades::StudentGrades(unsigned studentId, unsigned gradesLength, float *grades)
{
        m_studentId = studentId;
        m_gradesLength = gradesLength;
        m_grades = grades;
}

std::istream& operator >> (std::istream& in, StudentGrades& student)
{
        unsigned studentId, gradesLength;

        {
                char c = in.peek();
                c++;
        }
        
        req(!in.eof(), "Not enough input.");
        in >> studentId;
        
        req(!in.eof(), "Not enough input.");
        in >> gradesLength;

        student.m_studentId = studentId;
        student.m_gradesLength = gradesLength;

        std::cout << student.m_studentId << ": " << student.m_gradesLength << '\n';
        
        student.m_grades = new float[gradesLength];

        float temp;

        for (unsigned i = 0; i < gradesLength; ++i) {
                req(!in.eof(), "Not enough input.");
                in >> temp;
                
                student.m_grades[i] = temp;
        }

        std::cout << "\tGrades: ";
        for (unsigned i = 0; i < gradesLength; ++i) {
                std::cout << student.m_grades[i] << ' ';
        }
        std::cout << '\n';



        return in;
}

std::ostream& operator << (std::ostream& out, const StudentGrades& student)
{
        out << student.m_studentId << ": " << ~student << std::endl;

        return out;
}


StudentGrades::~StudentGrades()
{
        if (m_grades != nullptr)
                delete[] m_grades;
}

#endif
