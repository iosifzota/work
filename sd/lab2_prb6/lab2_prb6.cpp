#include <stdexcept>
#include <vector>
#include <cstdlib>
#include <istream>
#include <fstream>
#include <iostream>
#include <memory>
#include <climits>

#define nl std::cout << std::endl
#define nll nl; nl;
#define nlll nll; nl;
#define nllll nll; nll;

/* Assert macro. Fails gracefully. ;) */
#define assert_(cond)                           \
    if (!(cond)) {                              \
        std::cerr << "Assertion failed: "       \
                  << #cond << ".\t@"            \
                  << __LINE__ << std::endl;     \
        exit(-1);                               \
    }

/* Apply fn to each element of the vector */
template<typename T>
void map(std::vector<T>&, void (*fn)(T&));

template<typename T>
void map(const std::vector<T>&, void (*fn)(const T&));

/* Custom union_find. */
template<typename T>
class union_find
{
    std::vector<T> data;

public:
    unsigned find_root(unsigned i);
    unsigned find(unsigned i);

    /* Insert new "node". */
    void insert();

    /* Print data. */
    void print() const;
    void print_node(unsigned) const;

    /* Set the root of i's tree to root + compress path. */
    void set_root(unsigned i, unsigned root);

    /* Set the root of i/j's tree to which ever of the two
     * has the smaller root.
     */
    unsigned unify(unsigned i, unsigned j);

    unsigned flatten();

    inline T& operator [] (unsigned i);
    inline const T& operator [] (unsigned i) const;
};

/* Read union_find from istream. */
template<typename T>
std::istream& operator >> (std::istream& in, union_find<T>& uf);

/* Matrix. No expand functionality. */
template<typename T> class matrix {
private:
    T ** data;
    unsigned int nrows, ncols;

public:

    matrix(unsigned int m, unsigned int n);
    matrix(const matrix<T>&);
    ~matrix();

    inline unsigned int rows();
    inline unsigned int cols();
    inline unsigned int rows() const;
    inline unsigned int cols() const;

    void read(std::istream& input);
    void read();

    void print(std::ostream& output) const;
    void print() const;

    void print_canvas(std::ostream& output) const;
    void print_canvas() const;

    std::vector<T> find_freq(const unsigned& max_freq) const;

    inline T& operator()(const unsigned& row, const unsigned& col);
    inline const T& operator()(const unsigned& row, const unsigned& col) const;
    inline T* operator()(const unsigned& row);
};
/* Check if img(row, col) isn't a wall. */
inline bool is_obj_pixel(matrix<int>&, unsigned, unsigned);
/* Using is_obj_pixel check if img(row, col) has any neighbor. */
inline bool has_neighbor(matrix<int>&, unsigned, unsigned);

/* Get the min_label of the neighbors. */
unsigned min_equiv_label(matrix<int>&, matrix<int>&, union_find<unsigned>&, unsigned, unsigned);
/* Label input matrix. */
matrix<int> label(matrix<int>&, unsigned&);

void print_cell(matrix<int>& img, unsigned row, unsigned col);
void print_neighbors(matrix<int>& img, unsigned row, unsigned col);

int test_matrix();

template<typename T>
T max_v(const std::vector<T>& v, T max);

template<typename T>
T max_v(const std::vector<T>& v, T max)
{
    unsigned max_i = -1;
    for (unsigned i = 0; i < v.size(); ++i) {
        if ( v[i] > max ) {
            max = v[i];
            max_i = i;
        }
    }
    return max_i;
}


int test_matrix()
{
    unsigned rows = 0, cols = 0, max_label = -1;

    std::ifstream input("matrix.txt");

    assert_(input >> rows);
    assert_(input >> cols);

    matrix<int> mat(rows, cols);

    /* Read data */
    mat.read(input);
    input.close(); // ;)

    matrix<int> labels = label(mat, max_label);
    labels.print_canvas();

    std::cout << "Max label: " << max_label << std::endl;

    std::vector<int> freq = labels.find_freq(max_label);
    map<int>(freq, [](const int& elem) { std::cout << elem << " "; }); nl;

    int max_i = max_v(freq, -1);

    std::cout << "Biggest component "
              << max_i << " has "
              << freq[max_i] << " blocks.\n";

    return 0;
}

template<typename T>
std::vector<T> matrix<T>::find_freq(const unsigned& max_freq) const
{
    std::vector<T> freq(max_freq + 1, 0);

    //map<T>(freq, [](T& elem) { elem = 0; });

    try {
        for (unsigned row = 0; row < nrows; ++row) {
            for (unsigned col = 0; col < ncols; ++col) {
                if ( data[row][col] != -1) {
                    freq.at( data[row][col] ) += 1;
                }
            }
        }
    }
    catch(const std::out_of_range& oor) {
        std::cerr << "Out of range error: " << oor.what() << std::endl;
        exit(-1);
    }

    return freq;
}

int main()
{
    return test_matrix();
}

/* Iterate over obj_pixel neighbors. */
#define for_each_neighbor(_img, _row, _col)             \
    for (                                               \
        int i, j, count = 0,                            \
            di[] = { -1,  0 },                          \
            dj[] = {  0, -1 };                          \
        count < 2;                                      \
        ++count                                         \
        )                                               \
    {                                                   \
    i = _row + di[count], j = _col + dj[count];         \
    if ( !is_obj_pixel(_img, i, j) ) { continue; }      \

#define end_for_each }

/* Label input matrix. */
matrix<int> label(matrix<int>& img, unsigned& max_label)
{
    img.print_canvas();

    union_find<unsigned> prov_labels;
    matrix<int> labels(img.rows(), img.cols());

    /* Get provisonal labels. */
    for (unsigned row = 0, label = 0; row < img.rows(); ++row) {
        for (unsigned col = 0; col < img.cols(); ++col) {
            /* If it's a wall => it's a wall :3 */
            if (img(row, col) == -1) {
                labels(row, col) = -1;
            }
            /* If no neighbor => new label */
            else if (!has_neighbor(img, row, col)) {
                labels(row, col) = label++;
                prov_labels.insert();
            }
            /* Else unify neighbors to the min_label. */
            else {
                print_neighbors(img, row, col);
                labels(row, col) = min_equiv_label(img, labels, prov_labels, row, col);

                /* Unify */
                for_each_neighbor(img, row, col)
                    prov_labels.set_root( labels(i, j), labels(row, col) );
                end_for_each
            }
        }
    }

    /* Get final labels. */
    max_label = prov_labels.flatten();

    /* Set labels(row, col) to the final label. */
    for (unsigned row = 0; row < labels.rows(); ++row) {
        for (unsigned col = 0; col < labels.cols(); ++col) {
            if (labels(row, col) != -1) {
                labels(row, col) = prov_labels[labels(row, col)];
            }
        }
    }

    /* [Debug] */
    std::cout << "Out:\n";
    std::cout << "\tCanvas:\n";
    labels.print_canvas();

    std::cout << "\tLabels:\n";
    prov_labels.print(); nl;

    return labels;
}


/* Get the min_label of the neighbors. */
unsigned min_equiv_label(
    matrix<int>& img, matrix<int>& labels, union_find<unsigned>& prov_labels, unsigned row, unsigned col
    )
{
    std::cout << "[Debug] min_equiv_label:\n";

    labels.print_canvas(); nl;

    unsigned label, min_label = UINT_MAX;

    for_each_neighbor(img, row, col)
        label = prov_labels.find_root( labels(i, j) );
        if (label < min_label) {
            min_label = label;
        }
    end_for_each

    std::cout << "\n\tResult: " << min_label; nll;

    return min_label;
}

/* Check if img(row, col) isn't a wall. */
inline bool is_obj_pixel(matrix<int>& img, unsigned row, unsigned col)
{
    if (
        row >= img.rows() ||
        col >= img.cols() ||
        img(row, col) == -1
        )
    {
        return false;
    }
    return true;
}

/* Using is_obj_pixel check if img(row, col) has any neighbor. */
inline bool has_neighbor(matrix<int>& img, unsigned row, unsigned col)
{
    //            N   E
    int di[] = { -1,  0 };
    int dj[] = {  0, -1 };

    for (unsigned i = 0; i < 2; ++i) {
        if (is_obj_pixel(img, row + di[i], col + dj[i])) {
            return true;
        }
    }
    return false;
}

void print_cell(matrix<int>& img, unsigned row, unsigned col)
{
    std::cout << img(row, col) << "(" << row << ", " << col << ") ";
}

void print_neighbors(matrix<int>& img, unsigned row, unsigned col)
{
    std::cout << "Neighbors of ";
    print_cell(img, row, col);
    std::cout << ":\n";

    for_each_neighbor(img, row, col)
        print_cell(img, i, j); nl;
    end_for_each; nl;
}


/* ================================================================================
 * #BEGIN: Union implementaion
 * ================================================================================
 */
template<typename T>
unsigned union_find<T>::flatten()
{
    std::cout << "[Debug] flatten:\n";

    unsigned k = 0;

    for (unsigned i = 0; i < data.size(); ++i) {

        if (data[i] < i) {
            data[i] = data[data[i]];
        }
        else {
            data[i] = k++;
        }
    }
    print();

    return --k;
}

/* Set the root of i tree to the root of j tree.
 * And compress path.
 */
template<typename T>
unsigned union_find<T>::unify(unsigned i, unsigned j)
{
    unsigned root, rootj;

    root = find_root(i);

    if (i != j) {
        rootj = find_root(j);

        if(rootj < root) {
            root = rootj;
        }
        std::cout << "Smaller root is " << data[root] << ".\n";

        std::cout << "Flattening... ";
        print();

        set_root(j, root);
    }

    set_root(i, root);
    return root;
}


/* Set the root of i's tree and compress path. */
template<typename T>
void union_find<T>::set_root(unsigned i, unsigned root)
{
    std::cout << "\n[Debug] set root:\n";

    std::cout << "\tRoot: "; print_node(root);
    std::cout << "\tNode: "; print_node(i); nl;

    try {
        for (unsigned aux; data.at(i) < i; i = aux) {
            print_node(i); std::cout << " ";
            aux = data.at(i), data.at(i) = root;
        }
        data.at(i) = root;
    }
    catch(const std::out_of_range& oor) {
        std::cerr << "Out of range error: " << oor.what() << std::endl;
        exit(-1);
    }

    std::cout << "\t->: ";
    print(); nl;
}

/* Get the root of i's tree and compress path. */
template<typename T>
unsigned union_find<T>::find(unsigned i)
{
    unsigned root;

    root = find_root(i);
    set_root(i, root);

    return root;
}

/* Find the root of i's tree */
template<typename T>
unsigned union_find<T>::find_root(unsigned i)
{
    std::cout << "\n[Debug] find_root:\n";
    unsigned root = i;

    try {
        while (data.at(root) < root) {
            root = data.at(root);
        }
    }
    catch(const std::out_of_range& oor) {
        std::cerr << "Out of range error: " << oor.what() << std::endl;
        exit(-1);
    }

    std::cout << "\tRoot of "; print_node(i);
    std::cout << "is "; print_node(root); nl;
    return root;
}

/* Push back a new node. */
template<typename T>
void union_find<T>::insert()
{
    data.push_back(data.size());
}

/* Map print over data. */
template<typename T>
void union_find<T>::print() const
{
    map<T>(data, [](const T& elem) { std::cout << elem << " "; });
}

/* Print a node: value(index) */
template<typename T>
void union_find<T>::print_node(unsigned i) const
{
    try {
        std::cout << data.at(i) << "(" << i << ")";
    }
    catch(const std::out_of_range& oor) {
        std::cerr << "Out of range error: " << oor.what() << std::endl;
        exit(-1);
    }
}

template<typename T>
inline T& union_find<T>::operator[](unsigned i)
{
    if (i >= data.size()) {
        std::cerr <<"[Error] Trying to access array out of bounds." << std::endl;
        exit(-1);
    }

    return data[i];
}

template<typename T>
inline const T& union_find<T>::operator[](unsigned i) const
{
    if (i >= data.size()) {
        std::cerr <<"[Error] Trying to access array out of bounds." << std::endl;
        exit(-1);
    }

    return data[i];
}

template<typename T>
std::istream& operator >> (std::istream& in, union_find<T>& uf)
{
    T temp;

    while (in >> temp) {
        uf.insert(temp);
    }

    return in;
}
/* ================================================================================
 * #END: Union implementaion
 * ================================================================================
 */


/* ================================================================================
 * #BEGIN: Matrix implementaion
 * ================================================================================
 */
/* Constructor */
template<typename T>
matrix<T>::matrix(unsigned int m, unsigned int n)
{
    assert_(m != 0 && n != 0);

    nrows = m;
    ncols = n;

    data = new T*[nrows];
    data[0] = new T[nrows * ncols];

    for (unsigned int i = 1; i < nrows; ++i) {
        data[i] = data[i - 1] + ncols;
    }

    for (unsigned int row = 0; row < nrows; ++row) {
        for (unsigned int col = 0; col < ncols; ++ col) {
            data[row][col] = -1;
        }
    }
}

template<typename T>
matrix<T>::matrix(const matrix<T>& mat)
{
    nrows = mat.nrows;
    ncols = mat.ncols;

    data = new T*[nrows];
    data[0] = new T[nrows * ncols];

    for (unsigned i = 1; i < nrows; ++i) {
        data[i] = data[i - 1] + ncols;
    }

    for (unsigned int row = 0; row < nrows; ++row) {
        for (unsigned int col = 0; col < ncols; ++ col) {
            data[row][col] = mat(row, col);
        }
    }
}

template<typename T>
inline unsigned int matrix<T>::rows()
{
    return nrows;
}

template<typename T>
inline unsigned int matrix<T>::cols()
{
    return ncols;
}

template<typename T>
inline unsigned int matrix<T>::rows() const
{
    return nrows;
}

template<typename T>
inline unsigned int matrix<T>::cols() const
{
    return ncols;
}

/* Read matrix from istream. */
template<typename T>
void matrix<T>::read(std::istream& input)
{
    for (unsigned int i = 0; i < nrows; ++i) {
        for (unsigned int j = 0; j < ncols; ++j) {
            assert_(input >> data[i][j]);
        }
    }
}
template<typename T>
void matrix<T>::read() { this->read(std::cin); }

/* Print matrix to oustream. */
template<typename T>
void matrix<T>::print(std::ostream& output) const
{
    for (unsigned int i = 0; i < nrows; ++i) {
        for (unsigned int j = 0; j < ncols; ++j) {
            output << data[i][j] << " ";
        }
                output << std::endl;
    }
}
    template<typename T>
    void matrix<T>::print() const { print(std::cout); }

/* Custom print. */
template<typename T>
void matrix<T>::print_canvas(std::ostream& output) const
{
    for (unsigned int i = 0; i < nrows; ++i) {
        for (unsigned int j = 0; j < ncols; ++j) {
            if (data[i][j] == -1) {
                output << "- ";
            }
            else {
                output << data[i][j] << " ";
            }
        }
            output << std::endl;
    }
}
    template<typename T>
    void matrix<T>::print_canvas() const { print_canvas(std::cout); }

/* Matrix access () */
template<typename T>
inline T& matrix<T>::operator()(const unsigned& row, const unsigned& col)
{
    if (row >= nrows || col >= ncols) {
        std::cerr << "[Error] Trying to access array out of bounds: @" << this << std::endl;
        exit(-1);
    }
    return data[row][col];
}

/* Matrix access () */
template<typename T>
inline const T& matrix<T>::operator()(const unsigned& row, const unsigned& col) const
{
    if (row >= nrows || col >= ncols) {
        std::cerr << "[Error] Trying to access array out of bounds: @" << this << std::endl;
        exit(-1);
    }
    return data[row][col];
}

template<typename T>
inline T* matrix<T>::operator()(const unsigned& row)
{
    if (row >= nrows) {
        std::cerr << "[Error] Trying to access array out of bounds: @" << this << std::endl;
        exit(-1);
    }
    return data[row];
}

/* Destructor */
template<typename T>
matrix<T>::~matrix()
{
    delete data[0];
    delete data;
}
/* ================================================================================
 * #END: Matrix implementaion
 * ================================================================================
 */

/* ================================================================================
 * #Begin: Miscellaneous
 * ================================================================================
 */
/* Map fn over v. */
template<typename T>
void map(std::vector<T>& v, void (*fn)(T&))
{
    for (auto& itr : v) {
        (*fn)(itr);
    }
}

template<typename T>
void map(const std::vector<T>& v, void (*fn)(const T&))
{
    for (const auto& itr : v) {
        (*fn)(itr);
    }
}
/* ================================================================================
 * #End: Miscellaneous
 * ================================================================================
 */
