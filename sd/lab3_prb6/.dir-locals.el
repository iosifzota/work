;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

((nil
  (cmake-ide-dir . "build"))
 (c++-mode
  (flycheck-clang-language-standard . "c++11")
  (flycheck-gcc-language-standard . "c++11")))


