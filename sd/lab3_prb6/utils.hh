#ifndef __utils_hh
#define __utils_hh

#include <vector>
#include <istream>

#define nl std::cout << std::endl
#define nll nl; nl;
#define nlll nll; nl;
#define nllll nll; nll;

/* Assert macro. Fails gracefully. ;) */
#define assert_(cond)                           \
    if (!(cond)) {                              \
        std::cerr << "Assertion failed: "       \
                  << #cond << ".\t@"            \
                  << __LINE__ << std::endl;     \
        exit(-1);                               \
    }

#define assert__(cond)                              \
    ((cond) ? (NULL) :                              \
        (                                           \
            std::cerr << "Assertion failed: "       \
                      << #cond << ".\t@"            \
                      << __LINE__ << std::endl,     \
            exit(-1),                               \
            NULL                                    \
        ),                                          \
        NULL                                        \
    )

#define nn(n) for (int i = 0; i < n; ++i) std::cout << std::endl;

template<typename T>
void map(std::vector<T>& v, void (*fn)(T&))
{
    for (auto& itr : v) {
        (*fn)(itr);
    }
}

template<typename T>
void map(const std::vector<T>& v, void (*fn)(T&))
{
    for (auto& itr : v) {
        (*fn)(itr);
    }
}

template<typename T>
void map(const std::vector<T>& v, void (*fn)(const T&))
{
    for (const auto& itr : v) {
        (*fn)(itr);
    }
}

template<typename T>
std::istream& operator >> (std::istream& in, std::vector<T>& v)
{
    T data;

    while (in >> data) {
        v.push_back(data);
    }

    return in;
}

#endif
