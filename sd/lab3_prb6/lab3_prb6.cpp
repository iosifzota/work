#include <fstream>
#include <vector>
#include <sstream>
#include <cstdlib>
#include <memory>
#include <queue>
#include <functional>
#include <iostream>

#include "freq.hh"
#include "utils.hh"

#define input_file "Text.txt"

/* Given a file_path returns a vector of h_char nodes. */
std::vector< std::shared_ptr<h_char> > read_file(const char* file_path);

/* Insert in ORDER a h_char node into the vector. */
void insert(std::vector< std::shared_ptr<h_char> >&, std::shared_ptr<h_char>&);

/* Create h_char vector from frequencies vector. */
std::vector< std::shared_ptr<h_char> > create_freqs(const std::vector<unsigned>& freq);

class huff_tree
{
public:

    /* Create tree from an ordered h_char vector. */
    void create_tree(std::vector< std::shared_ptr<h_char> >&);
    void create_tree_from_file(const char *file_path);

    std::vector<std::string> create_codes();
    std::string decode(std::string);

    void breadth_traversal(std::function<void(std::shared_ptr<h_char>&)> action);
    void cc_body(
        std::vector <std::string>& codes,
        const std::shared_ptr<h_char>& current_node,
        char cc, std::string& code);

private:
    std::shared_ptr<h_char> root;
};

std::string encode(const char* file_path, const std::vector<std::string>& codes);

int test_huffman();
int main() { test_huffman(); }

#define input_file "Text.txt"

int test_huffman()
{
    huff_tree hf;

    /* Create huff_tree. */
    hf.create_tree_from_file(input_file);

    /* Create vector of prefix codes for the characters. */
    std::vector<std::string> codes = hf.create_codes();

    /* Print out the code for each character. */
    for (unsigned char c = 0; c < codes.size(); ++c) {
        if (codes[c].size()) {
            std::cout << c << ": " << codes[c] << '\n';
        }
    }
    nn(1);

    /* Encode the text. */
    std::string encoding = encode(input_file, codes);
    std::cout << "Encoded:\n" << encoding; 	nn(2);

    /* Decode the text. */
    std::string decoding = hf.decode(encoding);
    std::cout << "Decoded:\n" << decoding; 	nn(2);

    return 0;
}

std::string huff_tree::decode(std::string encoding)
{
    std::string decoding;
    std::shared_ptr<h_char> itr;

    unsigned pos = 0;

    assert_(root != nullptr);

    while (pos < encoding.size()) {

        itr = root;
        assert_(encoding[pos++] == '0');

        /* Follow the bits left and right through the tree. */
        while (pos < encoding.size() && itr->c == 0) {
            if (encoding[pos] == '0') {
                itr = itr->left;
            }
            else if (encoding[pos] == '1') {
                itr = itr->right;
            }
            else {
                std::cerr << "[Error] Unexpected symbol: " << encoding[pos] << std::endl;
                exit(-1);
            }

            assert_(itr != nullptr);
            ++pos;
        }

        assert_(itr->c != 0);

        decoding.push_back(itr->c);
    }

    return decoding;
}

std::string encode(const char* file_path, const std::vector<std::string>& codes)
{
    std::ifstream in(file_path);

    std::string encoding;

    char c;

    while ((c = in.get()) != EOF) {
        for (const auto& c_index : codes[c]) {
            encoding.push_back(c_index);
        }
    }

    return encoding;
}


void huff_tree::breadth_traversal(
    std::function<void(std::shared_ptr<h_char>&)> visit
    )
{
    std::shared_ptr<h_char> node;
    std::queue< std::shared_ptr<h_char> > fringe;
    fringe.push(this->root);

    do {
        node = fringe.front();
        fringe.pop();

        visit(node);

        if (node->left != nullptr) {
            fringe.push(node->left);
        }

        if (node->right != nullptr) {
            fringe.push(node->right);
        }

    } while (!fringe.empty());
}


void huff_tree::create_tree(std::vector< std::shared_ptr<h_char> >& data)
{
    std::shared_ptr<h_char> left, right, new_node;

    while (data.size() > 1) {
        /* Get right child. */
        right = data[data.size() - 1];
        data.pop_back();

        /* Get left child. */
        left = data[data.size() - 1];
        data.pop_back();

        /* Create new parent. */
        new_node = std::make_shared<h_char>(
            0,
            right->freq + left->freq,
            left,
            right
            );

        /* Insert the new parent. */
        insert(data, new_node);
    }

    assert_(data.size() > 0);

    root = data[0];
}


void insert(std::vector< std::shared_ptr<h_char> >& data, std::shared_ptr<h_char>& node)
{
    int i;

    data.push_back(node);

    for (i = data.size() - 2; i >= 0; --i) {
        if (*node.get() < *data[i].get()) {
            break;
        }
        data[i + 1] = data[i];
    }

    data[++i] = node;
}

std::vector< std::shared_ptr<h_char> > create_freqs(const std::vector<unsigned>& freq)
{
    std::shared_ptr<h_char> node;
    std::vector< std::shared_ptr<h_char> > data;

    for (unsigned c = 0; c < freq.size(); ++c) {
        if (freq[c]) {
            node = std::make_shared<h_char>(c, freq[c]);

            insert(data, node);
        }
    }

    return data;
}

std::vector< std::shared_ptr<h_char> > read_file(const char* file_path)
{
    std::ifstream in(file_path);

    std::vector<unsigned> freq;
    std::vector< std::shared_ptr<h_char> > data;

    freq = find_freq(in);

    /* Create h_char vector from frequencies vector. */
    data = create_freqs(freq);

    /* Print input data. */
    map< std::shared_ptr<h_char> >(
        data,
        []( std::shared_ptr<h_char>& ptr ) {
            std::cout << "'" << ptr->c << "'" << ": " << ptr->freq << ' '; }
        );
    nn(2);

    in.close();

    return data;
}


std::vector<std::string> huff_tree::create_codes()
{
    std::vector<std::string> codes(200);
    std::string dummy;

    cc_body(codes, root, '0', dummy);

    return codes;
}

void huff_tree::cc_body(
    std::vector <std::string>& codes,
    const std::shared_ptr<h_char>& current_node,
    char cc, std::string& code)
{
    assert_(current_node != nullptr);

    code.push_back(cc);

    if (current_node->c != 0) {
        assert_(current_node->right == nullptr);
        assert_(current_node->left == nullptr);
        codes[current_node->c] = code;
    }

    if (current_node->left != nullptr) {
        cc_body(codes, current_node->left, '0', code);
    }

    if (current_node->right != nullptr) {
        cc_body(codes, current_node->right, '1', code);
    }

    code.pop_back();
}

void huff_tree::create_tree_from_file(const char *file_path)
{
    std::vector< std::shared_ptr<h_char> > data = read_file(file_path);
    create_tree(data);
}
