#ifndef __freq_hh
#define __freq_hh

#include <vector>
#include <istream>
#include <memory>
#include <iostream>

struct h_char {
    char c;
    unsigned freq;
    std::shared_ptr<h_char> left, right;

    h_char(char c = 0, unsigned freq = 0,
           std::shared_ptr<h_char> left = nullptr,
           std::shared_ptr<h_char> right = nullptr);

    inline bool operator < (const h_char&) const;
    inline bool operator > (const h_char&) const;
    inline bool operator <=(const h_char&) const;
    inline bool operator >=(const h_char&) const;
};

std::vector<unsigned> find_freq(std::istream&);


/* What i learned: inline function must be defined in every
	file that uses the respective function, so it's easier to
	simply put it in the header. */
inline bool h_char::operator < (const h_char& other) const
{
	if (freq < other.freq) {
		return true;
	}
	else if (freq == other.freq && c < other.c) {
		return true;
	}

	return false;
}

inline bool h_char::operator > (const h_char& other) const
{
	return other < *this;
}

inline bool h_char::operator <=(const h_char& other) const
{
	return !(*this > other);
}

inline bool h_char::operator >=(const h_char& other) const
{
	return !(*this < other);
}

#endif
