#ifndef STACK
#define STACK

typedef struct {
	unsigned int n;
	int *st;
} Stack;

void push(Stack *st, int);
int pop(Stack *s);

unsigned int heightOf(Stack *s);

unsigned int isEmpty(Stack *s);
unsigned int isFull(Stack *s, unsigned int);

#endif
