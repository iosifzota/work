;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

((nil
  (cmake-ide-project-dir . "~/work/algorithms")
  (cmake-ide-build-dir . "~/work/algorithms/build"))
 (c++-mode
  (flycheck-clang-language-standard . "c++11")))

