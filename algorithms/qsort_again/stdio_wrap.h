#pragma once

#ifndef INCLUDE_stdio_wrap__
#define INCLUDE_stdio_wrap__

#include <stdio.h>
#include "misc.h"

/* Erorr checked fopen(). */
FILE *ec_fopen(const char *file_path, const char *mode);

/* Variable length TAB. */
#define TAB(length) printf("%" #length "c", 0)


#define file_isEmpty_(file_path, eof_expr) do {                         \
	FILE *____fd_test = ec_fopen(file_path, "a+");                  \
	assert(fgetc(____fd_test) != EOF && !feof(____fd_test), "", printf("File %s is empty.\n", file_path); fclose(____fd_test); eof_expr; break); \
	fclose(____fd_test);                                            \
    } while(0)


#define file_isEmpty(fd, file_path, eof_expr)                           \
    assert(fgetc(fd) != EOF && !feof(fd), "", printf("File %s is empty.\n", file_path); fclose(fd); eof_expr); \
    rewind(fd)

#define scanf_number(identifier, calling_fn, err_expr) {                \
	char *err = 0, str[1000];                                       \
	scanf("%s", str);                                               \
	identifier = strtol(str, &err, 10);                             \
	if (*err) {                                                     \
            printf("[Error] " #calling_fn ": \"%s\" is not a number.\n", str); \
            err_expr;                                                   \
	} }

/* Read and print an array, given a format_specifer */
#define freadarr(fd, format_specifier, arr, nelems)  do {               \
        forIn (i, 0, nelems) {                                          \
            if (EOF == fscanf(fd, format_specifier, arr + i)) {         \
                nelems = i; break;                                      \
            }                                                           \
        }                                                               \
    } while(0)
#define fprintarr(fd, format_specifier, arr, nelems)    \
    forIn (i, 0, nelems) {                              \
        fprintf(fd, format_specifier, *(arr + i));      \
    }                                                   \
    NEWLINE;                                            \

/* Read and print a matrix. */
#define freadmat(fd, format_specifier, matrix, rows,  cols)  do {       \
	file_isEmpty(fd, "passed to freadarr", rows = cols = 0; break); \
	int err = 0;                                                    \
        forIn (ln_index, 0, rows) {                                     \
            forIn (col_index, 0, cols) {                                \
                if ( EOF == fscanf(fd, format_specifier,matrix[ln_index] + col_index)) { \
                    if ((!col_index && !ln_index) || col_index) cols = col_index; \
                    err = 1;                                            \
                }                                                       \
            }                                                           \
            if (err) { rows = ln_index; break; }                        \
        }                                                               \
    } while(0)

#define fprintmat(fd, format_specifier, matrix, rows,  cols)            \
    NEWLINE;                                                            \
    forIn (ln_index, 0, rows) {                                         \
        forIn (col_index, 0, cols) {                                    \
            fprintf(fd, format_specifier " ",matrix[ln_index][col_index]); \
        }                                                               \
        NEWLINE;                                                        \
    }                                                                   \
    NEWLINE;



#endif
