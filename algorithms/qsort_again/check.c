#include "stdio.h"
#include "time.h"
#include "stdlib.h"
#include "misc.h"

int main()
{
	FILE *out;

	out = fopen("i.txt", "w");

	srand((unsigned int)time(NULL));
	forIn(i, 0, 1000) {
		fprintf(out, "%d ", (int)rand() % 1001);
	}
	fclose(out);

	return 0;
}