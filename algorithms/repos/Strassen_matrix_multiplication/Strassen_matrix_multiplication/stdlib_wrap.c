#include "stdlib_wrap.h"

void *ec_realloc(void *ptr, size_t size)
{
	void *new_ptr;

	new_ptr = realloc(ptr, size);

	if (new_ptr == NULL) {
		exit(RESOURCE_ACQUISITION_FAILED);
	}

	return new_ptr;
}
void *ec_malloc(size_t size)
{
	return ec_realloc(NULL, size);
}
void *ec_calloc(size_t nitems, size_t size)
{
	void *new_ptr;

	new_ptr = calloc(nitems, size);

	if (new_ptr == NULL) {
		exit(RESOURCE_ACQUISITION_FAILED);
	}

	return new_ptr;
}