#include "stdio_wrap.h"
#include "stdlib_wrap.h"

int tst();
int init();


int main()
{
	return tst();
	//return init();
	return 0;
}

#define SIZE 2

int tst()
{
	int mat1[][SIZE] = {
		{ 1, 2},
		{ 2, 1}
	};
	int mat2[][SIZE] = {
		{ 1, 2},
		{ 2, 1}
	};

	/*
	5 4
	4 5
	*/

	int p1, p2, p3, p4, p5, p6, p7;

	p1 = mat1[0][0] * (mat2[0][1] - mat2[1][1]);
	p2 = (mat1[0][0] + mat1[0][1]) * mat2[1][1];
	p3 = (mat1[1][0] + mat1[1][1]) * mat2[0][0];
	p4 = mat1[1][1] * (mat2[1][0] - mat2[0][0]);
	p5 = (mat1[0][0] + mat1[1][1]) * (mat2[0][0] + mat2[1][1]);
	p6 = (mat1[0][1] - mat1[1][1]) * (mat2[1][0] + mat2[1][1]);
	p7 = (mat1[0][0] - mat1[1][0]) * (mat2[0][0] + mat2[0][1]);

	int rez[SIZE][SIZE];

	rez[0][0] = p5 + p4 - p2 + p6;
	rez[0][1] = p1 + p2;
	rez[1][0] = p3 + p4;
	rez[1][1] = p1 + p5 - p3 - p7;

	fprintmat(stdout, "%d", rez, SIZE, SIZE);

	return 0;
}


int init()
{
	return 0;
}