#pragma once

#ifndef INCLUDE_stdlib_wrap__
#define INCLUDE_stdlib_wrap__

#include <stdlib.h>
#include "misc.h"

/* Error checked malloc, realloc and calloc. */
void *ec_malloc(size_t);
void *ec_realloc(void *, size_t);
void *ec_calloc(size_t, size_t);

/* Allocate memory for an array of the given type. */
#define mem_arr(type, ptr, size) \
    (ptr) = (type *) ec_malloc((size) * sizeof(type))
#define mem_arr_calloc(type, ptr, size) \
    (ptr) = (type *) ec_calloc(size, sizeof(type));

/* Allocate a matrix of any type. */
#define mem_matrix(type, matrix, rows, cols)    \
    mem_arr(type*, matrix, rows);               \
    forIn (i, 0, rows) {                        \
        mem_arr(type, matrix[i], cols);         \
    }
#define mem_matrix_calloc(type, matrix, rows, cols)    \
    mem_arr_calloc(type*, matrix, rows);               \
    forIn (i, 0, rows) {                        \
        mem_arr_calloc(type, matrix[i], cols);         \
    }

/* Deallocate a matrix of any type. */
#define free_matrix(matrix, rows)               \
    forIn (i, 0, rows) {                        \
        free(matrix[i]);                        \
    }                                           \
    free(matrix);


/* Delete |offset| elements, beginning with startingI. */
#define arrnshrink(type, arr, len, startingI, offset) do {  \
                                                            \
	if (offset >= (int)len || offset < 1 || len < 1) {      \
		len = 0;											\
		break;												\
	}                                                       \
                                                            \
	/* Start at the startingIndex. */                       \
	int begin = startingI;                                  \
	/* End at the last Index(len-1), minus the offset. */   \
	int end = len - 1 - offset;                             \
															\
	/* Left to right, a[i] = a[i + offset]. */              \
	for (                                                   \
		int i = begin;										\
		i <= end;											\
		i++)												\
	{														\
		arr[i] = arr[i + offset];							\
	}														\
															\
	/*arr = (type *)realloc(arr, (len - offset) * sizeof(type));*/\
															\
	len -= offset;											\
} while(0)


/* Shift array and =fill= beginning with startingI. */
#define arrnfill(type, arr, len, startingI, offset, fill_val) do {	\
	type fill = fill_val;										\
																\
	arr = (type *)ec_realloc(arr, len * sizeof(type) + offset);	\
																\
	if (offset < 1 || len < 1) {								\
		break;													\
	}															\
																\
	if (startingI >= len) {										\
		len = len + offset;										\
																\
		for (unsigned int i = len - offset; i < len; i++) {		\
			arr[i] = fill;										\
		}														\
																\
		break;													\
	}															\
																\
	/* Start at last Index(len-1), plus the offset. */			\
	int begin = len - 1 + offset;								\
	/* End at the startingIndex + offset. */					\
	int end = offset + startingI;								\
																\
	/* Right to left, a[i] = a[i - offset] */					\
	for (														\
		int i = begin;											\
		i >= end;												\
		i--)													\
	{															\
		arr[i] = arr[i - offset];								\
	}															\
																\
	/* Left to right, from startingIndex until (startingIndex + offset), fill. */ \
	for (int i = end - offset; i < end; i++) {					\
		arr[i] = fill;											\
	}															\
																\
	len += offset;												\
} while(0)


#endif