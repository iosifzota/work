#include "stdlib_wrap.h"
#include "stdio_wrap.h"
#include <string.h>
#include <ctype.h>


#define strtod_(str, identifier, calling_fn, err_expr) {\
	char *err = 0;								             \
	identifier = strtod(str, &err);	     					 \
	if (*err) {												 \
		printf("[Error] " #calling_fn ": \"%s\" is not a number.\n", str);\
		err_expr;											 \
	} }


#define $_  printf("$: ")

typedef struct {
	char *s;
	unsigned int len;
} String;

/* Return a token from a given file descriptor. */
String ftok(FILE *fd, const char *del);

#define assert_ftok(fd, token, err_expr)           \
		assert((token = ftok(fd, ",\n ")).len, "", \
			free(token.s); err_expr);

typedef struct {
	String lastName;
	String firstName;
	float grade;
} Student;

/* Free students (the strings). */
void free_students(Student *students, unsigned int numberof_students);

/* Create an array with the contents of a database file and assign it to *students. */
unsigned int retriveStudents(const char *db_file_path,Student **students);
/* Given a Student array, write it to do a database file. */
unsigned int updateStudents(const char *db_file_path, Student *current_students, unsigned int numberof_students);

/* Create an array with the contents of a file and assign it to *students. */
unsigned int readStudentsFile(Student **students, const char *file_path);
/* Get readStudentsFile() + retriveStudents(), then write them both to the database file. */
unsigned int insertStudents(const char *db_file_path, const char *file_path);

/* Search for a student in a database by firstName and lastName. Returns nr. of matches.
 * If there's match(es) creates an array with the indexes of the matches and assigns to *matches.*/
unsigned int searchStudents(const char *db_file_path, const char *firstName, const char *lastName, unsigned int **matches);
/* Search and remove a student. */
unsigned int removeStudent(const char *db_file_path, const char *firstName, const char *lastName);
/* Search and update a student's grade. */
unsigned int updateStudent(const char *db_file_path, const char *firstName, const char *lastName);

/* Print students given a Student array, or a database file path. */
void put_students(Student *students, unsigned int numberof_students);
void showStudents(const char *db_file_path);


void saveto_csv(const char *db_file_path, const char *csv_file_path);


/* Choose an active database. */
char **databases;
int active_db_index = -1; unsigned int rows, cols = 1;
int changeActiveDB();
int setActiveDB(char *db_file_path);

/* Database history management. */
#define history_limit 9
char *history_file_path;
void readDatabasesHistory(const char *history_file_path);
void saveDatabasesHistory(const char *history_file_path);
void showDatabasesMenu();


/* Sort the active db. */
void sort();
void showSortMenu();

/* Sort a database by firstName, lastName or Grade. */
void sortStudents(const char *db_file_path, int(*compare)(void *, void *));
int byfirstName(void *, void *);
int bylastName(void *, void *);
int byGrade(void *, void *);
void qsort_(Student *, int , int(*)(void *, void *));
void studentswap(Student *, int, int);

void showMenu(); // app-menu
int app(); // app
void quit();
int init(const char *);



int test()
{
	return 0;
}

int main(int argc, char *argv[])
{
	if (argc < 2) {
		return init(NULL);
	}
	else if (argc > 2) {
		printf("Usage: %s db_file_path", argv[0]);
		return 0;
	}

	return init(argv[1]);

	//return test();
	//return 0;
}

/* Execution starts here. */
int init(const char *db_file_path)
{
	/* Setup history. */
	rows = history_limit;
	mem_arr(char *, databases, rows);

	printf("Databases history file path: ");
	history_file_path = (ftok(stdin, ",!?#&*^$()<>\n ")).s;

	readDatabasesHistory(history_file_path);

	/* If db_file_path was given(argv[1]) use it as active db. */
	if (db_file_path) {
		char *temp;
		mem_arr(char, temp, strlen(db_file_path) + 1);
		strcpy(temp, db_file_path);
		setActiveDB(temp);
	}

	/* Choose an active database. */
	while (active_db_index == -1) {
		if (changeActiveDB() == -2) {
			quit();
			return 0;
		}
	}

	/* Start app. */
	return app();
}


void quit()
{
	saveDatabasesHistory(history_file_path);
	free(history_file_path);
	free_matrix(databases, rows);
	exit(0);
}


/* The bulk. */
int app()
{
#ifdef __linux__
	system("clear");
#endif
#ifdef _WIN32
	system("cls");
#endif

	showMenu(); $_;
	char *firstName, *lastName;

	int c;
	while ((c = getchar()) != EOF) {

		if (c != '\n') {

			switch (c) {
			case 'i':  // Read a file_path and attempt to add data to the active db.
				NEWLINE;
				char file_path[100];
				printf("File path: ");
				scanf("%s", file_path);
				insertStudents(databases[active_db_index], file_path);
				NEWLINE;

				break;

			case 'u': // Update a student's grade from the active db.
				NEWLINE;
				printf("Firstname: "); firstName = (ftok(stdin, ",!?#&*^$()<>\n ")).s;
				printf("Lastname: "); lastName = (ftok(stdin, ",!?#&*^$()<>\n ")).s;
				updateStudent(databases[active_db_index], firstName, lastName);
				free(firstName); free(lastName);
				NEWLINE;

				break;

			case 'r': // Remove a student from the active db.
				NEWLINE;
				printf("Firstname: "); firstName = (ftok(stdin, ",!?#&*^$()<>\n ")).s;
				printf("Lastname: "); lastName = (ftok(stdin, ",!?#&*^$()<>\n ")).s;
				removeStudent(databases[active_db_index], firstName, lastName);
				free(firstName); free(lastName);
				NEWLINE;

				break;

			case 'd': // Erase the active db.
				updateStudents(databases[active_db_index], NULL, 0);
				NEWLINE;

				break;

			case 'p': // Print the contents of the active db.
				NEWLINE;
				showStudents(databases[active_db_index]);
				NEWLINE;
				break;

			case 's': // Sort the active db.
				sort();
#ifdef __linux__
				system("clear");
#endif
#ifdef _WIN32
				system("cls");
#endif
				showMenu();

				break;

			case 'l': // Change active db
				changeActiveDB();
#ifdef __linux__
				system("clear");
#endif
#ifdef _WIN32
				system("cls");
#endif
				showMenu();

				break;

			case 'e':
				printf("File name: ");
				saveto_csv(databases[active_db_index], (ftok(stdin, ",!?#&*^$()<>\n ")).s);
				break;

			case '0':
				quit();

			case 'q':
				quit();

			case 'Q':
				quit();

				// clear screen & showMenu
			case 'c':
#ifdef __linux__
				system("clear");
#endif
#ifdef _WIN32
				system("cls");
#endif
				showMenu();

				break;

			default:
				puts("Invalid option.\n");

				break;
			}

			$_;
		}
	}
	return 0;
}

void showMenu()
{
	printf("Select option:\
            \ni. Insert students from csv\
            \nu. Update student\
            \nr. Remove student\
            \nd. Delete students database\
            \np. Print students\
			\nl. Load database\
			\ns. Sort database\
			\ne. Export to csv\
            \n0. Exit");

	while (active_db_index < 0) {
		if (-2 == changeActiveDB()) {
			exit(0);
		}
	}
	printf("\n\nActive database: %s\n\n", databases[active_db_index]);
}


void saveto_csv(const char *db_file_path, const char *csv_file_path)
{
	Student *students;
	unsigned int numberof_students;

	numberof_students = retriveStudents(db_file_path, &students);
	assert(numberof_students, "\n[Error] No writes!\n)", return);

	FILE *fd = ec_fopen(csv_file_path, "w");

	forIn(i, 0, numberof_students) {
		fprintf(fd, "%s, ", students[i].firstName.s);
		fprintf(fd, "%s, ", students[i].lastName.s);
		fprintf(fd, "%f\n", students[i].grade);
	}

	fclose(fd);
	free_students(students, numberof_students);
}

unsigned int removeStudent(const char *db_file_path, const char *firstName, const char *lastName)
{
	unsigned int numberof_students, numberof_matches = 0, *matches;
	Student *students;

	numberof_students = retriveStudents(db_file_path, &students);
	assert(numberof_students, "No students to search for.\n", return 0);

	numberof_matches = searchStudents(db_file_path, firstName, lastName, &matches);

	if (numberof_matches) {
		forIn(match_index, 0, numberof_matches) {
			free(students[matches[match_index] - match_index].firstName.s);
			arrnshrink(Student, students, numberof_students, (matches[match_index] - match_index), 1);
		}

		updateStudents(db_file_path, students, numberof_students);
		free(matches);
	}

	free(students);
	return numberof_students;
}

void showEdit()
{
	printf("Edit by:\
            \n1. First Name\
            \n2. Last Name\
            \n3. Grade\
            \nw. Write to db\
            \n0. Exit\n\n");
}


unsigned int updateStudent(const char *db_file_path, const char *firstName, const char *lastName)
{
	unsigned int numberof_students, numberof_matches = 0, *matches;
	Student *students;

	numberof_students = retriveStudents(db_file_path, &students);
	assert(numberof_students, "No students to search for.\n", return 0);

	numberof_matches = searchStudents(db_file_path, firstName, lastName, &matches);

	if (numberof_matches) {
	#ifdef __linux__
		system("clear");
	#endif
	#ifdef _WIN32
		system("cls");
	#endif

		showEdit(); $_;

		int c; String s; float gr;
		while ((c = getchar()) != EOF) {

			if (c != '\n') {

				switch (c) {
				case '1':
					NEWLINE;
					printf("New first name: ");	s = ftok(stdin, ",!?#&*^$()<>\n ");
					free(students[matches[0]].firstName.s);
					students[matches[0]].firstName.s = s.s;
					students[matches[0]].firstName.len = s.len;
					NEWLINE;
					break;

				case '2':
					NEWLINE;
					printf("New last name: ");	s = ftok(stdin, ",!?#&*^$()<>\n ");
					free(students[matches[0]].lastName.s);
					students[matches[0]].lastName.s = s.s;
					students[matches[0]].lastName.len = s.len;
					NEWLINE;
					break;

				case '3':
					NEWLINE;
					printf("New grade name: ");	scanf("%f", &gr);
					students[matches[0]].grade = gr;
					NEWLINE;
					break;

				case 'p':
					NEWLINE;
					put_students(students, numberof_students);
					NEWLINE;
					break;

				case 'w':
					updateStudents(db_file_path, students, numberof_students);
					break;

				case '0':
					free(matches);
					free_students(students, numberof_students);
					return 0;

				case 'q':
					free(matches);
					free_students(students, numberof_students);
					return 0;

				case 'Q':
					free(matches);
					free_students(students, numberof_students);
					return 0;

					// clear screen & showMenu
				case 'c':
	#ifdef __linux__
					system("clear");
	#endif
	#ifdef _WIN32
					system("cls");
	#endif
					showEdit();

					break;

				default:
					puts("Invalid option.\n");

					break;
				}

				$_;
			}
		}

	}


	return numberof_students;
}


unsigned int updateStudents(const char *db_file_path, Student *students, unsigned int numberof_students)
{
	if (!numberof_students) {

		char c = 0;

		do {
			if (c != '\n')
				printf("\nDelete datebase?<y/n> ");
		} while (!strchr("yYnN", (c = getchar())));

		if (strchr("yY", c)) {
			FILE *db = ec_fopen(db_file_path, "wb");
			fwrite(&numberof_students, sizeof(numberof_students), 1, db);
			puts("... deleted!");
			fclose(db);
		}

		return 0;
	}

	assert(students, "[Error] Null input.\n\n", return 0);

	FILE *db = ec_fopen(db_file_path, "wb");

	fwrite(&numberof_students, sizeof(numberof_students), 1, db);

	forIn(student_index, 0, numberof_students) {
		/* First name. */
		fwrite(&students[student_index].firstName.len, sizeof(unsigned int), 1, db);
		fwrite(students[student_index].firstName.s, students[student_index].firstName.len + 1, 1, db);

		/* Last name. */
		fwrite(&students[student_index].lastName.len, sizeof(unsigned int), 1, db);
		fwrite(students[student_index].lastName.s, students[student_index].lastName.len + 1, 1, db);

		/* Grade. */
		fwrite(&students[student_index].grade, sizeof(float), 1, db);
	}

	fclose(db);

	return numberof_students;
}

unsigned int searchStudents(const char *db_file_path, const char *firstName, const char *lastName, unsigned int **matches)
{
	unsigned int numberof_students, numberof_matches = 0;
	Student *students;

	assert(numberof_students = retriveStudents(db_file_path, &students), "", return 0);

	mem_arr(unsigned int, *matches, numberof_students);

	forIn(student_index, 0, numberof_students) {
		if (
			!strcmp(students[student_index].firstName.s, firstName) &&
			!strcmp(students[student_index].lastName.s, lastName)
			)
		{
			(*matches)[numberof_matches++] = student_index;
		}
	}
	free_students(students, numberof_students);

	assert(numberof_matches, "No match found.\n", free(*matches));
	return numberof_matches;
}

void showStudents(const char *db_file_path)
{	
	unsigned int numberof_students = 0;
	Student *students;

	assert(numberof_students = retriveStudents(db_file_path, &students), "No students.\n", return);

	printf("Current students(%u): \n", numberof_students);
	put_students(students, numberof_students);

	free_students(students, numberof_students);
}


/* Get a name from db. Used only by retriveStudents. */
#define read_name(student_index, name_str) \
	fread(&(name_str.len), sizeof(unsigned int), 1, db); \
	mem_arr(char, (name_str.s), (name_str.len) + 1); \
	fread((name_str.s), 1, (name_str.len) + 1, db);

unsigned int retriveStudents(const char *db_file_path, Student **students)
{
	FILE *db = ec_fopen(db_file_path, "ab+");

	{ // EOF check
		char c = 0;
		fread(&c, sizeof(char), 1, db);

		if (feof(db)) {
			puts("Empty database.");
			fclose(db);
			return 0;
		}
		rewind(db);
	}

	unsigned int numberof_students = 0;

	/* Read the number students. */
	fread(&numberof_students, sizeof(int), 1, db);
	assert(numberof_students, "", fclose(db); return 0);

	/* Read the students. */
	mem_arr(Student, *students, numberof_students);

	forIn(student_index, 0, numberof_students) {
		read_name(student_index, (*students)[student_index].firstName);
		read_name(student_index, (*students)[student_index].lastName);
		fread(&(*students)[student_index].grade, sizeof(float), 1, db);
	}

	fclose(db);
	return numberof_students;
}


unsigned int insertStudents(const char *db_file_path, const char *file_path)
{
	Student *old_students, *new_students, *students;
	unsigned int numberof_old_students = 0,
		numberof_new_students = 0,
		numberof_students = 0;

	/* Get new students. */
    numberof_new_students = readStudentsFile(&new_students, file_path);
	assert(numberof_new_students, "", return 0);

	/* Get old students. */
	numberof_old_students = retriveStudents(db_file_path, &old_students);

	/* Gather up new and old students in one array. */
	numberof_students = numberof_old_students + numberof_new_students;
	mem_arr(Student, students, numberof_students);

	arrcat(students, 0, new_students, numberof_new_students);
	free(new_students);

	if (numberof_old_students) {
		arrcat(students, numberof_new_students, old_students, numberof_old_students);
		free(old_students);
	}
	
	/* Write the array to db. */
	updateStudents(db_file_path, students, numberof_students);

	free_students(students, numberof_students);
	return numberof_students;
}

void put_students(Student *students, unsigned int numberof_students)
{
	for (unsigned int i = 0; i < numberof_students; ++i) {
		printf("%d: %20s \t %20s \t %5.2f \n",
			i + 1,
			students[i].firstName.s,
			students[i].lastName.s,
			students[i].grade);
	}
}


unsigned int readStudentsFile(Student **students, const char *file_path)
{
	FILE *input;
	unsigned int numberof_students, space_size = 30;

	input = ec_fopen(file_path, "r");
	file_isEmpty_(file_path, return 0);

	mem_arr(Student, *students, space_size);

	String token;
	int err = 0;
	for (numberof_students = 0; !feof(input); ++numberof_students) {

		/* Check if students array is big enough. */
		if (numberof_students >= space_size) {
			space_size += 30;
			*students = (Student *)ec_realloc(students, space_size);
		}

		/* First name. */
		assert_ftok(input, (*students)[numberof_students].firstName, break);

		/* Last name. */
		assert_ftok(input, (*students)[numberof_students].lastName, ++err; break);

		/* Grade. */
		assert_ftok(input, token, ++err; break);
		strtod_(token.s,
			(*students)[numberof_students].grade,
			read_csv, free(token.s); ++err; break);
	}

	fclose(input);

	assert(!err, "",
		printf("[Error] Reading student data from file \"%s\" failed.\n", file_path);
		free_students(*students, numberof_students + 1);
		*students = NULL;
		return 0);

	return numberof_students;
}


void showDatabasesMenu()
{
	puts("Choose database");
	if (rows) {
		TAB(1); puts("Previously used databases:");
		forIn(database_index, 0, rows) {
			TAB(5); printf("%u. %s\n", database_index + 1, databases[database_index]);
		}
	}
	TAB(1); puts("o. Other");
	TAB(1); puts("0. Exit\n");
}


void saveDatabasesHistory(const char *history_file_path)
{
	puts("Saving database history ...");
	assert(rows, "Nothing to save.\n", return);

	FILE *fd = ec_fopen(history_file_path, "w");
	fprintarr(fd, "%s\n", databases, rows);
	fclose(fd);
}

void readDatabasesHistory(const char *history_file_path)
{
	FILE *fd = ec_fopen(history_file_path, "a+");
	file_isEmpty(fd, history_file_path, rows = 0; return);
	fclose(fd);

	fd = ec_fopen(history_file_path, "r");

	String temp;
	forIn(i, 0, rows) {
		assert((temp = ftok(fd, "\n \0")).len, "", rows = i; break);
		databases[i] = temp.s;
	}

	if (rows) {
		fclose(fd);
	}
}

int setActiveDB(char *db_file_path)
{
	assert(db_file_path, "[Error] Empty db_file_path\n", exit(-1));
	if (rows >= history_limit) { --rows; } // Overwrite the last entry.
	databases[rows++] = db_file_path;
	active_db_index = rows - 1;
	return 0;
}

int changeActiveDB()
{
#ifdef __linux__
	system("clear");
#endif
#ifdef _WIN32
	system("cls");
#endif

	showDatabasesMenu(); $_;

	int c;
	while ((c = getchar()) != EOF) {

		if (c != '\n') {
			if (isdigit(c)) {
				if (c - '0' > rows || c == '0') {
					puts("Invalid option.");
					continue;
				}
				active_db_index = c - '0' - 1;
				return active_db_index;
			}
			else {
				switch (c)
				{
				case 'o':
					printf("Insert database file path: ");
					setActiveDB((ftok(stdin, ",!?#&*^$()<>\n ")).s);
					return 0;

				case 'p':
					showDatabasesMenu();
					break;
				case 'c':
#ifdef __linux__
					system("clear");
#endif
#ifdef _WIN32
					system("cls");
#endif
					showDatabasesMenu();
					break;

				case '0':
					return -2;
				case 'q':
					return -2;

				default:
					puts("Invalid option.");
					break;
				}
			}
			$_;
		}
	}
	return -1;
}


void sort()
{
#ifdef __linux__
	system("clear");
#endif
#ifdef _WIN32
	system("cls");
#endif

	showSortMenu(); $_;

	int c;
	while ((c = getchar()) != EOF) {

		if (c != '\n') {

			switch (c) {
			case '1':
				NEWLINE; // by firstName.
				sortStudents(databases[active_db_index], byfirstName);
				showStudents(databases[active_db_index]);
				NEWLINE;

				break;

			case '2': // by lastName.
				NEWLINE;
				sortStudents(databases[active_db_index], bylastName);
				showStudents(databases[active_db_index]);
				NEWLINE;

				break;

			case '3': // by grade.
				NEWLINE;
				sortStudents(databases[active_db_index], byGrade);
				showStudents(databases[active_db_index]);
				NEWLINE;

				break;

			case 'p':
				NEWLINE;
				showStudents(databases[active_db_index]);
				NEWLINE;
				break;

			case 'l': // Change active db
				changeActiveDB();
#ifdef __linux__
				system("clear");
#endif
#ifdef _WIN32
				system("cls");
#endif
				showSortMenu();

				break;

			case '0':
				return;

			case 'q':
				return;

			case 'Q':
				return;

				// clear screen & showMenu
			case 'c':
#ifdef __linux__
				system("clear");
#endif
#ifdef _WIN32
				system("cls");
#endif
				showSortMenu();

				break;

			default:
				puts("Invalid option.\n");

				break;
			}

			$_;
		}
	}
	return;
}

void showSortMenu()
{
	printf("Sort by:\
            \n1. First Name\
            \n2. Last Name\
            \n3. Grade\
            \np. Print students\
			\nl. Load database\
            \n0. Exit");

	while (active_db_index < 0) {
		if (-2 == changeActiveDB()) {
			exit(0);
		}
	}
	printf("\n\nActive database: %s\n\n", databases[active_db_index]);
}


void sortStudents(const char *db_file_path, int(*compare)(void *, void *))
{
	unsigned int numberof_students = 0;
	Student *students;

	assert(numberof_students = retriveStudents(db_file_path, &students), "No students.\n", return);

	//qsort(students, numberof_students, sizeof(students[0]), compare);
	qsort_(students, numberof_students, compare);

	updateStudents(db_file_path, students, numberof_students);
	free(students);
}


int byfirstName(void *s1, void *s2)
{
	return strcmp((*(Student *)s1).firstName.s, (*(Student *)s2).firstName.s);
}
int bylastName(void *s1, void *s2)
{
	return strcmp((*(Student *)s1).lastName.s, (*(Student *)s2).lastName.s);
}
int byGrade(void *s1, void *s2)
{
	float res = (*(Student *)s1).grade - (*(Student *)s2).grade;

	if (res > 0.0f) {
		return 1;
	}
	else if (res < 0.0f) {
		return -1;
	}

	return 0;
}


void studentswap(Student *students, int i1, int i2)
{
	Student aux = students[i1];
	students[i1] = students[i2];
	students[i2] = aux;
}

void qsort_body(Student *students, long left, long right, int(*cmp)(void *, void *))
{
	if (left >= right - 1) {
		return;
	}

	long last;

	/* Pivot */
	studentswap(students, left, (left + right) / 2);

	/* Parition */
	last = left;

	for (long i = left + 1; i < right; i++) {
		if ( (*cmp)((void *)(students + left), (void *)(students + i)) > 0) {
			studentswap(students, ++last, i);
		}
	}

	studentswap(students, left, last);

	if (last > left) {
		qsort_body(students, left, last, cmp);
	}

	if (last < right) {
		qsort_body(students, last + 1, right, cmp);
	}

}

void qsort_(Student *students, int numberof_students, int(*cmp)(void *, void *))
{
	return qsort_body(students, 0, numberof_students, cmp);
}


void free_students(Student *students, unsigned int numberof_students)
{
	forIn(student_index, 0, numberof_students) {
		assert(students[student_index].firstName.s != NULL, "", continue);
		free(students[student_index].firstName.s);

		assert(students[student_index].lastName.s != NULL, "", continue);
		free(students[student_index].lastName.s);
	}
	free(students);
}

String ftok(FILE *fd, const char *del)
{
	String token;
	unsigned int size = 30, s_index = 0;
	char c;

	mem_arr(char, token.s, size);

	/* Ignore initial delimiters. */
	while ((c = fgetc(fd)) != EOF && strchr(del, c)) { ; }

	if (c == EOF) {
		free(token.s);
		return (String){ NULL, 0 };
	}

	do {
		if (s_index >= size) {
			token.s = (char *)ec_realloc(token.s, size += 30);
		}
		*(token.s + s_index) = c;
		++s_index;
	} while ((c = fgetc(fd)) != EOF && !strchr(del, c));

	token.s = (char *)ec_realloc(token.s, s_index + 1); // "Tight fitting" string.
	token.s[s_index] = 0; // End string.
	token.len = s_index;

	return token;
}