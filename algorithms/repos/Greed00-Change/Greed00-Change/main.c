/*
* DONE!
*/

#include<stdlib.h>
#include<stdio.h>
#include<stdarg.h>

#define NEWLINE puts("");

void *ec_malloc(unsigned long size);
void *ec_realloc(void *ptr, unsigned long size);
void cleanup(unsigned int, ...);

#define mem_arr(ptr, size, type) ptr = (type *) ec_malloc(size * sizeof(type))

/* Read array. */
void readarr(int[], unsigned long);

/* Print array. */
void printarr(int[], unsigned long);


#define arrswap(arr, pos_a, pos_b, type) { \
    type aux = arr[pos_b];\
    arr[pos_b] = arr[pos_a];\
    arr[pos_a] = aux;\
}

int isSmaller(void *a, void *b)
{
	int *x = (int *)a;
	int *y = (int *)b;

	if (*x > *y) {
		return *x;
	}
	if (*x < *y) {
		return *x - *y;
	}

	return 0;
}


int get_change(int coin_types[], unsigned int nr_coin_types, int change, int solution[]);


#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"

/* BEIGN:  Error codes. */

#define  SUCCESS      (0)
#define  NULL_INPUT   (1)
#define  NULL_OUTPUT  (2)
#define  DOMAIN_ERR   (3)   // Input does not belong to the domanin of the function.
#define  NOT_FOUND    (404)
#define  FOUND        (-404)
#define  TRUE         ('t')
#define  NIL          ('0')

/* END_OF: Error codes. */

/* Used for signaling errors. (ex: strtol_ sets errno_ to DOMAIN_ERR if conversion failed. */
int errno_ = 0;  // Error codes are the ones defined above.
char errplace_[100] = "";
char errmsg_[100] = "";


void critical_fail_msg() {
	printf("[Error] %s(): ", errplace_);
	switch (errno_) {
	case NULL_INPUT:
		printf("Null input. - %s returned true.\n", errmsg_);
		break;
	case NULL_OUTPUT:
		printf("%s returned NULL.\n", errmsg_);
		break;
	case DOMAIN_ERR:
		printf("Domain error. - %s\n", errmsg_);
		break;
	}
}


int null_check(unsigned int argc, ...);
int strchr_(char *s, char c);

int strcpy_(char *target, char *src);
int strncpy_(char *target, char *src, int nchars);
int strncpy_until_chr(char *target, char *src, int nchars, char end_char, char secure);


int strcat_(char *, char *);
int strncat_(char *target, char *src, int nchars);
int strncat_until_chr(char *target, char *src, int nchars, char end_char);

int strcpy_va(char *target, unsigned int n_sources, ...);

#define assert_critical( calling_fun, type, expr, expected_output, __errno__) { \
        errno_ = 0;\
        type output = (expr);\
        if (output != expected_output) {\
            errno_ = __errno__;\
            strcpy_(errplace_, #calling_fun);\
            strcpy_(errmsg_, #expr);\
            critical_fail_msg();\
            cleanup(0);\
            exit(__errno__);\
        }}

#define assert(calling_fun, type, expr, expected_output) { \
        errno_ = 0;\
        type output = (expr);\
        if (output != expected_output) {\
            printf(ANSI_COLOR_RED "%s returned %s. Expected %s. - In %s(). [Failed]" ANSI_COLOR_RESET "\n",\
                   #expr,\
                   "...",\
                   #expected_output,\
                   #calling_fun\
                );\
        }\
        else puts(ANSI_COLOR_GREEN "Test passed. [OK]" ANSI_COLOR_RESET);}



int main()
{
	int nr_coin_types, required_change, nr_coins_for_change;
	int *coin_types, *change;

	printf("Nr. of coin types: ");
	scanf("%d", &nr_coin_types);

	// Allocate memory for the coin types.
	mem_arr(coin_types, nr_coin_types, int);

	// Read and sort the coin types.
	readarr(coin_types, nr_coin_types);
	qsort(coin_types, nr_coin_types, sizeof(int), isSmaller);


	printf("Change: ");
	scanf("%d", &required_change);

	// Allocate memory for the change.
	mem_arr(change, required_change * nr_coin_types, int);

	// Get the change.
	nr_coins_for_change = get_change(coin_types, nr_coin_types, required_change, change);


	// Print the change.
	if (nr_coins_for_change) {
		printf("The minimum number of coins to form the required change: %d\n", nr_coins_for_change);

		printarr(change, nr_coins_for_change);
	}


	// Keep console window open.
	getchar();


	// Free memory.
	cleanup(2, coin_types, change);

	return 0;

}

int get_change(int coin_types[], unsigned int nr_coin_types, int required_change, int change[])
{
	assert_critical(get_change, int, null_check(2, coin_types, change), !TRUE, NULL_INPUT);

	int current_change = 0, current_coin_type = nr_coin_types - 1, nth_coin = 0;

	/* Loop until current_change == change.*/
	while (current_change < required_change && current_coin_type >= 0) {

		/* Add current type of coin to current change, if possible. */
		while (current_change + coin_types[current_coin_type] <= required_change) {
			current_change += coin_types[current_coin_type];

			change[nth_coin++] = coin_types[current_coin_type];
		}

		/* If not try using the next type of coin. */
		current_coin_type--;
	}

	if (current_change < required_change) {
		printf("The required change %d, cannot be formed with the given coin types: ", required_change);
		printarr(coin_types, nr_coin_types);
		return 0;
	}

	return nth_coin;
}


void readarr(int arr[], unsigned long n)
{
	for (unsigned long i = 0; i < n; i++) {
		scanf("%d", arr + i);
	}
}

void printarr(int arr[], unsigned long n)
{
	for (unsigned long i = 0; i < n; i++) {
		printf("%d ", arr[i]);
	}
	NEWLINE;
}


void *ec_realloc(void *ptr, unsigned long size)
{
	void *new_ptr;

	new_ptr = realloc(ptr, size);

	if (new_ptr == NULL) {
		puts("[Error] ec_realloc(): realloc() returned NULL.");
	}

	return new_ptr;
}
void *ec_malloc(unsigned long size)
{
	return ec_realloc(NULL, size);
}

void cleanup(unsigned int argc, ...)
{
	va_list args;

	va_start(args, argc);

	for (unsigned int nth_arg = 0; nth_arg < argc; ++nth_arg) {
		free(va_arg(args, void*));
	}

	va_end(args);
}


int null_check(unsigned int argc, ...)
{
	va_list args;

	va_start(args, argc);

	for (unsigned int nth_arg = 1; nth_arg <= argc; ++nth_arg) {
		if (!va_arg(args, void*)) {
			va_end(args);

			return TRUE;
		}
	}

	va_end(args);

	return !TRUE;
}

int strcpy_(char *target, char *src)
{
	return strncpy_until_chr(target, src, -1, '\0', 0);
}


/* If (nchars > 0) copy at most nchars from =src= to =target=,
* else if (nchars < 0) copy =src= to =target=. */
int strncpy_(char *target, char *src, int nchars)
{
	return strncpy_until_chr(target, src, nchars, '\0', 0);
}

int strcpy_va(char *target, unsigned int n_sources, ...)
{
	va_list sources;

	va_start(sources, n_sources);

	for (unsigned int nth_source = 1; nth_source <= n_sources; ++nth_source) {
		strcpy_(target, va_arg(sources, char*));
	}

	return SUCCESS;
}

int strncpy_until_chr(char *target, char *src, int nchars, char end_char, char secure)
{
	/* Secure "true" means the calling function checked the parmeters before calling. */
	if (!secure) {
		assert_critical("strncpy_", int, null_check(2, target, src), !TRUE, NULL_INPUT);
	}

	/* Copy until the end of =src=. */
	if (nchars < 0 && end_char <= '\0') {
		while ((*target++ = *src++)) { ; }
		return SUCCESS;
	}


	/* Copy until =end_char= is encountered. */
	if (nchars < 0) {

		while ((*target = *src) && *target != end_char) {
			target++;
			src++;
		}

		/* End string if needed. */
		if (*target) {
			*target = '\0';
		}
		return SUCCESS;
	}


	/* Copy nchars. */
	if (end_char <= '\0') {

		for (int iterator = nchars; *src && iterator; --iterator) {
			*target++ = *src++;
		}

		/* End string if needed. */
		if (*target) {
			*target = '\0';
		}

		return SUCCESS;
	}



	/* Copy nchars until end_char is encountered. */
	while ((*target = *src) && (*target != end_char) && nchars--) {
		target++;
		src++;
	}

	/* End string if needed. */
	if (*target) {
		*target = '\0';
	}

	return SUCCESS;
}


int strcat_(char *target, char *src)
{
	return strncat_until_chr(target, src, -1, '\0');
}

int strncat_(char *target, char *src, int nchars)
{
	return strncat_until_chr(target, src, nchars, '\0');
}

int strncat_until_chr(char *target, char *src, int nchars, char end_char)
{
	assert_critical("strncpy_", int, null_check(2, target, src), !TRUE, NULL_INPUT);

	/* Skip until the end of =target= */
	while (*target) {
		++target;
	}

	/* Copy starting from the end of =target=, the string =src=. */
	return strncpy_until_chr(target, src, nchars, end_char, 's'); // s stands for secure
}




/* Similar to strchr but in case of success
* it returns an int value, insted of a pointer.
*/
int strchr_(char *s, char c)
{
	if (!s) {
		errno_ = NULL_INPUT;
		return NULL_INPUT;
	}
	while (*s && *s != c) {
		s++;
	}
	return *s;
}