#include "stdio_wrap.h"
#include "stdlib_wrap.h"
#include "limits.h"
#include "stdint.h"
#include "time.h"


/* swap: interchange v[i] and v[j] */
void swap(int [], int, int);
void quicksort(int [], int, int);
int rand_n(int);
int rand_range(int, int);
void inter_sort(int [], int);

#define INPUT "input.txt"

int itr_perf = 0;

int partition(int a[], int lo, int hi)
{ // Partition into a[lo..i-1], a[i], a[i+1..hi].
	int i = lo, j = hi + 1; // left and right scan indices
	int v = a[lo]; // partitioning item

	while (true)
	{ // Scan right, scan left, check for scan complete, and exchange.
		while (a[++i] < v) if (i == hi) break;
		while (v < a[--j]) if (j == lo) break;
		if (i >= j) break;
		swap(a, i, j);
	}
	swap(a, lo, j); // Put v = a[j] into position
	return j; // with a[lo..j-1] <= a[j] <= a[j+1..hi].
}

void qsort_(int a[], int lo, int hi)
{
	if (hi <= lo) return;

	int j = partition(a, lo, hi); // Partition (see page 291).

	qsort_(a, lo, j - 1); // Sort left part a[lo .. j-1].
	qsort_(a, j + 1, hi); // Sort right part a[j+1 .. hi].
}

int main()
{
	int arr[100000];
	size_t n = 100000;

	file_isEmpty_(INPUT, return 0);

	FILE *input = ec_fopen(INPUT, "r");
	freadarr(input, "%d", arr, n);
	fclose(input);

	inter_sort(arr, n);
	printf("Insertion-sort iterations: %d\n", itr_perf);

	quicksort(arr, 0, n - 1);
	printf("Quicksort iterations: %d\n", itr_perf);


	FILE *out = ec_fopen("out.txt", "w");
	fprintarr(out, "%d ", arr, n);
	fclose(out);

	return 0;
}



void quicksort(int arr[], int left, int right)
{
	if (left >= right) {
		return;
	}

	++itr_perf;

	int pivot_index, last;
	
	pivot_index = rand_range(left, right);
	last = left;

	swap(arr, left, pivot_index);

	for (int i = left + 1; i <= right; ++i) {
		if (arr[i] < arr[left]) {
			swap(arr, ++last, i);
		}
	}

	swap(arr, left, last);
	quicksort(arr, left, last - 1);
	quicksort(arr, last + 1, right);

}



void inter_sort(int arr[], int nelems)
{
	int key, unsorted_index, sorted_index;

	itr_perf = 0;

	for (
		unsorted_index = 1;
		unsorted_index < nelems;
		++unsorted_index
		)
	{
		key = arr[unsorted_index];

		for (
			sorted_index = unsorted_index - 1;
			sorted_index >= 0;
			--sorted_index
			)
		{
			if (arr[sorted_index] < key) {
				break;
			}

			arr[sorted_index + 1] = arr[sorted_index];
		}

		arr[sorted_index + 1] = key;
		++itr_perf;
	}
}

/* qsort: sort v[left]...v[right] into increasing order */

int rand_range(int l, int r)
{
	int rand = rand_n(r - l);

	return (rand < l) ? l + rand : rand;
}

int rand_n(int n)
{
	static init;

	if (!init) {
		srand((unsigned int) time(NULL));
	}

	return rand() % (n + 1);
}

/* swap: interchange v[i] and v[j] */
void swap(int v[], int i, int j)
{
	int temp;
	temp = v[i];
	v[i] = v[j];
	v[j] = temp;
}
