#include "stdio_wrap.h"

FILE *ec_fopen(const char *file_path, const char *mode)
{
	FILE *fd;

	fd = fopen(file_path, mode);

	if (NULL == fd) {
		puts("[Error] fopen() returned NULL.");
		exit(RESOURCE_ACQUISITION_FAILED);
	}

	return fd;
}