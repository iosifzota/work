#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <limits.h>


/* Utitlites. TO BE IGNORED. */
/* ======================================================================== */
#define NEWLINE puts("");

#define NELEMS(x) (sizeof(x)/sizeof(x[0]))
#define max(x,y) (((x) > (y)) ? (x) : (y))
#define min(x,y) (((x) > (y)) ? (y) : (x))

/* For itr = [start, end]; */
#define forIn(itr, start, end) \
    for (unsigned int itr = (start); (itr) < (end); ++(itr))

#define assert(expr, err_msg, err_expr)         \
    if (!(expr)) {                              \
        printf(err_msg);						\
        err_expr;                               \
    }

/* Error codes. */
enum errcode {
	SUCCESS,
	ERROR,
	NULL_INPUT = -'I',
	DOMAIN_ERR,  // Input does not belong to the domanin of the function.
	NOT_FOUND = 404,
	FOUND = -404,
	NIL = '0',
	RESOURCE_ACQUISITION_FAILED = 7
};
typedef enum errcode errcode;


/* Error checked malloc, realloc and calloc. */
void *ec_malloc(size_t);
void *ec_realloc(void *, size_t);
void *ec_calloc(size_t, size_t);

/* Allocate memory for an array of the given type. */
#define mem_arr(type, ptr, size) \
    (ptr) = (type *) ec_malloc((size) * sizeof(type))
#define mem_arr_calloc(type, ptr, size) \
    (ptr) = (type *) ec_calloc(size, sizeof(type));

/* Allocate a matrix of any type. */
#define mem_matrix(type, matrix, rows, cols)    \
    mem_arr(type*, matrix, rows);               \
    forIn (i, 0, rows) {                        \
        mem_arr(type, matrix[i], cols);         \
    }

/* Deallocate a matrix of any type. */
#define free_matrix(matrix, rows)               \
    forIn (i, 0, rows) {                        \
        free(matrix[i]);                        \
    }                                           \
    free(matrix);

/* Read array. */
#define freadarr(fd, format_specifier, arr, nelems)  do {       \
        forIn (i, 0, nelems) {                                  \
            if (EOF == fscanf(fd, format_specifier, arr + i)) { \
                nelems = i; break;                              \
            }                                                   \
        }                                                       \
    } while(0)
/* Print array. */
#define fprintarr(fd, format_specifier, arr, nelems)    \
    forIn (i, 0, nelems) {                              \
        fprintf(fd, format_specifier, *(arr + i));      \
    }                                                   \
    NEWLINE;                                            \

/* Read matrix. */
#define freadmat(fd, format_specifier, matrix, rows,  cols)  do {							\
	int err = 0;																			\
        forIn (ln_index, 0, rows) {															\
            forIn (col_index, 0, cols) {													\
                if ( EOF == fscanf(fd, format_specifier,matrix[ln_index] + col_index)) {	\
                    if ((!col_index && !ln_index) || col_index) cols = col_index;			\
                    err = 1;																\
                }																			\
            }																				\
            if (err) { rows = ln_index; break; }											\
        }																					\
    } while(0)
/* Print matrix. */
#define fprintmat(fd, format_specifier, matrix, rows,  cols)				\
    NEWLINE;																\
    forIn (ln_index, 0, rows) {												\
        forIn (col_index, 0, cols) {										\
            fprintf(fd, format_specifier " ",matrix[ln_index][col_index]);	\
        }																	\
        NEWLINE;															\
    }																		\
    NEWLINE;
/* ======================================================================== */

typedef struct Matrici {
	int **N;
	int **E;
	int **V;
	int **S;
} Matrici;

Matrici mat_swap_digit0(int **mat, int rows, int cols);
void matcpy(int **new_mat, int **mat, int rows, int cols);
void mat_find_digit0(int **mat, int rows, int cols, int *digit0_row, int *digit0_col);
void matswap(int **mat, int row, int col, int new_row, int new_col);



/* BEGIN */
int main()
{
	int **mat;
	unsigned int rows, cols;

	printf("Rows: ");
	scanf("%d", &rows);

	printf("Cols: ");
	scanf("%d", &cols);

	mem_matrix(int, mat, rows, cols);
	freadmat(stdin, "%d", mat, rows, cols);

	Matrici matrici = mat_swap_digit0(mat, rows, cols);

	if (matrici.N) {
		fprintmat(stdout, "%3d", matrici.N, rows, cols);
	}
	if (matrici.S) {
		fprintmat(stdout, "%3d", matrici.S, rows, cols);
	}
	if (matrici.V) {
		fprintmat(stdout, "%3d", matrici.V, rows, cols);
	}
	if (matrici.E) {
		fprintmat(stdout, "%3d", matrici.E, rows, cols);
	}

	free_matrix(mat, rows);
	return 0;
}

#define swap_digit0(dir, new_row, new_col) do {		\
		int **new_mat;							\
		mem_matrix(int, new_mat, rows, cols);	\
		matcpy(new_mat, mat, rows, cols);		\
		matswap(new_mat, digit0_row, digit0_col, new_row, new_col);\
		matrici.dir = new_mat;\
	} while (0);

Matrici mat_swap_digit0(int **mat, int rows, int cols)
{
	int digit0_row, digit0_col;
	Matrici matrici;

	mat_find_digit0(mat, rows, cols, &digit0_row, &digit0_col);

	// NORTH
	if (digit0_row - 1 >= 0) {
		swap_digit0(N, digit0_row - 1, digit0_col);
	}
	else {
		matrici.N = NULL;
	}
	// EAST
	if (digit0_col - 1 >= 0) {
		swap_digit0(E, digit0_row, digit0_col - 1);
	}
	else {
		matrici.E = NULL;
	}
	// WEST
	if (digit0_col + 1 < cols) {
		swap_digit0(V, digit0_row, digit0_col + 1);
	}
	else {
		matrici.V = NULL;
	}
	// SOUTH
	if (digit0_row + 1 < rows) {
		swap_digit0(S, digit0_row + 1, digit0_col);
	}
	else {
		matrici.S = NULL;
	}

	return matrici;
}


void matcpy(int **new_mat, int **mat, int rows, int cols)
{
	forIn(row, 0, rows) {
		forIn(col, 0, cols) {
			new_mat[row][col] = mat[row][col];
		}
	}
}

void matswap(int **mat, int row, int col, int new_row, int new_col)
{
	int aux = mat[row][col];
	mat[row][col] = mat[new_row][new_col];
	mat[new_row][new_col] = aux;
}

_Bool has_digit(int number, int digit)
{
	if (!number) {
		return true;
	}
	while (number) {
		if (number % 10 == 0) {
			return true;
		}
		number /= 10;
	}
	return false;
}

void mat_find_digit0(int **mat, int rows, int cols, int *digit0_row, int *digit0_col)
{
	forIn(row, 0, rows) {
		forIn(col, 0, cols) {
			if (has_digit(mat[row][col], 0)) {
				*digit0_col = col;
				*digit0_row = row;
				return;
			}
		}
	}

	*digit0_col = -1;
	*digit0_row = -1;
}

/* END */











/* Utilites. TO BE IGNORED. */
/* ======================================================================== */

/* Memory management. */
#define assert_alloc(allocator_name)                            \
    assert((new_ptr != NULL),                                   \
           "[Error] " #allocator_name "() returned NULL.\n",    \
           exit(RESOURCE_ACQUISITION_FAILED));
void *ec_calloc(size_t nitems, size_t size)
{
	void *new_ptr;

	new_ptr = calloc(nitems, size);
	assert_alloc(calloc);

	return new_ptr;
}
void *ec_realloc(void *ptr, size_t size)
{
	void *new_ptr;

	new_ptr = realloc(ptr, size);
	assert_alloc(realloc);

	return new_ptr;
}
void *ec_malloc(size_t size)
{
	return ec_realloc(NULL, size);
}
/* ======================================================================== */
