#include <stdwrap.h>

int order(void *number1, void *number2)
{
	return *(uint32_t *)number1 - *(uint32_t *)number2;
}

int arr_sum(uint32_t arr[], uint32_t nelems);

uint32_t arr_last_N(uint32_t arr[], uint32_t nelems);

#define arrcpy(t, s, s_nelems) \
		forIn (index, 0, s_nelems) {  \
			t[index] = s[index];	\
		}

#define binSearch(type, index, key, arr, nelems, compare) do {\
		type * pointer = (type *)bsearch(                 \
						&(type){ key },      \
						arr,                 \
						nelems,              \
						sizeof(type),        \
						order);                \
		index = (pointer) ? pointer - arr : -1; \
	} while(0)

void max_books(uint32_t l_shelf, uint32_t nbooks, uint32_t *book_lens, uint32_t **sol);

int main()
{
	uint32_t nbooks, l_shelf, *book_lens, **sol;

	FILE *input;
	input = ec_fopen("input.txt", "r");

	fscanf(input, "%u", &nbooks);

	/* Read the book lengths. */
	mem_arr(uint32_t, book_lens, nbooks);
	freadarr(input, "%u", book_lens, nbooks);

	printf("Books: ");
	fprintarr(stdout, "%u", book_lens, nbooks);

	/* Read the shelf length. */
	fscanf(input, "%u", &l_shelf);
	mem_matrix_calloc(uint32_t, sol, l_shelf, nbooks);

	NEWLINE;
	max_books(l_shelf, nbooks, book_lens, sol);

	fprintarr(stdout, "%u", sol[l_shelf - 1], nbooks);

	free_matrix(sol, l_shelf);
	free_va(1, book_lens);

	return 0;
}

void max_books(uint32_t l_shelf, uint32_t nbooks, uint32_t *book_lens, uint32_t **sol)
{
	if (l_shelf > 1) {
		max_books(l_shelf - 1, nbooks, book_lens, sol);
	}
	else {
		uint32_t candidate_index;
		binSearch(uint32_t, candidate_index, l_shelf, book_lens, nbooks, order);

		if (candidate_index != -1) {
			sol[l_shelf - 1][candidate_index] = 1;
		}

		return;
	}

	uint32_t sol_sum;

	forIn(sol_index1, 0, (l_shelf)/2) {

		/* Walk backwards starting from half. */
		uint32_t sol_index = (l_shelf)/2 - sol_index1 - 1;

		/* Calculate previous solution sum. */
		sol_sum = sol_index + 1;

		if ( -1 != arr_last_N(sol[sol_index], nbooks) && sol_sum <= l_shelf/2 + 1) {

			/* Find complemenatry book to fill current l_shelf, if any. */
			forIn(candidate_index, arr_last_N(sol[sol_index], nbooks) + 1, nbooks) {

				/* Ignore books with lengths bigget then the current l_shelf. */
				if (book_lens[candidate_index] >= l_shelf) {
					break;
				}

				if (sol_sum + book_lens[candidate_index] == l_shelf) {

					/* Complementary book was found, so copy to the solution for the current l_shelf
					 * the found book_len and also the previous solution that this book complemented. */
					arrcpy(sol[l_shelf - 1], sol[sol_index], nbooks);
					sol[l_shelf - 1][candidate_index] = book_lens[candidate_index];

					return;
				}
			}

		}
	}

	/* No complementary found. Check IF a book has the current l_shelf. */
	uint32_t candidate_index;
	binSearch(uint32_t, candidate_index, l_shelf, book_lens, nbooks, order);

	if (candidate_index != -1) {
		/* A book with length == l_shelf was found. */
		sol[l_shelf - 1][candidate_index] = book_lens[candidate_index];
	}
	/* ELSE, current l_shelf cannot be fully filed with the given books. :P */
}

int arr_sum(uint32_t arr[], uint32_t nelems)
{
	int sum = 0;

	forIn(index, 0, nelems) {
		sum += arr[index];
	}

	return sum;
}


uint32_t arr_last_N(uint32_t arr[], uint32_t nelems)
{
	forIn(index, 0, nelems) {
		if (arr[nelems - index - 1]) {
			return nelems - index - 1;
		}
	}

	return -1;
}
