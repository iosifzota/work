#include <stdlib.h>
#include <stdio.h>

unsigned int maxlen(int[], unsigned int, int[], unsigned int);

void
readarr(
	int arr[], unsigned int nelems, FILE *fp
)
{
	for (unsigned int index = 0; index < nelems; ++index) {
		fscanf(fp, "%d", arr + index);
	}
}

void
printarr(
	int arr[], unsigned int nelems
)
{
	for (unsigned int index = 0; index < nelems; ++index) {
		printf("%d ", arr[index]);
	}
	puts("");
}

int
longest_ordered_subarray(
	int arr[], unsigned int nelements, int lengths[], int solution[], int element_index
)
{
	if (element_index < 0)
		return 0;

	/* Find the max of the next possible lengths. */
	unsigned int maxlen_index = maxlen(lengths, element_index, arr, nelements);

	/* Add the found max to the current length. */
	lengths[element_index] = 1 + lengths[maxlen_index];

	/* Update solution */
	solution[element_index] = maxlen_index;

	/* Keep at it 'til then end of array. */
	return longest_ordered_subarray(arr, nelements, lengths, solution, --element_index);
}




int main(void)
{
	int *arr, *s, *l;
	unsigned int nelems;

	FILE *fp;

	fp = fopen("input.txt", "r");

	fscanf(fp, "%d", &nelems);

	arr = (int *)malloc(nelems * sizeof(int));
	s = (int *)malloc(nelems * sizeof(int));
	l = (int *)malloc(nelems * sizeof(int));

	if (!arr) {
		puts("NULL");
		exit(1);
	}


	readarr(arr, nelems, fp);

	printarr(arr, nelems);


	longest_ordered_subarray(arr, nelems, l, s, 8);

	printarr(l, nelems);
	printarr(s, nelems);


	return 0;
}

unsigned int
maxlen(
	int lengths[], unsigned int starting_index, int arr[], unsigned int nelements
)
{
	/* The max of the next possible lengths */
	unsigned int maxlen_index = starting_index;
	int maxlen = 0;

	if (starting_index == nelements - 1) {
		lengths[starting_index] = 0;

		return (starting_index);
	}

	/* The current element to calculate maxlen for. */
	int pivot = arr[starting_index];


	for (
		unsigned int index = starting_index + 1;
		index < nelements;
		++index
		)
	{
		if (arr[index] >= pivot && lengths[index] > maxlen) {
			maxlen_index = index;
			maxlen = lengths[maxlen_index];
		}
	}

	return maxlen_index;
}


