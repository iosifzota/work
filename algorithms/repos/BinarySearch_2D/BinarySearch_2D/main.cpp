#include <iostream>
#include <cstdlib>

#define NEWLINE std::cout<<std::endl;

void *ec_realloc(void *ptr, unsigned int size);
void *ec_malloc(unsigned int size);

int **malloc_2D_int(unsigned int lines, unsigned int cols);
void free_2D_int(int **__2D, unsigned int cols);


char strchr_(char *s, char c);

inline int fgetc_except(FILE *file, char *except);
unsigned int fget_uint(FILE *file);


int init()
{
	FILE *file;

	int **matrix;

	unsigned int lines, cols;

	if (NULL == (file = fopen("Matrice.txt", "r"))) {
		printf("[Error] fopen() returned NULL.");
		exit(-1);
	}


	lines = fget_uint(file);
	cols = fget_uint(file);

	std::cout << "Linii: " << lines;
	NEWLINE;
	std::cout << "Coloane: " << cols;
	NEWLINE;

	// Aloca memorie pt cartiere.
	matrix = malloc_2D_int(lines, cols);

	for (unsigned int ln_index = 0; ln_index < lines; ++ln_index) {
		for (unsigned int col_index = 0; col_index < cols; ++col_index) {
			matrix[ln_index][col_index] = fget_uint(file);
		}
	}

	for (unsigned int ln_index = 0; ln_index < lines; ++ln_index) {
		for (unsigned int col_index = 0; col_index < cols; ++col_index) {
			std::cout << matrix[ln_index][col_index] << " ";
		}
		NEWLINE;
	}



	//free_2D_int(matrix, cols);

	fclose(file);

	return 0;
}

int main()
{

	return init();
}



int **malloc_2D_int(unsigned int lines, unsigned int cols)
{
	int **__2D;

	__2D = (int **) ec_malloc(lines * sizeof(int *));

	for (unsigned int index = 0; index < cols; ++index) {
		__2D[index] = (int *) ec_malloc(cols * sizeof(int));
	}

	return __2D;
}

void free_2D_int(int **__2D, unsigned int lines)
{
	for (unsigned int index = 0; index < lines; ++index) {
		free(__2D[index]);
	}

	free(__2D);
}


char strchr_(char *s, char c)
{
	while (*s && *s != c) {
		s++;
	}
	return *s;
}


unsigned int fget_uint(FILE *file)
{
	unsigned int number = 0;
	char c, found;

	if (!file) {
		perror("[Error] Null was passed as file descriptor.");
		exit(-1);
	}

	while ((c = fgetc_except(file, " \n")) != EOF) {
		number = (number * 10) + (c - '0');
		found = 1;
	}

	if (!found) {
		perror("[Error] Failed to get input from file.");
		exit(-1);
	}

	return number;
}

inline int fgetc_except(FILE *file, char *except)
{
	char c;
	if ( (c = fgetc(file)) != EOF && !strchr_(except, c) ) {
		return c;
	}
	return EOF;
}



void *ec_realloc(void *ptr, unsigned int size)
{
	void *new_ptr;

	new_ptr = realloc(ptr, size);

	if (!new_ptr) {
		printf("[Error] realloc() return NULL");
		exit(-1);
	}

	return new_ptr;
}
void *ec_malloc(unsigned int size)
{
	return ec_realloc(NULL, size);
}