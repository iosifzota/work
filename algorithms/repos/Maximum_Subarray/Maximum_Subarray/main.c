#include <stdio.h>
#include "misc.h"
#include "limits.h"

int max_subarray(int arr[], unsigned int nelems);

int main()
{
	int a[] = { 13, -3, -25, 20, -3, -16, -23, 18 , 20, -7, 12, -5, -22, 15, -4, 7 };

	max_subarray(a, NELEMS(a));

	return 0;
}


int max_subarray(int arr[], unsigned int nelems)
{
	int max_ending_here = 0, max_so_far = INT_MIN, s_index = 0, e_index = 0, temps_index = 0;

	forIn(arr_index, 0, nelems) {
		max_ending_here += arr[arr_index];

		if (max_ending_here > max_so_far) {
			max_so_far = max_ending_here;
			s_index = temps_index;
			e_index = arr_index;
		}

		if (max_ending_here < 0) {
			max_ending_here = 0;
			temps_index = arr_index + 1;
		}
	}

	printf("Maximum subarray: %d", max_so_far);
	printf(" (%d, %d)\n", s_index, e_index);

	return 0;
}
