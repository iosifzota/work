#include "stdio_wrap.h"
#include "stdlib_wrap.h"
#include "time.h"

int cmp(void *v1, void *v2)
{
	return *(int *)v1 - *(int *)v2;
}

int rand_n(int);
int rand_range(int, int);
void swap(int [], int, int);

void KR_rand_qsort(int [], int, int);
void KR_qsort(int [], int, int);

void inter_sort(int [], int);

void alg_rand_sort(int a[], int lo, int hi);
void alg_sort(int a[], int lo, int hi);

#define INPUT "nr.txt"
int itr_perf = 0;

int main()
{
	int test[] = {
		10, 9, 34, 14, 8, 6, 39, 1, 4, 34, 431, 43, 98, 35, 89, 349, 956, 5, 3, 43
	};
	//alg_rand_sort(test, 0, NELEMS(test) - 1);
	//fprintarr(stdout, "%d ", test, NELEMS(test));

	int arr[100000], n = 100000;
	FILE *input, *out;

	file_isEmpty_(INPUT, return 0); input = ec_fopen(INPUT, "r");
	freadarr(input, "%d", arr, n);
	fclose(input);

	itr_perf = 0;
	inter_sort(arr, n);
	printf("Insertion-sort iterations: %d\n", itr_perf);
	out = ec_fopen("qsort_res.txt", "w"); fprintarr(out, "%d ", arr, n); fclose(out);


	printf("Quicksort v1:\n");
	itr_perf = 0;
	KR_qsort(arr, 0, n - 1);
	printf("simple:      %d\n", itr_perf);

	//itr_perf = 0;
	//KR_rand_qsort(arr, 0, n - 1);
	//printf("randomized:  %d\n", itr_perf);

	NEWLINE;

	printf("Quicksort v2:\n");
	itr_perf = 0;
	alg_sort(arr, 0, n - 1);
	printf("simple:      %d\n", itr_perf);

	itr_perf = 0;
	alg_rand_sort(arr, 0, n - 1);
	printf("randomized:  %d\n", itr_perf);

	
	out = ec_fopen("out.txt", "w");
	fprintarr(out, "%d ", arr, n);
	fclose(out);

	FILE *verf, *res;

	verf = ec_fopen("qsort_res.txt", "r");
	res = ec_fopen("out.txt", "r");

	int a, b, c = 3;
	forIn(i, 0, n) {
		if (!c) {
			printf("\b\b\b");
			c = 3;
		}
		fscanf(verf, "%d", &a);
		fscanf(res, "%d", &b);
		if (a != b) {
			printf("%d: %d != %d\n", i, a, b);
		}
		else {
			printf("."); --c;
		}
	}

	fclose(verf); fclose(res);

	return 0;
}

 void alg_rand_sort(int a[], int lo, int hi)
{
	if (hi <= lo) {
		return;
	}

	// random pivot
	swap(a, lo, rand_range(lo, hi));

	int key = a[lo], last = lo, rest = hi;

	while (last < rest)
	{
		 //if (a[i] < key) ++last; (skip)
		 if (a[last + 1] > key) { 
			while (a[rest] > key && last < rest) {
				rest--;     ++itr_perf;
			}

			if (last < rest) {
				swap(a, ++last, rest--);
			}
		}
		 else { ++last; ++itr_perf; }
	} // Now a[lo..last] < key < a[rest+1..hi].

	swap(a, lo, last); // Place pivot
	alg_rand_sort(a, lo, last - 1);
	alg_rand_sort(a, rest + 1, hi);
}

	/*	
	printf("IN: ");
	fprintarr(stdout, "%d ", a+lo, hi - lo + 1);
	printf("%d: ", v);
	//*/


void alg_sort(int a[], int lo, int hi)
{
	if (hi <= lo) {
		return;
	}

	int key = a[lo], last = lo, rest = hi;

	while (last < rest)
	{
		 //if (a[i] < key) ++last; (skip)
		 if (a[last + 1] > key) { 
			while (a[rest] > key && last < rest) {
				++itr_perf;
				rest--;
			}

			if (last < rest) {
				swap(a, ++last, rest--);
			}
		}
		 else { ++last; ++itr_perf; }
	} // Now a[lo..last] < key < a[rest+1..hi].

	swap(a, lo, last); // Place pivot
	alg_sort(a, lo, last - 1);
	alg_sort(a, rest + 1, hi);
}



void KR_rand_qsort(int arr[], int left, int right)
{
	if (left >= right) {
		return;
	}

	int last = left;

	swap(arr, left, rand_range(left, right));

	for (int i = left + 1; i <= right; ++i) {
		if (arr[i] < arr[left]) {
			swap(arr, ++last, i);
		}
		++itr_perf;
	}

	swap(arr, left, last);
	KR_rand_qsort(arr, left, last - 1);
	KR_rand_qsort(arr, last + 1, right);
}


void KR_qsort(int v[], int left, int right)
{
	int i, last;
	void swap(int v[], int i, int j);
	if (left >= right) /* do nothing if array contains */
			return; /* fewer than two elements */
	swap(v, left, (left + right) / 2); /* move partition elem */
	last = left; /* to v[0] */
	for (i = left + 1; i <= right; i++) { /* partition */
		if (v[i] < v[left]) {
			swap(v, ++last, i);
		}
		++itr_perf;
	}
	swap(v, left, last); /* restore partition elem */
	KR_qsort(v, left, last - 1);
	KR_qsort(v, last + 1, right);
}


int rand_range(int l, int r)
{
	int rand = rand_n(r - l);

	return (rand < l) ? l + rand : rand;
}

int rand_n(int n)
{
	static init;

	if (!init) {
		srand((unsigned int) time(NULL));
	}

	return rand() % (n + 1);
}

/* swap: interchange v[i] and v[j] */
void swap(int v[], int i, int j)
{
	int temp;
	temp = v[i];
	v[i] = v[j];
	v[j] = temp;
}

void inter_sort(int arr[], int nelems)
{
	int key, unsorted_index, sorted_index;

	itr_perf = 0;

	for (
		unsorted_index = 1;
		unsorted_index < nelems;
		++unsorted_index
		)
	{
		key = arr[unsorted_index];

		for (
			sorted_index = unsorted_index - 1;
			sorted_index >= 0;
			--sorted_index
			)
		{
			++itr_perf;
			if (arr[sorted_index] < key) {
				break;
			}

			arr[sorted_index + 1] = arr[sorted_index];
		}

		arr[sorted_index + 1] = key;
	}
}