/*
* Nr_prim.cpp
*/
#include <iostream>
#include <cmath>

bool prim_clasic(int n, int &itr)
{
	if (n < 2 || (n>2 && n % 2 == 0))
		return 0;

	for (int i = 3; i <= sqrt(n); i += 2)
	{
		itr++;

		if (n % i == 0)
			return 0;
	}

	return 1;
}

int t_fermat(int x, int n, int &itr)
{
	int r = 1;

	for (int i = n - 1; i > 0; i = i / 2)
	{
		if (i % 2 == 1)
			r = x * r % n;

		x = x * x % n;

		itr++;
	}

	return r;
}

bool prim_rand(int n, int &itr)
{
	if (n < 2 || (n>2 && n % 2 == 0))
		return 0;

	for (int i = 0; i < 5; i++)
	{
		int x = rand() % (n - 1) + 1;

		if (t_fermat(x, n, itr) != 1)
			return 0;
	}

	return 1;

}

int t2(int x, int n, int &itr)
{
	int r = 1;

	for (int i = n - 1; i > 0; i = i / 2)
	{
		itr++;

		if (i % 2 == 1)
			r = x * r % n;

		int y = x;
		x = x * x % n;

		if (x == 1 && y != 1 && y != n - 1)
			return 1;
	}

	return r != 1;
}

bool prim_rand2(int n, int &itr)
{
	if (n < 2 || n>2 && n % 2 == 0)
		return 0;

	for (int i = 0; i < 5; i++)
	{
		int x = rand() % (n - 1) + 1;

		if (t2(x, n, itr))
			return 0;
	}

	return 1;

}

int main()
{
	int *n, nr, i, itr;

	std::cout << "Nr. de numere de verificat: ";
	std::cin >> nr; // const int nr = 4;

	n = new int[nr]; // { 34673, 679, 46559, 3551 };

	i = 0;
	while (i < nr)
	{
		std::cin >> n[i]; // std::cout << n[i];

		std::cout << "\n" << prim_clasic(n[i], itr = 0);
		std::cout << " iteratii prim clasic: " << itr << "\n";

		std::cout << prim_rand(n[i], itr = 0);
		std::cout << " prim randomizat v1: " << itr << "\n";

		std::cout << prim_rand2(n[i++], itr = 0);
		std::cout << " prim randomizat v2: " << itr << "\n\n";
	}

	delete[] n;
	return 0;
}
/* END_OF Nr_prim.cpp */