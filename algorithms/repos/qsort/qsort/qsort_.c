/*
int partition(int a[], int p, int r)
{
	int i = p - 1, key = a[r];

	for (int j = p; j < r ; ++j) {
		if (a[j] <= key) {
			swap(a, ++i, j);
		}
	}

	swap(a, ++i, r);
	return i; // with a[lo..j-1] <= a[j] <= a[j+1..hi].
}

int partition_randomized(int a[], int p, int r)
{
	int i = rand_range(p, r);
	swap(a, i, r);

	return partition(a, p, r);
}

void st_qsort(int a[], int p, int r)
{
	if (p < r) {
		int q = partition_randomized(a, p, r); // Partition (see page 291).

		st_qsort(a, p, q - 1); // Sort left part a[lo .. j-1].
		st_qsort(a, q + 1, r); // Sort right part a[j+1 .. hi].
	}
}


*/
