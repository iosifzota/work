#include "stdio_wrap.h"
#include "stdlib_wrap.h"
#include "limits.h"
#include "stdint.h"
#include "time.h"
#include <malloc.h>
#include <string.h>

int cmp(void *v1, void *v2)
{
	return *(int *)v1 - *(int *)v2;
}

int rand_n(int);
int rand_range(int, int);
void swap(int [], int, int);

void rand_qsort(int [], int, int);
void KR_qsort(int [], int, int);

void inter_sort(int [], int);

void algo_sort(int a[], int lo, int hi);

#define INPUT "input.txt"
int itr_perf = 0;

int main()
{
	int test[] = {
		10, 9, 34, 14, 8, 6, 39, 1, 4, 34, 431, 43, 98, 35, 89, 349, 956, 5, 3, 43
	};
	algo_sort(test, 0, NELEMS(test) - 1);
	fprintarr(stdout, "%d ", test, NELEMS(test));


	int arr[100000], n = 100000;
	FILE *input, *out;

	file_isEmpty_(INPUT, return 0); input = ec_fopen(INPUT, "r");
	freadarr(input, "%d", arr, n);
	fclose(input);
	out = ec_fopen("read.txt", "w"); fprintarr(out, "%d ", arr, n); fclose(out);


	qsort(arr, n, sizeof(arr[0]), cmp);
	out = ec_fopen("qsort_res.txt", "w"); fprintarr(out, "%d ", arr, n); fclose(out);

	/*
	itr_perf = 0;
	inter_sort(arr, n);
	printf("Insertion-sort iterations: %d\n", itr_perf);
	*/
	/*
	itr_perf = 0;
	KR_qsort(arr, 0, n - 1);
	printf("Quicksort iterations: %d\n", itr_perf);
	*/
	/*
	itr_perf = 0;
	rand_qsort(arr, 0, n - 1);
	printf("Quicksort(randomized) iterations: %d\n", itr_perf);
	*/
	/*
	itr_perf = 0;
	algo_sort(arr, 0, n - 1);
	printf("Algo sort iterations: %d\n", itr_perf);
	*/
	
	/*
	out = ec_fopen("out.txt", "w");
	fprintarr(out, "%d ", arr, n);
	fclose(out);
	*/

	return 0;
}

 void algo_sort(int a[], int lo, int hi)
{
	if (hi <= lo) {
		return;
	}
	int last = lo, i = lo + 1, gt = hi;
	int rand_index = rand_range(lo, hi);
	swap(a, lo, rand_index);
	int v = a[lo];

	/*	
	printf("IN: ");
	fprintarr(stdout, "%d ", a+lo, hi - lo + 1);
	printf("%d: ", v);
	//*/
	
	while (i <= gt)
	{
		++itr_perf;
		//if (a[i] < v) swap(a, last++, i++);
		 if (a[i] > v) { 
			while (a[gt] > v && i <= gt) {
				//printf("%d ", a[gt]);
				gt--;
			}

			if (i <= gt) {
				swap(a, i++, gt--);
			}
		}
		else i++;
	} // Now a[lo..lt-1] < v = a[lt..gt] < a[gt+1..hi].
	swap(a, lo, i - 1);
	
	/*
	printf("\nOUT: ");
	fprintarr(stdout, "%d ", a+lo, hi - lo + 1);
	NEWLINE;
	//*/

	algo_sort(a, lo, i - 2);
	algo_sort(a, gt + 1, hi);
}



void rand_qsort(int arr[], int left, int right)
{
	if (left >= right) {
		return;
	}

	int last = left;

	swap(arr, left, rand_range(left, right));

	for (int i = left + 1; i <= right; ++i) {
		if (arr[i] < arr[left]) {
			swap(arr, ++last, i);
		}
		++itr_perf;
	}

	swap(arr, left, last);
	rand_qsort(arr, left, last - 1);
	rand_qsort(arr, last + 1, right);
}


void KR_qsort(int v[], int left, int right)
{
	int i, last;
	void swap(int v[], int i, int j);
	if (left >= right) /* do nothing if array contains */
			return; /* fewer than two elements */
	swap(v, left, (left + right) / 2); /* move partition elem */
	last = left; /* to v[0] */
	for (i = left + 1; i <= right; i++) { /* partition */
		if (v[i] < v[left]) {
			swap(v, ++last, i);
		}
		++itr_perf;
	}
	swap(v, left, last); /* restore partition elem */
	KR_qsort(v, left, last - 1);
	KR_qsort(v, last + 1, right);
}


int rand_range(int l, int r)
{
	int rand = rand_n(r - l);

	return (rand < l) ? l + rand : rand;
}

int rand_n(int n)
{
	static init;

	if (!init) {
		srand((unsigned int) time(NULL));
	}

	return rand() % (n + 1);
}

/* swap: interchange v[i] and v[j] */
void swap(int v[], int i, int j)
{
	int temp;
	temp = v[i];
	v[i] = v[j];
	v[j] = temp;
}

void inter_sort(int arr[], int nelems)
{
	int key, unsorted_index, sorted_index;

	itr_perf = 0;

	for (
		unsorted_index = 1;
		unsorted_index < nelems;
		++unsorted_index
		)
	{
		key = arr[unsorted_index];

		for (
			sorted_index = unsorted_index - 1;
			sorted_index >= 0;
			--sorted_index
			)
		{
			++itr_perf;
			if (arr[sorted_index] < key) {
				break;
			}

			arr[sorted_index + 1] = arr[sorted_index];
		}

		arr[sorted_index + 1] = key;
	}
}