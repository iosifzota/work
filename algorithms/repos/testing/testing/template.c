#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <limits.h>


/* Utitlites. TO BE IGNORED. */
/* ======================================================================== */
#define NEWLINE puts("");
#define dd printf("HERE %d\n", __LINE__); // Used for debugging.

#define NELEMS(x) (sizeof(x)/sizeof(x[0]))
#define max(x,y) (((x) > (y)) ? (x) : (y))
#define min(x,y) (((x) > (y)) ? (y) : (x))

/* For itr = [start, end]. */
#define forIn(itr, start, end) \
    for (unsigned int itr = (start); (itr) < (end); ++(itr))

/* Assert expr. If not true print err_msg and exe err_expr. */
#define assert(expr, err_msg, err_expr)	\
    if (!(expr)) {						\
        printf(err_msg);				\
        err_expr;						\
    }

/* Array concatenation. */
#define arrcat(t, t_nelems, s, s_nelems) {      \
	forIn(s_index, 0, s_nelems) {				\
            t[t_nelems + s_index] = s[s_index]; \
	} }

/* Error codes. */
enum errcode {
	SUCCESS,
	ERROR,
	NULL_INPUT = -'I',
	DOMAIN_ERR,  // Input does not belong to the domanin of the function.
	NOT_FOUND = 404,
	FOUND = -404,
	NIL = '0',
	INSUFFICENT_INPUT,
	RESOURCE_ACQUISITION_FAILED = 7
};
typedef enum errcode errcode;

int errno_ = SUCCESS;

/* Error checked malloc, realloc and calloc. */
void *ec_malloc(size_t);
void *ec_realloc(void *, size_t);
void *ec_calloc(size_t, size_t);

/* Allocate memory for an array of =type=. */
#define mem_arr(type, ptr, size) \
    (ptr) = (type *) ec_malloc((size) * sizeof(type))
#define mem_arr_calloc(type, ptr, size) \
    (ptr) = (type *) ec_calloc((size), sizeof(type));

/* Contigous matrix allocation. */
#define mem_mat(type, matrix, rows, cols)			\
	mem_arr(type*, matrix, rows * cols + rows);		\
	type *data_index = matrix + rows;				\
	forIn(row, 0, rows) {							\
		matrix[row] = data_index + (row * cols);	\
	}

/* Allocate matrix. */
#define mem_matrix(type, matrix, rows, cols)    \
    mem_arr(type*, matrix, rows);               \
    forIn (i, 0, rows) {                        \
        mem_arr(type, matrix[i], cols);         \
    }
/* Deallocate matrix. */
#define free_matrix(matrix, rows)	\
    forIn (i, 0, rows) {            \
        free(matrix[i]);            \
    }                               \
    free(matrix);

/* Variable length TAB. */
#define TAB(length) printf("%" #length "c", 0)

/* Strtol wrapper. */
#define strtol_(str, identifier, calling_fn, err_expr)	 					\
	identifier = strtol(str, &err, 10);										\
	if (*err) {																\
        printf("[Error] " #calling_fn ": \"%s\" is not a number.\n", str);	\
        err_expr;															\
	}

typedef struct {
	char *s;
	unsigned int len;
} String;

/* Return a token from a file descriptor. */
String ftok(FILE *fd, const char *del);

/* Get a token from a file descriptor and assert it's not null. */
#define assert_ftok(fd, token, del, err_expr) \
		assert((token = ftok(fd, del)).len,	"",	err_expr);

/* Erorr checked fopen(). */
FILE *ec_fopen(const char *file_path, const char *mode);

/* Assert file at =file_path= is not empty. */
#define assert_not_empty(file_path, eof_expr) do {		\
	FILE *fd_test = ec_fopen(file_path, "a+");			\
	if(fgetc(fd_test) == EOF || feof(fd_test))	{		\
		printf("File %s is empty.\n", file_path);		\
		fclose(fd_test);								\
		eof_expr; break;								\
	}													\
	fclose(fd_test);									\
    } while(0)

/* Flexible read array. */
#define freadarr_va(fd, format_specifier, arr, nelems) do { \
	forIn (i, 0, nelems) {                                  \
		if (EOF == fscanf(fd, format_specifier, arr + i)) { \
			nelems = i; break;                              \
		}                                                   \
	}                                                       \
    } while(0)
/* Error checked read of an array. */
#define freadarr(fd, format_specifier, arr, nelems)		    \
	forIn (i, 0, nelems) {                                  \
		if (EOF == fscanf(fd, format_specifier, arr + i)) { \
			puts("[Error] Not enough input.\n");			\
			exit(INSUFFICENT_INPUT);						\
		}                                                   \
	}
/* Print array. */
#define fprintarr(fd, format_specifier, arr, nelems)    \
    forIn (i, 0, nelems) {                              \
        fprintf(fd, format_specifier, *(arr + i));      \
    }                                                   \
    NEWLINE;

/* Error checked read of a matrix. */
#define freadmat(fd, format_specifier, matrix, rows,  cols)								\
	forIn (ln_index, 0, rows) {															\
		forIn (col_index, 0, cols) {													\
			if ( EOF == fscanf(fd, format_specifier,matrix[ln_index] + col_index)) {	\
				puts("[Error] Not enough input.\n");									\
				exit(INSUFFICENT_INPUT);												\
			}																			\
		}																				\
	}
/* Print matrix. */   /* NEWLINE is printed to stdout only. */
#define fprintmat(fd, format_specifier, matrix, rows,  cols)				\
    NEWLINE;																\
    forIn (ln_index, 0, rows) {												\
        forIn (col_index, 0, cols) {										\
            fprintf(fd, format_specifier " ",matrix[ln_index][col_index]);	\
        }																	\
        NEWLINE;															\
    }																		\
    NEWLINE;
/* ======================================================================== */


/* BEGIN */
#define INIT(input_file) do {							\
		assert_not_empty(input_file, exit(NULL_INPUT));	\
		FILE *input = ec_fopen(input_file, "r");		\
														\
														\
														\
		fclose(input);									\
	} while(0)

#define INPUT_FILE "" // <- FILL

int main(int argc, char **argv)
{
	INIT(INPUT_FILE);

	return 0;
}
/* END */



/* Utilites. TO BE IGNORED. */
/* ======================================================================== */
/* Return a token from a given file descriptor. */
String ftok(FILE *fd, const char *del)
{
	String token;
	unsigned int size = 30, s_index = 0;
	char c;

	assert(del, "[Error] Empty del string.\n", return ((String){ NULL, 0 }));

	mem_arr(char, token.s, size);

	/* Ignore initial delimiters. */
	while ((c = fgetc(fd)) != EOF && strchr(del, c)) { ; }

	if (c == EOF) {
		free(token.s);
		return (String) { NULL, 0 };
	}

	do {
		if (s_index >= size) {
			token.s = (char *)ec_realloc(token.s, size += 30);
		}
		*(token.s + s_index) = c;
		++s_index;
	} while ((c = fgetc(fd)) != EOF && !strchr(del, c));

	token.s = (char *)ec_realloc(token.s, s_index + 1); // "Tight fitting" string.
	token.s[s_index] = 0; // End string.
	token.len = s_index;

	return token;
}

/* I/O handling. */
FILE *ec_fopen(const char *file_path, const char *mode)
{
	FILE *fd;

	assert(file_path, "[Error] Empty file_path string.\n", exit(NULL_INPUT));
	assert(mode, "[Error] Empty mode string.\n", exit(NULL_INPUT));

	fd = fopen(file_path, mode);

	if (NULL == fd) {
		puts("[Error] fopen() returned NULL.");
		exit(RESOURCE_ACQUISITION_FAILED);
	}

	return fd;
}

/* Memory management. */
#define assert_alloc(allocator_name)                            \
    assert((new_ptr != NULL),                                   \
           "[Error] " #allocator_name "() returned NULL.\n",    \
           exit(RESOURCE_ACQUISITION_FAILED));
void *ec_calloc(size_t nitems, size_t size)
{
	void *new_ptr;

	new_ptr = calloc(nitems, size);
	assert_alloc(calloc);

	return new_ptr;
}
void *ec_realloc(void *ptr, size_t size)
{
	void *new_ptr;

	new_ptr = realloc(ptr, size);
	assert_alloc(realloc);

	return new_ptr;
}
void *ec_malloc(size_t size)
{
	return ec_realloc(NULL, size);
}
/* ======================================================================== */
