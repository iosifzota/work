#include <iostream>
#include <fstream>

#include <memory>
#include <queue>
#include <vector>
#include <unordered_set>

#include <cstdlib>
#include <cstdio>


/* For itr = [start, end]; */
#define forIn(itr, start, end)                                  \
    for (unsigned int itr = (start); (itr) < (end); ++(itr))

#define NEWLINE puts("");



/*
  input.txt:
  7 2 4
  5 0 6
  8 3 1

  1 5 2
  0 7 4
  8 6 3
*/


struct Blank_move {
    int x_offset;
    int y_offset;
};
struct Blank_move move_north = { -1,  0 };
struct Blank_move move_south = { 1,  0 };
struct Blank_move move_east = { 0,  1 };
struct Blank_move move_west = { 0, -1 };

#define N_ACTIONS 4
struct Blank_move moves[] = {
    move_north,
    move_south,
    move_east,
    move_west
};
// Print matrix to stdout.
template <typename type>
void printmat(std::vector< std::vector<type> >&);
template <typename type>
void printmat(std::vector< std::vector<type> >& mat, std::fstream output);

// Read matrix from a given fstream.
template <typename type>
void readmat(std::vector< std::vector<type> >&, std::fstream&);
void fetch_input(
    std::vector< std::vector<char> >&,
    std::vector< std::vector<char> >&,
    const char *input_file_path);
/* ======================================================================== */
struct Blank_xOy {
    int x;
    int y;
};
Blank_xOy find_blank(std::vector< std::vector<char> >);
Blank_xOy find_blank(std::vector< std::vector<char> >, char);
struct PuzzleState {
    std::vector < std::vector<char> > puzzle_state;
    Blank_xOy blank_xOy;
};

/* Overload equal operator to work with the PuzzleState type.
 * Required for std::unorderd_set to work with the PuzzleState hash. */
inline bool operator == (PuzzleState const& stateA, PuzzleState const& stateB)
{
    return (stateA.blank_xOy.x == stateB.blank_xOy.x) &&
        (stateA.blank_xOy.y == stateB.blank_xOy.y) &&
        (stateA.puzzle_state == stateB.puzzle_state);
}
/* PuzzleState hash.*/
struct Hash {
    size_t operator() (const PuzzleState& state) const {

        size_t hash, count;
        hash = state.blank_xOy.x + state.blank_xOy.y;

        count = 1;
        for (
            auto state_row = state.puzzle_state.begin();
            state_row < state.puzzle_state.end();
            ++state_row
            )
        {
            for (
                auto state_col = state_row->begin();
                state_col < state_row->end();
                ++state_col
                )
            {
                hash += *state_col * count;
                count *= 2;
            }
        }

        return hash;
    }
};

struct Successor {
    Blank_move move;
    Blank_xOy blank_xOy;
    std::vector< std::vector<char> > result;
};


/* Apply an move to a given state. */
std::vector< std::vector <char> > apply_change(PuzzleState, Blank_xOy);
/* Assert if Blank_move is possible on the given Blank_xOy. */
Blank_xOy assert_move(Blank_xOy, Blank_move, const int);
/* Taste if two states are the same. */
bool goal_test_fn(PuzzleState& goal_state, PuzzleState& test_state);

/* SearchGraph: Tnode -> nodul grafului
   fringe -> coada cu nodurile generate
   closed -> hashul cu starile intanlite
   parent -> ultimul nod extins
   goal -> starea tinta
   next_node() -> extinde urmatorul nod din fringe
   goal_test() -> verifica daca s-a atins starea tinta
   expand() -> expinde nodulul =parent=
*/
template<typename Tstate, typename Taction, typename Successor, typename Hash>
class SearchGraph {

public:
    struct Tnode {
        Tstate state;
        std::shared_ptr<Tnode> parent;
        Taction action;
        unsigned int path_cost, depth;
    };

    SearchGraph(Tstate init_state, Tstate goal_state)
	{
            parent = std::make_shared<Tnode>();
            parent->state = init_state;
            parent->parent = nullptr;
            parent->path_cost = parent->depth = 0;

            fringe.push(parent);

            goal = goal_state;
	}

    bool next_node()
	{
            if (fringe.empty()) {
                return false;
            }
            parent = fringe.front();
            fringe.pop();

            return true;
	}

    bool goal_test(bool goal_test_fn(Tstate& goal_state, Tstate& test_state))
	{
            return goal_test_fn(goal, parent->state);
	}

    void expand(
        void insert_all(
            SearchGraph<Tstate, Taction, Successor, Hash>* graph,
            std::shared_ptr<Tnode> parent)
	)
	{
            closed.insert(parent->state);
            insert_all(this, parent);
	}


    Tstate goal;
    std::shared_ptr<Tnode> parent;
    std::queue< std::shared_ptr<Tnode> > fringe;
    std::unordered_set<Tstate, Hash> closed;
private:
};

/* Returns a vector of *unique successors of a given parent.
 * *uniquie = unique state in the SearchGraph. */
std::vector<Successor>
successor_fn(
    std::shared_ptr<SearchGraph<PuzzleState, Blank_move, Successor, Hash>::Tnode>,
    SearchGraph<PuzzleState, Blank_move, Successor, Hash> *graph);

/* Get the successor of Tnode and add them to the SearchGraph fringe. */
void insert_all(
    SearchGraph<PuzzleState, Blank_move, Successor, Hash> *,
    std::shared_ptr<SearchGraph<PuzzleState, Blank_move, Successor, Hash>::Tnode>
    );

/* Print move, path_cost, depth. */
void print_solution(
    SearchGraph<PuzzleState, Blank_move, Successor, Hash>& graph,
    const char *output_file_path);

#define PUZZLE_SIZE 3

/* Defines 2 PuzzleState instances: 1 for init_state, 1 for goal_state */
#define INIT(input_file_path)                                           \
    PuzzleState init_state;                                             \
    PuzzleState goal_state;                                             \
    do {                                                                \
	std::vector< std::vector<char> > init_st(PUZZLE_SIZE, std::vector<char>(PUZZLE_SIZE)); \
	std::vector< std::vector<char> > goal_st(PUZZLE_SIZE, std::vector<char>(PUZZLE_SIZE)); \
	fetch_input(init_st, goal_st, input_file_path);                 \
                                                                        \
	/* Find the position of '0'. */                                 \
	Blank_xOy init_blank = find_blank(init_st, '0');                \
	Blank_xOy goal_blank = find_blank(goal_st, '0');                \
	/* Set the postion of '0' to ' ' (blank). */                    \
	init_st[init_blank.x][init_blank.y] = ' ';                      \
	goal_st[goal_blank.x][goal_blank.y] = ' ';                      \
                                                                        \
	/* Set the init_state and the goal_state */                     \
	init_state = { init_st, init_blank };                           \
	goal_state = { goal_st, goal_blank };                           \
    } while (0)


#define INPUT_FILE_PATH "input.txt"
#define OUTPUT_FILE_PATH "output.txt"

int main()
{
    INIT(INPUT_FILE_PATH); // Defines 2 PuzzleState instances: 1 for init_state, 1 for goal_state;

    SearchGraph<PuzzleState, Blank_move, Successor, Hash> graph(init_state, goal_state);

    while (graph.next_node()) {
        if (graph.goal_test(goal_test_fn)) {
            print_solution(graph, OUTPUT_FILE_PATH);
            puts("Solution!!");
            return 0;
        }
        graph.expand(insert_all);
        putchar('.');
    }

    puts("No solution");

    return 0;
}

void insert_all(
    SearchGraph<PuzzleState, Blank_move, Successor, Hash> *graph,
    std::shared_ptr<SearchGraph<PuzzleState, Blank_move, Successor, Hash>::Tnode> parent
    )
{
    std::vector<Successor> successors = successor_fn(parent, graph);

    for (auto& successor : successors) {
        auto node = std::make_shared<SearchGraph<PuzzleState, Blank_move, Successor, Hash>::Tnode>();
        node->state.puzzle_state = successor.result;
        node->state.blank_xOy = successor.blank_xOy;
        node->parent = parent;
        node->action = successor.move;
        node->depth = parent->depth + 1;
        node->path_cost = parent->path_cost + 1;

        graph->fringe.push(node);
    }
    //NEWLINE;
}

void print_solution(
    SearchGraph<PuzzleState, Blank_move, Successor, Hash>& graph,
    const char *output_file_path)
{
    std::fstream output;
    output.open(output_file_path, std::ios::out);

    output << "Move: " << graph.parent->action.x_offset << " ";
    output << graph.parent->action.y_offset << std::endl;

    output << "Depth: " << graph.parent->depth << std::endl;
    output << "Path cost: " << graph.parent->depth << std::endl;

    for (auto& row : graph.parent->state.puzzle_state) {
        for (auto& elem : row) {
            output << elem << " ";
        }
        output << std::endl;
    }

    output.close();
}


Blank_xOy assert_move(Blank_xOy blank_xOy, Blank_move move, const int puzzle_size)
{
    if ((blank_xOy.x += move.x_offset) < 0 || blank_xOy.x >= puzzle_size) {
        return { -1, -1 };
    }
    if ((blank_xOy.y += move.y_offset) < 0 || blank_xOy.y >= puzzle_size) {
        return { -1, -1 };
    }

    return blank_xOy;
}

std::vector< std::vector <char> >
apply_change(
    PuzzleState current_state, Blank_xOy result_blank_xOy)
{
    char aux = current_state.puzzle_state[result_blank_xOy.x][result_blank_xOy.y];
    current_state.puzzle_state[result_blank_xOy.x][result_blank_xOy.y] = ' ';
    current_state.puzzle_state[current_state.blank_xOy.x][current_state.blank_xOy.y] = aux;

    return current_state.puzzle_state;
}

std::vector<Successor>
successor_fn(
    std::shared_ptr<SearchGraph<PuzzleState, Blank_move, Successor, Hash>::Tnode> parent,
    SearchGraph<PuzzleState, Blank_move, Successor, Hash> *graph)
{
    std::vector<Successor> successors;

    Blank_xOy temp_blank_xOy;
    PuzzleState temp_state;

    forIn(move_index, 0, N_ACTIONS) {

        if (
            (temp_blank_xOy =     // Test if action is possible.
             assert_move(
                 parent->state.blank_xOy,
                 moves[move_index],
                 PUZZLE_SIZE)
                ).x != -1

            || temp_blank_xOy.y != -1
            )
        {
            temp_state = {
                apply_change(   // Apply action.
                    parent->state,
                    temp_blank_xOy),
                temp_blank_xOy
            };

            // Ignore duplicate state.
            if (graph->closed.find(temp_state) != graph->closed.end()) {
                //puts("Ignoring duplicate state.");
                continue;
            }

            successors.push_back(
                {
                    moves[move_index],   // Add to the successors vector.
                        temp_blank_xOy,
                        apply_change(
                            parent->state,
                            temp_blank_xOy)
                        });
        }
    }

    return successors;
}


bool goal_test_fn(PuzzleState& goal_state, PuzzleState& test_state)
{
    if (goal_state.blank_xOy.x != test_state.blank_xOy.x
        || goal_state.blank_xOy.y != test_state.blank_xOy.y)
    {
        return false;
    }

    std::vector< std::vector<char> >::iterator goal_row, test_row;
    std::vector<char>::iterator goal_col, test_col;

    for (
        goal_row = goal_state.puzzle_state.begin(),
            test_row = test_state.puzzle_state.begin();
        goal_row < goal_state.puzzle_state.end();
        ++goal_row, ++test_row
        )
    {
        for (
            goal_col = goal_row->begin(),
                test_col = test_row->begin();
            goal_col < goal_row->end();
            ++goal_col, ++test_col
            )
        {
            if (*test_col != *goal_col) {
                return false;
            }
        }

    }

    return true;
}

void
fetch_input(std::vector< std::vector<char> >& init_state,
            std::vector< std::vector<char> >& goal_state, const char* input_file_path)
{
    std::fstream input;
    input.open(input_file_path, std::ios::in);

    readmat<char>(init_state, input);
    readmat<char>(goal_state, input);

    input.close();
}

Blank_xOy find_blank(std::vector< std::vector<char> > state)
{
    int x = 0, y = 0;

    for (auto& row : state) {
        for (auto& col : row) {
            if (col == '0') {
                return { x, y };
            }
            y++;
        }
        y = 0;
        x++;
    }

    return { -1, -1 };
}


Blank_xOy find_blank(std::vector< std::vector<char> > state, char search_value)
{
    int x = 0, y = 0;

    for (auto& row : state) {
        for (auto& col : row) {
            if (col == search_value) {
                return { x, y };
            }
            y++;
        }
        y = 0;
        x++;
    }

    return { -1, -1 };
}


void showq(std::queue <std::shared_ptr<SearchGraph<PuzzleState, Blank_move, Successor, Hash>::Tnode>> g)
{
    int count = 0;
    while (!g.empty())
    {
        printf("%d:\n", ++count);
        std::shared_ptr<SearchGraph<PuzzleState, Blank_move, Successor, Hash>::Tnode> node = g.front();

        printmat(node->state.puzzle_state);
        NEWLINE;

        g.pop();
    }
    NEWLINE;
}


template <typename type>
void printmat(std::vector< std::vector<type> >& mat)
{
    NEWLINE;

    for (auto& row : mat) {
        for (auto& elem : row) {
            std::cout << elem << " ";
        }
        NEWLINE;
    }

    NEWLINE;
}

template <typename type>
void printmat(std::vector< std::vector<type> >& mat, std::fstream output)
{
    NEWLINE;

    for (auto& row : mat) {
        for (auto& elem : row) {
            output << elem << " ";
        }
        NEWLINE;
    }

    NEWLINE;
}


template <typename type>
void readmat(std::vector< std::vector<type> >& mat, std::fstream& input)
{
    for (auto& row : mat) {
        for (auto& elem : row)
        {
            if (input.peek() == EOF) {
                std::cerr << "[Error] Not enough input."; NEWLINE;
                exit(1);
            }
            input >> elem;
        }
    }
}
