/*
 * Game-tree (bool_minimax); Quicksort
 */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>
#include <limits.h>
#include <math.h>


/* Utitlites. TO BE IGNORED. */
/* ======================================================================== */
#define NEWLINE puts("");

#define NELEMS(x) (sizeof(x)/sizeof(x[0]))
#define max(x,y) (((x) > (y)) ? (x) : (y))
#define min(x,y) (((x) > (y)) ? (y) : (x))

/* For itr = [start, end]. */
#define forIn(itr, start, end) \
    for (unsigned int itr = (start); (itr) < (end); ++(itr))

#define arrcpy(dest, source, nelems, offset)    \
    forIn(i, 0, nelems) {                       \
        dest[i + offset] = source[i];           \
    }

/* Assert expr. If not true print err_msg and exe err_expr. */
#define assert(expr, err_msg, err_expr)         \
    if (!(expr)) {                              \
        printf(err_msg);                        \
        err_expr;                               \
    }

/* Error codes. */
enum errcode {
    SUCCESS,
    ERROR,
    NULL_INPUT = -'I',
    DOMAIN_ERR,  // Input does not belong to the domanin of the function.
    NOT_FOUND = 404,
    FOUND = -404,
    NIL = '0',
    INSUFFICENT_INPUT,
    RESOURCE_ACQUISITION_FAILED = 7
};
typedef enum errcode errcode;

/* Error checked malloc, realloc and calloc. */
void *ec_malloc(size_t);
void *ec_realloc(void *, size_t);
void *ec_calloc(size_t, size_t);

/* Allocate memory for an array of =type=. */
#define mem_arr(type, ptr, size) \
    (ptr) = (type *) ec_malloc((size) * sizeof(type))

/* Strtol base 10 wrapper.
 * If converstion failed the variable =identifier= resolves to,
 * will contain the error return value of strtol (0, for gcc at least).
 */
#define strtol_decimal(str, identifier, calling_fn, err_expr) do {      \
	char *err;                                                      \
	assert(str && *str,                                             \
            "[Error] strtol_(): Null input\n", exit(NULL_INPUT));       \
                                                                        \
	identifier = strtol(str, &err, 10);                             \
                                                                        \
	if (*err) {                                                     \
            printf("[Error] " #calling_fn "(): \"%s\" is not a number.\n", str); \
            err_expr;                                                   \
	}                                                               \
    } while(0)

/* Read a string, attempt conversion to long using strtol_decimal. */
#define freadint(fd, identifier) do {                                   \
	String token;                                                   \
	assert_ftok(fd, token, " \n", exit(NULL_INPUT));                \
	strtol_decimal(token.s, identifier, freadint, exit(DOMAIN_ERR));\
	free(token.s);                                                  \
    } while(0)

typedef struct {
    char *s;
    unsigned int len;
} String;

/* Return a token from a file descriptor. */
String ftok(FILE *fd, const char *del);

/* Get a token from a file descriptor and assert it's not null. */
#define assert_ftok(fd, token, del, err_expr)           \
    assert((token = ftok(fd, del)).len,                 \
           "[Error] ftok() retruned NULL\n", err_expr);

/* Error checked read of an array. */
#define freadarr(fd, format_specifier, arr, nelems)             \
    forIn (i, 0, nelems) {                                      \
        if (EOF == fscanf(fd, format_specifier, arr + i)) {     \
            puts("[Error] Not enough input.\n");                \
            exit(INSUFFICENT_INPUT);                            \
        }                                                       \
    }
/* Print array. */
#define fprintarr(fd, format_specifier, arr, nelems)    \
    forIn (i, 0, nelems) {                              \
        fprintf(fd, format_specifier, *(arr + i));      \
    }                                                   \
    if (fd == stdout) NEWLINE;


typedef struct {
    unsigned int n, size;
    int *st;
} Stack;

/* Push to stack. If isFull() allocate 10 more slots to make room. */
void push(Stack *, int);
/* Pop and element from the stack and return it. */
int pop(Stack *);
/* Check if stack is empty. */
_Bool isEmpty(Stack *);
/* Check if stack is full. */
_Bool isFull(Stack *);

/* While !isEmpty(): pass pop() return value to print_fn(). */
void print_stack(Stack *, void(*print_fn)(int));

/* Stack memory management */
Stack alloc_stack(unsigned int);
Stack realloc_stack(Stack *, unsigned int);
void free_stack(Stack *);
/* ======================================================================== */



/* BEGIN */

/* Used for counting number of iteration. */
unsigned int itr;

/* Mod(n) random generator. */
int rand_mod(int n);
/* Generate numbers from [l, r]. */
int rand_range(int l, int r);

/* lg */
#define lg(b, n) (log(n)/log(b))

/* Game tree */
int game_tree();

_Bool bool_minimax(int[], int, int, int, int, Stack *);
void which_way(char, int, Stack *);
void print_choice(int);

/* Quicksort */
void test_qsort();

void rand_quicksort(int[], int, int, int (*)(void *, void *));
void quicksort(int[], int, int, int (*)(void *, void *));

void swap(int[], int, int);
int cmp_ints(void *v1, void *v2);


int main()
{
    puts("Game tree");
    game_tree();

    NEWLINE;

    puts("Quicksort");
    test_qsort();

    return 0;
}


#define GAME_TREE_INPUT do {                                            \
        /* Default input */                                             \
        choices = def;                                                  \
        nchoices = NELEMS(def);                                         \
                                                                        \
        printf("Default input: ");                                      \
        fprintarr(stdout, "%d ", def, nchoices);                        \
        printf("Use default input? <y/n> ");                            \
                                                                        \
        /* Read option from stdin. */                                   \
        String token = ftok(stdin, " \n");                              \
                                                                        \
        /* Validate option. */                                          \
        assert(token.len == 1 && (strchr("yYnN", token.s[0])),          \
               "Invalid option\n\n",                                    \
               free(token.s); return game_tree());                      \
                                                                        \
        /* If did not opt for default, provide another array. */        \
        if (strchr("nN", token.s[0])) {                                 \
            printf("\nNumber of elements (power of 2): ");		\
            freadint(stdin, nchoices);                                  \
                                                                        \
            /* Check if array size is a power of 2. */			\
            assert(lg(2, nchoices) == (long)lg(2, nchoices),            \
                   "Number of elements must be a power two.\n",         \
                   free(token.s); return game_tree());                  \
                                                                        \
            mem_arr(int, choices, nchoices);                            \
            freadarr(stdin, "%d", choices, nchoices);			\
        }                                                               \
        free(token.s);                                                  \
    } while (0);

#define GAME_TREE_DEFAULT_INPUT                 \
    1, 1, 0, 0, 1, 0, 0, 1

int game_tree()
{
    int def[] = { GAME_TREE_DEFAULT_INPUT }, *choices, nchoices, max_depth;
    Stack path;

    GAME_TREE_INPUT;

    max_depth = lg(2, nchoices);
    path = alloc_stack(nchoices - 1);   // 2^lg2(nchoices - 1)

    if (bool_minimax(choices, 0, 0, max_depth, 1, &path)) {
        printf("\nWinnable!\nChoices: ");
        print_stack(&path, print_choice);
    }
    else {
        puts("\nNot winnable. :(");
    }

    printf("\nNumber of itertions: %d\n", itr);

    /* Cleanup */
    if (choices != def) {
        free(choices);
    }
    free_stack(&path);
    return 0;
}

/* Return false if !winnable. If winnable return true and fill path with the optimal choices. */
_Bool bool_minimax(int choices[], int node_index, int depth, int max_depth, int player, Stack *path)
{
    ++itr;
    if (depth == max_depth)
        return choices[node_index];

    int choiceA, choiceB, rand;

    rand = rand_mod(2);

    if (player == 1) {

        choiceA = bool_minimax(choices, node_index * 2 + rand, depth + 1, max_depth, 2, path);

        if (choiceA) { // First choice ?
            which_way('A', rand, path);
            return choiceA;
        }

        choiceB = bool_minimax(choices, node_index * 2 + (1 - rand), ++depth, max_depth, 2, path);

        if (choiceB) { // Second choice ?
            which_way('B', rand, path);
            return choiceB;
        }

        // No optimal choice
        path->n = 0;
        return 0;
    }

    choiceA = bool_minimax(choices, node_index * 2 + rand, depth + 1, max_depth, 1, path);

    if (!choiceA) {
        path->n = 0; // dead end
        return 0;
    }

    choiceB = bool_minimax(choices, node_index * 2 + (1 - rand), ++depth, max_depth, 1, path);

    if (choiceB) {
        if (!isEmpty(path)) {
            pop(path); // Choose either L/R by discarding one choice.
        }
        return choiceB;
    }

    path->n = 0; // dead end
    return 0;
}

void which_way(char choice, int rand, Stack *path)
{
    if ('A' == choice) {
        push(path, rand);
        return;
    }
    push(path, 1 - rand);
}

void print_choice(int choice)
{
    if (choice == 1) {
        printf("[Right] ");
    }
    else {
        printf("[Left]  ");
    }
}

#define QUICKSORT_INPUT do {                                            \
        /* Default input */                                             \
        nelems = NELEMS(a);                                             \
        arr1 = a; arr2 = b;                                             \
                                                                        \
        printf("Default input: ");                                      \
        fprintarr(stdout, "%d ", a, nelems);                            \
        printf("Use default input? <y/n> ");                            \
                                                                        \
        /* Read option from stdin. */                                   \
        String token = ftok(stdin, " \n");                              \
                                                                        \
        /* Validate option. */                                          \
        assert(token.len == 1 && (strchr("yYnN", token.s[0])),          \
               "Invalid option\n\n",                                    \
               free(token.s); return test_qsort());                     \
                                                                        \
        /* If did not opt for default, provide another array. */        \
        if (strchr("nN", token.s[0])) {                                 \
            printf("\nNumber of elements: ");                           \
            freadint(stdin, nelems);                                    \
                                                                        \
            mem_arr(int, arr1, nelems);                                 \
            mem_arr(int, arr2, nelems);                                 \
                                                                        \
            freadarr(stdin, "%d", arr1, nelems);                        \
            arrcpy(arr2, arr1, nelems, 0);                              \
        }                                                               \
        free(token.s);                                                  \
    } while (0)

/* Macro for testing qsort functions. */
#define test_quicksort(qsort_fn, arr, nelems, cmp_fn)   \
    itr = 0;                                            \
    qsort_fn(arr, 0, nelems - 1, cmp_fn);               \
    printf(#qsort_fn "\n\t");                           \
    fprintarr(stdout, "%d ", (arr), (nelems));		\
    printf("\tNumber of iterations: %d\n\n", itr);

/* Default input. */
#define QSORT_DEFAULT_INPUT                                             \
    3, 7, 18, 6, 11, 9, 13, 14, 19, 10, 2, 20, 5, 15, 16, 17, 4, 12, 1, 8

void test_qsort()
{
    int a[] = { QSORT_DEFAULT_INPUT }, b[] = { QSORT_DEFAULT_INPUT }, *arr1, *arr2, nelems;

    QUICKSORT_INPUT;

    puts("\nUnsorted input:");
    test_quicksort(quicksort, arr1, nelems, cmp_ints);
    test_quicksort(rand_quicksort, arr2, nelems, cmp_ints);

    puts("\nAlready sorted input:");
    test_quicksort(quicksort, arr1, nelems, cmp_ints);
    test_quicksort(rand_quicksort, arr1, nelems, cmp_ints);
}

void rand_quicksort(int arr[], int left, int right, int (*cmp_fn)(void *, void *))
{
    if (left >= right) {
        ++itr;
        return;
    }

    int last;

    swap(arr, left, rand_range(left, right)); // ++

    last = left;

    for (int i = left + 1; i <= right; ++i) { /* partition */
        if (arr[i] < arr[left]) {
            swap(arr, ++last, i);
        }
        ++itr;
    }

    swap(arr, left, last); /* Place pivot in the final position (bubble sort) */
    rand_quicksort(arr, left, last - 1, cmp_fn);
    rand_quicksort(arr, last + 1, right, cmp_fn);
}


void quicksort(int arr[], int left, int right, int (*cmp_fn)(void *, void *))
{
    if (left >= right) {
        return;
        ++itr;
    }

    int last;

    last = left;

    for (int i = left + 1; i <= right; i++) { /* partition */
        if (arr[i] < arr[left]) {
            swap(arr, ++last, i);
        }
        ++itr;
    }

    swap(arr, left, last); /* Place pivot in the final position (bubble sort) */
    quicksort(arr, left, last - 1, cmp_fn);
    quicksort(arr, last + 1, right, cmp_fn);
}

int cmp_ints(void *v1, void *v2)
{
    return *(int *)v1 - *(int *)v2;
}

int rand_range(int l, int r)
{
    int rand = rand_mod(r + 1);
    return (rand < l) ? rand_range(l, r) : rand;
}

int rand_mod(int n)  // Uniform random generator
{
    static _Bool init;

    if (!init) {
        srand((unsigned int)time(NULL));
        init = true;
    }

    int pseudo_rand = rand();

    if (pseudo_rand == n) {  // Avoid higher probability for 0 (0 == 0, and 0 == n % n).
        return rand_mod(n);
    }

    return pseudo_rand % n;
}

/* swap: interchange v[i] and v[j] */
void swap(int arr[], int i, int j)
{
    int temp;
    temp = arr[i];
    arr[i] = arr[j];
    arr[j] = temp;
}

/* END */


/* Utilites. TO BE IGNORED. */
/* ======================================================================== */
void print_stack(Stack *stack, void(*print_fn)(int))
{
    while (!isEmpty(stack)) {
        (*print_fn)(pop(stack));
    }
}

void push(Stack *s, int elem)
{
    if (isFull(s)) {
        realloc_stack(s, s->size + 10);
    }
    s->st[s->n++] = elem;
}

int pop(Stack *s)
{
    return (!isEmpty(s)) ? s->st[s->n-- - 1] : INT_MIN; // -1 due to 0 .. n-1	indexing
}

_Bool isEmpty(Stack *s)
{
    return 0 == s->n;
}

_Bool isFull(Stack *s)
{
    return s->n >= s->size;
}

Stack alloc_stack(unsigned int size)
{
    Stack temp;
    temp.n = 0;
    temp.size = size;
    mem_arr(int, temp.st, size);

    return temp;
}

Stack realloc_stack(Stack *stack, unsigned int size)
{
    assert(stack, "", return alloc_stack(size));

    stack->size = size;
    stack->st = (int *)ec_realloc(stack->st, sizeof(int) * size);

    return *stack;
}

void free_stack(Stack *s)
{
    assert(s, "[Error] free_stack(): Null input.\n", exit(NULL_INPUT));
    free(s->st);
}

/* Return a token from a given file descriptor. */
String ftok(FILE *fd, const char *del)
{
    String token;
    unsigned int size = 30, s_index = 0;
    char c;

    assert(fd && del && *del, "[Error] ftok(): Null input.\n", exit(NULL_INPUT));

    mem_arr(char, token.s, size);

    /* Ignore initial delimiters. */
    while ((c = fgetc(fd)) != EOF && strchr(del, c)) { ; }

    if (c == EOF) {
        free(token.s);
        return (String) { NULL, 0 };
    }

    do { // fgetc() until EOF or delimiter is reached.
        if (s_index >= size) {
            token.s = (char *)ec_realloc(token.s, size += 30);
        }
        *(token.s + s_index) = c;
        ++s_index;
    } while ((c = fgetc(fd)) != EOF && !strchr(del, c));

    token.s = (char *)ec_realloc(token.s, s_index + 1); // "Tight fitting" string.
    token.s[s_index] = 0; // End string.
    token.len = s_index;

    return token;
}

/* Memory management. */
#define assert_alloc(allocator_name)                            \
    assert((new_ptr != NULL),                                   \
           "[Error] " #allocator_name "() returned NULL.\n",    \
           exit(RESOURCE_ACQUISITION_FAILED));
void *ec_realloc(void *ptr, size_t size)
{
    assert(size, "[Error] alloc(): size cannot be 0.\n", exit(DOMAIN_ERR));

    void *new_ptr;

    new_ptr = realloc(ptr, size);
    assert_alloc(realloc);

    return new_ptr;
}
void *ec_malloc(size_t size)
{
    return ec_realloc(NULL, size);
}
/* ======================================================================== */
