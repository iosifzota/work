#include <stdbool.h>
#include <time.h>
#include <math.h>
#include "stdio_wrap.h"
#include "stdlib_wrap.h"
#include "stack.h"

#define lg(b, n) (log(n)/log(b))

/* Uniform random generator */
int rand_mod(int);

/* Stack memory management */
Stack getStack(unsigned int);
void freeStack(Stack *);

/* FN */
void which_way(char, int, Stack *);
int bool_minimax(int [], int, int, int, int, Stack *);

unsigned int itr = 0;
int main()
{
    int choices[] = { 0, 1, 0, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 2, 2 };
    //int choices[] = { 0, 1, 1, 0, 0, 0, 0, 0 };
    int nchoices, max_depth, winnable;
    Stack path;

    nchoices  = NELEMS(choices);
    max_depth = lg(2, nchoices);
    path      = getStack(max_depth);

    winnable = bool_minimax(choices, 0, 0, max_depth, 1, &path);

    printf("Itr: %d\n", itr);

    if (winnable) {
        puts("Winnable!");

        while(!isEmpty(&path)) {
            int val = pop(&path);
            if (val == 1) {
                printf("[Right] ");
            }
            else if (val == -1) {
                printf("[L | R] ");
            }
            else {
                printf("[Left]  ");
            }
        }
        NEWLINE;
    }
    else {
        puts("Not winnable. :(");
    }

    return 0;
}

int bool_minimax(int choices[], int node_index, int depth, int max_depth, int player, Stack *path)
{

    if (depth == max_depth)
        return choices[node_index];

    ++itr;

    int choiceA, choiceB, rand;

    rand = rand_mod(2);

    if (player == 1) {

        choiceA = bool_minimax(choices, node_index*2 + rand, depth + 1, max_depth, 2, path);

        if (choiceA) {
            which_way('A', rand, path);
            puts("First choice.");
            return choiceA;
        }

        choiceB = bool_minimax(choices, node_index*2 + (1 - rand), ++depth, max_depth, 2, path);

        if (choiceB) {
            which_way('B', rand, path);
            printf("%d | %d\n", choiceA, choiceB);
            return choiceB;
        }

        puts("No optimal choice.");
        path->n = 0;
        return choiceA | choiceB;
    }

    choiceA = bool_minimax(choices, node_index*2 + rand, depth + 1, max_depth, 1, path);

    if (!choiceA) {
        puts("No optimal choice.");
        path->n = 0;
        return choiceA;
    }

    choiceB = bool_minimax(choices, node_index*2 + (1 - rand), ++depth, max_depth, 1, path);

    if (choiceB) {
        push(path, -1);
        printf("%d & %d\n", choiceA, choiceB);
        return choiceB;
    }

    puts("No optimal choice.");
    path->n = 0;
    return choiceA & choiceB;
}


void which_way(char choice, int rand, Stack *path)
{
    if ('A' == choice) {
        push(path, rand);
        return;
    }
    push(path, 1 - rand);
}


Stack getStack(unsigned int size)
{
    Stack temp;
    temp.n = 0;
    mem_arr(int, temp.st, size);

    return temp;
}
void freeStack(Stack *s)
{
    free(s->st);
}

int rand_mod(int n)  // Uniform random generator
{
    static _Bool init;

    if (!init) {
        srand((unsigned int) time(NULL));
        init = true;
    }

    int pseudo_rand = rand();

    if (pseudo_rand == n) {  // Avoid higher probability for 0 (0 == 0, and 0 == n % n).
        return rand_mod(n);
    }

    return pseudo_rand % n;
}

