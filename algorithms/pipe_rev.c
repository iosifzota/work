#include "stdio_wrap.h"
#include "stdlib_wrap.h"
#include <limits.h>

/*
  Read a length. A list of prices per length.
  Find the maximum revenue (0, or more cuts).
  n = pipe length;
  p[n] = prices array;
  
  r(n) = max(INT_MIN, p[i] + r(n-i)) | i = i, n // The revenue function.

  Declare R[n] // Memoize the revenuse.
  Declare S[n] // Store solution for n-th len.
               //(the i of the above formula the resulted in max revenue)
 */

typedef struct {
    int *R, *S;
    unsigned int n;
} Solution;

void max_revenue(Solution rev, int prices[]);
void put_max_revenue(Solution max_rev);

#define INPUT_FILE "pipe_input.txt"

int main()
{
    Solution revenue;
    int *prices;

    file_isEmpty_(INPUT_FILE, return -1);

    FILE *input = fopen(INPUT_FILE, "r");
    fscanf(input, "%u", &revenue.n);

    mem_arr(int, prices, revenue.n + 1);
    mem_arr(int, revenue.R, revenue.n + 1);
    mem_arr(int, revenue.S, revenue.n + 1);
    freadarr(input, "%d", prices + 1, revenue.n);

    max_revenue((Solution){ revenue.R, revenue.S, revenue.n + 1}, prices);

    put_max_revenue(revenue);

    free(prices); free(revenue.S); free(revenue.R);
    fclose(input);

    return 0;
}


void max_revenue(Solution rev, int prices[])
{
    int q, cut;

    rev.R[0] = 0;

    forIn(rn, 1, rev.n) {
        q = INT_MIN;

        forIn(prev_rn, 1, rn + 1) {
            cut = prices[prev_rn] + rev.R[rn - prev_rn];

            if (q < cut) {
                q = cut;
                rev.S[rn] = prev_rn; // Cut that cuppled with the prev_rn gives the max rn
            }
        }

        rev.R[rn] = q;
    }
}

void put_max_revenue(Solution max_rev)
{
    while(max_rev.n) { // Go back cut by cut and print at each step the size of the cut.
        printf("%d ", max_rev.S[max_rev.n]);
        max_rev.n -= max_rev.S[max_rev.n];
    }
}
