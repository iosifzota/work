#pragma once

#ifndef INCLUDE_misc_h__
#define INCLUDE_misc_h__

#define NEWLINE puts("");
#define dd printf("HERE %d\n", __LINE__);

#define NELEMS(x) (sizeof(x)/sizeof(x[0]))

#define max(x,y) (((x) > (y)) ? (x) : (y))
#define min(x,y) (((x) > (y)) ? (y) : (x))

/* For itr = [start, end]; */
#define forIn(itr, start, end) \
    for (unsigned int itr = (start); (itr) < (end); ++(itr))

#define assert(expr, err_msg, err_expr)         \
    if (!(expr)) {                              \
        printf(err_msg);			\
        err_expr;                               \
    }

#define if_true_else(expr, if_expr, else_expr)  \
    if ((expr)) {                               \
        if_expr;                                \
    }                                           \
    else {                                      \
        else_expr;                              \
    }
#define if_true(expr, if_expr) if_true_else(expr, if_expr,)

#define if_false_else(expr, if_expr, else_expr) \
    if (!(expr)) {                              \
        if_expr;                                \
    }                                           \
    else {                                      \
        else_expr;                              \
    }
#define if_false(expr, if_expr) if_false_else(expr, if_expr,)

#define arrcat(t, t_nelems, s, s_nelems) {      \
	forIn(s_index, 0, s_nelems) {           \
            t[t_nelems + s_index] = s[s_index]; \
	} }
	

/* Pseudo boolean. */
/*enum bool{
	false, true   // Use _Bool <stdbool.h>
};
typedef enum bool bool;
*/

/* Error codes. */
enum errcode {
	SUCCESS,
	ERROR,
	NULL_INPUT = -'I',
	NULL_OUTPUT = -'O',
	DOMAIN_ERR,  // Input does not belong to the domanin of the function.
	NOT_FOUND = 404,
	FOUND = -404,
	NIL = '0',
	RESOURCE_ACQUISITION_FAILED = 7
};
typedef enum errcode errcode;


/* Colors */
#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"

#endif
