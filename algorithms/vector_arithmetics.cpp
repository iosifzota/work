#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define NEWLINE std::cout << std::endl

/* BEIGN:  Error codes. */
#define SUCCESS         0
#define INPUT_ERR       1
#define DOMAIN_ERR      2
/* END_OF: Error codes. */

/* Used for signaling errors. (ex: strtol_ sets errno_ to DOMAIN_ERR if conversion failed. */
int errno_ = 0;  // Error codes are the ones defined in the header.

/* Returns the char if found in string otherwise returns 0.
 * Dependency of strtol_
 */
unsigned int strchr_(char *, char);



void *ec_realloc(void *ptr, unsigned int size)
{
    void *new_ptr;

    new_ptr = realloc(ptr, size);

    if (!new_ptr) {
        printf("[Error] ec_realloc(): realloc() returned NULL.");
        exit(-1);
    }

    return new_ptr;
}

void *ec_malloc(unsigned int size)
{
    return ec_realloc(NULL, size);
}


char *cin_getline()
{
    char *string;

    /* i - used for *iterating* over the block s points to. */
    unsigned int i = 0, size = 100;

    /* Allocate memory for string */
    string = (char *)ec_malloc(size);

    for (
        int c;
        /* *While* c != EOF ^ '\n'. */
        EOF != (c = getchar()) && '\n' != c;
        /* *Insert* c into string. */
        *(string + i) = c, i++
        )
    {
        /* Resize string to fit the current chr ^ more chrs. */
        if (i >= size) {
            string = (char *)ec_realloc(string, (size += 30));
        }
    }

    /* Discard unused memory (also needed *if* i == size). */
    string = (char *)ec_realloc(string, i + 1);

    /* End string. */
    *(string + i) = '\0';

    return string;
}

/* Converts a string to string of numbers.
 * Returns -1 if conversion failed and sets errno_.
 */
char *string_to_strnumber(const char *string)
{
    if (NULL == string || !*string) {
        printf("[Warning] string_to_strnumber(): Null input.\n");

        errno_ = INPUT_ERR;
        return NULL;
    }

    char *strnumber = (char *) malloc((strlen(string) + 1) * sizeof(char));


    /* Used for iterating over string. */
    unsigned int strI = 0;

    /* Used for forming strnumber. */
    unsigned int numI = 0;


    char white_space[] = " \n\t";

    /* Skip inital white space. */
    while ( strchr_(white_space,  string[strI]) ) {
        strI++;
    }

    /* Check for sign. */
    if ( string[strI] == '-') {
        strnumber[numI++] = string[strI++];
    }
    else if ( string[strI] == '+') {
        strI++;
    }
    

    /* If input string is null OR if input string contains only a sign (+ or -). */
    if (!string[strI]) {
        printf("[Warning] string_to_strnumber(): Please provide a number: %c[0-9].\n", string[strI - 1]);

        errno_ = INPUT_ERR;
        return NULL;
    }

    /* Ignore insegnificant zeorous */
    while ( string[strI + 1] && string[strI] == '0') {
        strI++;
    }

    while (string[strI]) {

        /* If string[i] is a digit. */
        if (string[strI] >= '0' && string[strI] <= '9') {
            strnumber[numI++] = string[strI++] - '0';
        }

        else if ( strchr_(white_space,  string[strI]) ) {

            /* Ignore white space after the number. */
            while ( strchr_(white_space,  string[strI]) ) {
                strI++;
            }
            if (string[strI]) {
                printf("[Error] A number has no white space.\n");
                errno_ = INPUT_ERR;
                return NULL;
            }
        }

        /* string[i] is not a digit. */
        else {
            printf("[Error] Conversion failed: NaN.\n");
            errno_ = DOMAIN_ERR;
            return NULL;
        }
    }

    strnumber[numI] = 'E';

    return strnumber;

}

unsigned long strnumber_len(char *strnumber)
{
    unsigned int len = 0;

    while (*strnumber != 'E') {
        len++;
        strnumber++;
    }

    return len;
}

/* =================================================
 * Strings.
 * =================================================
 */
unsigned int strchr_(char *s, char c)
{
    while (*s && *s != c) { s++; }

    return *s;
}




/*
 * [INPUT] 2 arrays of digits, their respective length, a numeric base
 * "Adds" two arrays of digits in the given base, by modifying the second array.
 */

template <class type>
unsigned int arradd(type [], type [], unsigned int, unsigned int, unsigned int);

template <class type>
unsigned int arrsub(type [], type [], unsigned int, unsigned int, unsigned int);

template <class type>
int arrborrow(type [], unsigned int, unsigned int);

/* Shift array (fill = 0) */
template <class type>
unsigned int arrnshift (type [], unsigned int, unsigned int, int);

/* Shift array and *fill* */
template <class type>
unsigned int arrnfill (type [], unsigned int, unsigned int, int, type);

template <class type>
int arrnshrink (type arr[], unsigned len, unsigned int startingI, int offset);


template <class type>
int arrnumber_cmp(type [], type [], unsigned int, unsigned int);

template <class type>
void arrncpy(type arrDest[], type arrSrc[], unsigned int len);



int init()
{
    char *string;
    string = cin_getline();


    char *token = NULL;
    char white_space[] = " \t\n";


    /* LEFT operand */
    char *operandA = NULL;

    if ( NULL == (token = strtok(string, white_space)) ) {
        puts("[Error] Please provide a left operand.");
        return -1;
    }
    if ( NULL == (operandA = string_to_strnumber(token)) ) {
        printf("[Error] Invalid left operand: \"%s\".\n", token);
        return -1;
    }



    /* Operation */
    char op = 0;
    if ( NULL == (token = strtok(NULL, white_space)) ) {
        puts("[Error] Please provide an operation.");
        return -1;
    }

    char operations[] = "+-*";
    if ( strlen(token) == 1 ) {
        if ( !(op = strchr_(operations, *token)) ) {
            printf("[Error] Undefined operation: %s\n", token);
            return -1;
        }
    }
    else {
        puts("[Error] Please provide valid operation.");
        return -1;
    }




    /* RIGHT operand */
    char *operandB = NULL;

    if ( NULL == (token = strtok(NULL, white_space)) ) {
        puts("[Error] Please provide a right operand.");
        return -1;
    }
    if ( NULL == (operandB = string_to_strnumber(token)) ) {
        printf("[Error] Invalid right operand: \"%s\".\n", token);
        return -1;
    }


    /* Check arity. */
    if ( NULL != (token = strtok(NULL, white_space)) ) {
        return -1;
    }




    unsigned int lenA, lenB;
    lenA = strnumber_len(operandA);
    lenB = strnumber_len(operandB);


    unsigned int result_len = 0;
    if (op == '+') {
        result_len = arradd(operandA, operandB, lenA, lenB, 10);
    }
    else if(op == '-') {
        result_len = arrsub(operandA, operandB, lenA, lenB, 10);
    }

    unsigned int i = 0;
    if (operandB[i] > 10) {
        printf("%c", operandB[i++]);
    }
    for (; i < result_len; i++) {
        printf("%d", operandB[i]);
    }
    NEWLINE;

    return 0;
}

int main()
{
    int ret;
    if ( (ret = init()) ) {
        puts("\n[Usage] [+-]0-9 +,-,* [+-]0-9: <number> <operation> <number>.");
    }

    return ret;
}


/* Find sign; return 1 for '+', -1 for '-' and 0 for not found. */
template <class type>
int find_sign(type term[])
{
    if (term[0] == '-') {
        return -1;
    }
    else if (term[0] == '+') {
        return 1;
    }

    // no sign
    return 0;
}



template <class type>
unsigned int arrsub(type termA[], type termB[], unsigned int lenA, unsigned int lenB, unsigned int base)
{
    /* Find sign, if any. (1 for +, -1 for -, 0 for not found.) */
    int signA = find_sign(termA);
    int signB = find_sign(termB);

    printf("subbing lA: %u, lB: %u\n", lenA, lenB);


    // Skip sign OR set sign to 1 if no sign was found.
    unsigned int termA_1st_digit, termB_1st_digit;
    termA_1st_digit = termB_1st_digit = 0;

    if (signA) {
        termA_1st_digit++;
    }
    else {
        signA = 1;
    }

    if (signB) {
        termB_1st_digit++;
    }
    else {
        signB = 1;
    }

    if (termA_1st_digit) {
        std::cout << termA[0];
    }
    for (unsigned int i = termA_1st_digit; i < lenA; i++) {
        std::cout << (int)termA[i];
    }

    std::cout << " - ";

    if (termB_1st_digit) {
        std::cout << termB[0];
    }
    for (unsigned int i = termB_1st_digit; i < lenB; i++) {
        std::cout << (int)termB[i];
    }

    std::cout << std::endl;


    /* Find the order relation between termA and termB. */
    int order = arrnumber_cmp(termA, termB, lenA, lenB);


    //    -a - b = - (a + b)                  a - -b = a + b
    if ( ((signA == -1) && (signB == 1)) || ((signA == 1) && (signB == -1)) ) {
        if (signA == -1) {

            if (termB_1st_digit == 0) {
                lenB = arrnshift(termB, lenB, termB_1st_digit, 1);
            }

            termB[0] = '-'; 
        }
        else {
            arrnshift(termB, lenB, 0, -1);
            lenB--;
        }

        return arradd<char>(termA, termB, lenA, lenB, base);
    }
    
    //    -a - (-b) = -a + b
    else if ( signB == -1 ) {
        signB = 1;
    }

    // The remaining calculation: a - b = a + (-b)
    else {
        signB = -1;
    }


    /* So there's either (-a + b) OR (a + (-b)) to calculate */


    printf("A: %d; B: %d\n", signA, signB);


    //[Obs] The next statemates make sure that the smaller number is the negative number.
    int result_sign = 0;


    /* Equal numbers. */
    if (!order) {
        termB[0] = 0;
        return 1;
    }

    /*If a > b AND -a + b to calculate; then flip signs: a + (-b). */
    else if (order > 0 && (signA == -1)) { 
        result_sign = signA;  // Set result_sign = sign of the greater number (a > b).

        signA = 1;
        signB = -1;

    }
    /*Elif a < b AND a + (-b) to calculate; then flip signs: -a + b */ 
    else if (order < 0 && (signB == -1)) {
        result_sign = signB;  // Set result_sign = sign of the greater number (b > a).

        signA = -1;
        signB = 1;
    }

    /* If termB (which will store the result) is smaller than termA. */
    if (
        (lenB - termB_1st_digit) < (lenA - termA_1st_digit)
        )
    {                                           
        /* Shift termB, starting at the first digit. */
        lenB = arrnshift<type>(termB, lenB, termB_1st_digit, lenA - lenB);
    }


    /* Operate */
    int iA, jB;

    iA = lenA - 1;
    jB = lenB - 1;

    int sum, transport;
    sum = transport = 0;

    printf("A: %d; B: %d\n", signA, signB);


    while (
           iA >= (int)termA_1st_digit &&
           jB >= (int)termB_1st_digit
           )
    {
        sum = termA[iA]*signA + termB[jB]*signB;

        if (sum < 0) {
            if (signA == 1) {
                sum += arrborrow(termA, iA, base);
            }
            else {
                sum += arrborrow(termB, jB, base);
            }

        }

        termB[jB] = sum;

        iA--; jB--;
    }

    if (result_sign == -1) {
        if (termB_1st_digit > 0) {
            termB[0] = '-';
        }
        else {
            lenB = arrnfill(termB, lenB, 0, 1, '-');
        }
    }
    else if (termB_1st_digit > 0) {
        arrnshift(termB, lenB, 0, -1);
        lenB--;
    }



    return lenB;
}


template <class type>
int arrborrow(type arr[], unsigned int poz, unsigned int base)
{
    int i, found;

    i = poz - 1; // The next position to look for borrow.
    found = 0;


    printf("b in: ");
    for (unsigned int j = 0; j <= poz; j++) {
        std::cout << (int)arr[j];
    }
    NEWLINE;
    printf("\nb|%u|: %u\n", i, arr[i]);

    /* Find borrow right to left, and get borrow if possible. */
    for (; i >= 0 && !found; i--) {
        if (arr[i] && arr[i] < (int)base) {

            printf("f|%u|: %u\n", i, arr[i]);

            arr[i]--;
            found = 1;
        }
    }

    /* Nowhere to borrow from. */ 
    if (!found) {
        return -1;
    }

    /* Distribute the borrow, left to right. */
    for (i += 2; i < (int)poz; i++) {
        arr[i] += base - 1;
    }

    /* The borrower gets full unit. */
    arr[i] += base;

    return base;
}

template <class type>
unsigned int arradd(type termA[], type termB[], unsigned int lenA, unsigned int lenB, unsigned int base)
{

    /* Find sign, if any. (1 for +, -1 for -, 0 for not found.) */
    int signA = find_sign(termA);
    int signB = find_sign(termB);

    printf("adding lA: %u, lB: %u\n", lenA, lenB);

    // Skip sign OR set sign to 1 if no sign was found.
    unsigned int termA_1st_digit, termB_1st_digit;
    termA_1st_digit = termB_1st_digit = 0;

    if (signA) {
        termA_1st_digit++;
    }
    else {
        signA = 1;
    }

    if (signB) {
        termB_1st_digit++;
    }
    else {
        signB = 1;
    }


    if (termA_1st_digit) {
        std::cout << termA[0];
    }
    for (unsigned int i = termA_1st_digit; i < lenA; i++) {
        std::cout << (int)termA[i];
    }

    std::cout << " + ";

    if (termB_1st_digit) {
        std::cout << termB[0];
    }
    for (unsigned int i = termB_1st_digit; i < lenB; i++) {
        std::cout << (int)termB[i];
    }

    std::cout << std::endl;

    /* If signs are different, return call arrsub. */
    if (signA != signB) {

        if (signA == -1) {
            lenA = arrnshift(termA, lenA, 0, -1);

            return arrsub(termB, termA, lenB, lenA, base);
        }

        else {
            lenB = arrnshift(termB, lenB, 0, -1);

            return arrsub(termA, termB, lenA, lenB, base);
        }
    }

    /* If termB (which will store the result) is smaller than termA. */
    if (
        (lenB - termB_1st_digit) < (lenA - termA_1st_digit)
        )
    {                                           
        /* Shift termB, starting at the first digit. */
        lenB = arrnshift<type>(termB, lenB, termB_1st_digit, lenA - lenB);
    }


    /* Operate */

    int iA, jB;

    iA = lenA - 1;
    jB = lenB - 1;

    int sum, transport;
    sum = transport = 0;


    while (
           iA >= (int)termA_1st_digit &&
           jB >= (int)termB_1st_digit
           )
    {
        sum = termA[iA] + termB[jB] + transport;

        termB[jB] = sum % (int)base;
        transport = (sum >= (int)base) ? 1 : 0;           // t = (sum - termB[i]) / base;

        iA--; jB--;
    }


    /* While transport & there's more of termB (term[jB]-isDigit) add transport. */
    while (jB >= (int)termB_1st_digit && transport) {
        sum = termB[jB] + transport;

        termB[jB] = sum % (int)base;
        transport = (sum >= (int)base) ? 1 : 0;

        jB--;
    }

    /* If there's transport: shift by 1, starting at the index of 1st_digit
     * and fill new position with 1 (transport);
     */
    if (transport) {
        arrnfill<type>(termB, lenB, termB_1st_digit, 1, transport);
        lenB++;
    }

    return lenB;
}
    
template <class type>
int arrnshrink (type arr[], unsigned len, unsigned int startingI, int offset)
{
    offset *= -1;

    if (offset >= (int)len) {
        return len;
    }

    /* Start at the startingIndex. */
    int begin = startingI;
    /* End at the last Index(len-1), minus the offset. */
    int end = len - 1 - offset;

    /* Left to right, a[i] = a[i + offset]. */
    for (
        int i = begin;
        i <= end;
        i++)
    {
        arr[i] = arr[i + offset];
    }

    arr = (type *) realloc(arr, (len - offset) * sizeof(type));
     
    return len - offset;
}


/* Righit shift and fill. */
template <class type>
unsigned int arrnfill (type arr[], unsigned len, unsigned int startingI, int offset, type fill)
{

    arr = (type *) ec_realloc(arr, len * sizeof(type) + offset);

    /* Shift after end of arr = fill len -> len + offset =
     * = left shift by enlarging the arr.
     */
    if (startingI >= len) {
        len = len + offset;

        for (unsigned int i = len - offset; i < len; i++) {
            arr[i] = fill;
        }

        return len;
    }

    /* Start at last Index(len-1), plus the offset. */
    int begin = len - 1 + offset;
    /* End at the startingIndex + offset. */
    int end = offset + startingI;
    
    /* Right to left, a[i] = a[i - offset] */
    for (
        int i = begin;
        i >= end;
        i--)
    {
        arr[i] = arr[i - offset];
    }

    /* Left to right, from startingIndex until (startingIndex + offset), fill. */
    for (int i = end - offset; i < end; i++) {
        arr[i] = fill;
    }

    return len + offset;
}


/* Shift array (fill = 0) */
template <class type>
unsigned int arrnshift (type arr[], unsigned len, unsigned int startingI, int offset)
{
    if (!offset || !len) {
        return len;
    }

    /* Shift by enlarging to the left(+offset)/right(-offset) */
    if (offset > 0) {
        return arrnfill<type>(arr, len, startingI, offset, 0);
    }

    return arrnshrink<type>(arr, len, startingI, offset);
}


template <class type>
int arrnumber_cmp(type termA[], type termB[], unsigned int lenA, unsigned int lenB)
{
    unsigned int iA, jB;

    iA = jB = 0;

    if (find_sign(termA)) {
        iA++; 
    }

    if (find_sign(termB)) {
        jB++;
    }

    if ( (lenA - iA) - (lenB - jB) ) {
        return (lenA - iA) - (lenB - jB);
    }

    for (; iA < lenA; iA++, jB++) {
        if (termA[iA] != termB[jB]) {

            return termA[iA] - termB[jB];
        }
    }

    return 0;
}


template <class type>
void arrncpy(type arrDest[], type arrSrc[], unsigned int len)
{
    for (unsigned int i = 0; i < len ; i++) {
        arrDest[i] = arrSrc[i];
    }
}
