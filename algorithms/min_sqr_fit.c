#include "stdlib_wrap.h"
#include "stdio_wrap.h"

typedef struct Rectangle {
    int l, h;
} Rectangle;

void put_pal(Rectangle pal);
void min_sqr_fit(Rectangle pal);

int main()
{
    Rectangle pal;
    
    printf("l h: ");
    scanf("%d %d", &pal.l, &pal.h);


    min_sqr_fit(pal);

    return 0;
}

void put_pal(Rectangle pal)
{
    printf("%dx%d", pal.l, pal.h);
}

void swap(int *a, int *b)
{
    int aux;
    aux = *a;
    *a = *b;
    *b = aux;
}

void min_sqr_fit(Rectangle pal)
{
    int sqr, count;

    if (pal.l < pal.h) {
        swap(&pal.l, &pal.h);
    }

    NEWLINE;

    for(sqr = pal.h; sqr; sqr = min(pal.l, pal.h)) {
        count = 0;
        while (pal.l - sqr >= 0) {

            pal.l -= sqr;
            ++count;
        }

        printf("%d * (", count); put_pal((Rectangle){ sqr, sqr }); printf(")\n");

        if (pal.l < pal.h) {
            swap(&pal.l, &pal.h);
        }
    }
}
