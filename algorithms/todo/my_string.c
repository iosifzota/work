#include "my_string.h"

int strcmp_va(unsigned int nstrings, ...)
{

    va_list strings_va;

    char *str;

    bool equal = false;

    /* Init strings_va. */
    va_start(strings_va, nstrings);

    /* Get the string string to compare to. */
    str = va_arg(strings_va, char *);

    /* Compare each string to =str=. */
    forIn(index, 0, nstrings) {
        if ( !strcmp_(str, va_arg(strings_va, char *)) ) {
            equal = true;
            break;
        }
    }

    va_end(strings_va);

    return equal;
}

int strcmp_(const char *s, const char *t)
{
    unsigned int index = 0;

    for (; *(s + index); ++index) {
        if ( *(s + index) - *(t + index) ) {
            break;
        }
    }

    return *(s + index) - *(t + index);
}
