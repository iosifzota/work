#include "wrappers.h"

int puti(long long int_var)
{
    if (int_var > 9)
        puti(int_var/10);

    putchar('0' + (int_var % 10));

    return SUCCESS;
}

char *cin_getline()
{
    char *line;
    int c;

    /* i - used for *iterating* over the block s points to. */
    unsigned int i = 0, line_size = 100;

    /* Allocate memory for line */
    line = (char *) ec_malloc(line_size);

    /* Skip initial '\n'. */
    for (; EOF != (c = getchar()) && '\n' == c; ) { ; }

    /* Get the valid char that stop the previous for. */
    if ( EOF != c && '\n' != c) {
        line[i++] = c;
    }

    while (EOF != c && '\n' != c) {

        if (i >= line_size) {
            line = (char *) ec_realloc(line, (line_size += 30));
        }
        
        line[i++] = c;
    }

    for (;
        /* *While* c != EOF ^ '\n'. */
        EOF != (c = getchar()) && '\n' != c;
        /* *Insert* c into line. */
        line[i++] = c
        )
    {
        /* Resize line to fit the current chr ^ more chrs. */
        if (i >= line_size) {
            line = (char *) ec_realloc(line, (line_size += 30));
        }
    }

    /* Discard unused memory (also needed *if* i == line_size). */
    line = (char *)ec_realloc(line, i + 1);

    /* End line. */
    *(line + i) = '\0';

    return line;
}


FILE *ec_fopen(const char *file_path, const char *mode)
{
    FILE *fd;

    fd = fopen(file_path, mode);

    if ( NULL == fd ) {
        puts("[Error] fopen() returned NULL.");
        exit(-1);
    }

    return fd;
}

void free_va(unsigned int n_ptrs, ...)
{
    va_list ptrs;

    va_start(ptrs, n_ptrs);

    forIn (ptrc, 0, n_ptrs) {
        free(va_arg(ptrs, void*));
    }

    va_end(ptrs);
}

void *ec_realloc(void *ptr, size_t size)
{
    void *new_ptr;
 
    new_ptr = realloc(ptr, size);
 
    if (new_ptr == NULL) {
        puts("[Error] ec_realloc(): realloc() returned NULL.");
    }
 
    return new_ptr;
}
void *ec_malloc(size_t size)
{
    return ec_realloc(NULL, size);
}
