#ifndef INCLUDE_my_string_h__
#define INCLUDE_my_string_h__

#include "misc.h"
#include <stdarg.h>

int strcmp_va(unsigned int, ...);
int strcmp_(const char *, const char *);

#endif
