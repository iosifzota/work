#include "wrappers.h"

int binsearch_sum(int, int, int);

int main()
{
    unsigned int ntests;
    int candidate;

    FILE *fd;

    fd = ec_fopen("numbers", "r");

    fscanf(fd, "%d", &ntests);

    forIn (testc, 0, ntests) {
        fscanf(fd, "%d", &candidate);

        printf("Testing %d ...\n", candidate);
        printf("R: %d\n", binsearch_sum(1, candidate, candidate));
    }


    fclose(fd);
}

int binsearch_sum(int left, int right, int key)
{

    if (left > right) {
        return -1;
    }

    int mid_index = (left + right) / 2;

    int mid_sum = mid_index * (mid_index + 1) / 2;

    //printf("(%d): %d\t", mid_index, mid_sum);

    if (key == mid_sum ) {
        return mid_index;
    }

    if (key > mid_sum) {
        return binsearch_sum(mid_index + 1, right, key);
    }

    /* key < mid_sum */
    return binsearch_sum(left, mid_index - 1, key);
}
