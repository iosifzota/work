#define mem_arr(type, ptr, size) (ptr) = (type *) ec_malloc(size * sizeof(type))

#define array(type) struct { type *ptr; unsigned int size; } 

#define array_resize(type, array, new_size) {   \
        mem_arr(type, (array).ptr, new_size);   \
        (array).size = new_size;                \
    }

#define array_foreach(type, array, element)                             \
    type *element;                                                      \
    for (                                                               \
        unsigned int index = 0;                                         \
        index < (array).size && ((element) = &(array).ptr[index]);      \
        ++index                                                         \
        )


#define resize(type, vector, new_size) {        \
        mem_arr(type, vector.v, new_size);      \
        vector.size = new_size;                 \
    }

