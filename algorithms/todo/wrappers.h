#ifndef INCLUDE_mylib__
#define INCLUDE_mylib__

#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>

#include "misc.h"

/*
 * <stdlib.h> wrappers:
 */
void *ec_malloc(size_t);
void *ec_realloc(void *, size_t);
void free_va(unsigned int ptrc, ...);

/* Allocate meory for an array of the given type */
#define mem_arr(type, ptr, size) \
    (ptr) = (type *) ec_malloc(size * sizeof(type))

/* Allocate/Deallocate a matrix of any type. */
#define mem_matrix(type, matrix, rows, cols)    \
    mem_arr(type*, matrix, rows);               \
    forIn (i, 0, rows) {                        \
        mem_arr(type, matrix[i], cols);         \
    }
#define free_matrix(matrix, rows)               \
    forIn (i, 0, rows) {                        \
        free(matrix[i]);                        \
    }                                           \
    free(matrix);

/* fopen() wrapper. */
FILE *ec_fopen(const char *file_path, const char *mode);


/*
 * <stdio.h> wrappers:
 */
/* Print an integer variable */
int puti(long long);

/* Get a line (*string) (del: EOF, '\n') from stdin.
 * !! Retuns heap memory. The caller is responsible
 *    for deallocating it !!
 */
char *cin_getline();

#define TAB(length) printf("%" #length "c", 0)

/* Read and print an array, given a format_specifer */
#define freadarr(fd, format_specifier, arr, nelems)     \
    forIn (i, 0, nelems) {                              \
        fscanf(fd, format_specifier, arr + i);          \
    }
#define fprintarr(fd, format_specifier, arr, nelems)    \
    forIn (i, 0, nelems) {                              \
        fprintf(fd, format_specifier " ", arr[i]);      \
    }                                                   \
    NEWLINE;                                            \

/* Read and print a matrix. */
#define freadmat(fd, format_specifier, matrix, rows,  cols)             \
    forIn (ln_index, 0, rows) {                                         \
        forIn (col_index, 0, cols) {                                    \
            fscanf(fd, format_specifier, mat[ln_index] + col_index);    \
        }                                                               \
    }
#define fprintmat(fd, format_specifier, matrix, rows,  cols)            \
    NEWLINE;                                                            \
    forIn (ln_index, 0, rows) {                                         \
        forIn (col_index, 0, cols) {                                    \
            fprintf(fd, format_specifier " ",mat[ln_index][col_index]); \
        }                                                               \
        NEWLINE;                                                        \
    }                                                                   \
    NEWLINE;

#endif
