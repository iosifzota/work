#include<iostream>
#include<cstdlib>
#include<stdio.h>



void *ec_malloc(unsigned long size);
void *ec_realloc(void *ptr, unsigned long size);

/* Read array. */
template <class type>
void readarr(type arr[], unsigned long n)
{
    for (unsigned long i = 0; i < n; i++) {
        std::cin >> arr[i];
    }
}


/* Print array. */
template <class type>
void printarr(type arr[], unsigned long n)
{
    for (unsigned long i = 0; i < n; i++) {
        std::cout << arr[i] << " ";
    }
    std::cout << std::endl;
}

template <class type>
inline void swap(type arr[], unsigned long i, unsigned long j)
{
    unsigned long aux = arr[i];
    arr[i] = arr[j];
    arr[j] = aux;
}



template <class type>
void qsort(type arr[], int left, int right);

template <class type>
void quicksort(type arr[], int n)
{
    return qsort(arr, 0, n - 1);
}



int main()
{
    int n;

    std::cout << "n: ";
    std::cin >> n;

    int *arr;

    arr = (int *) ec_malloc(n * sizeof(int));

    readarr(arr, n);

    quicksort(arr, n);

    printarr(arr, n);

    return 0;

}


template <class type>
void qsort(type arr[], int left, int right)
{
    if (left >= right) {
        return;
    }

    /* Pivot */
    swap(arr, left, (left + right) / 2);

    /* Parition */
    int last = left;

    for (int i = left + 1; i <= right; i++) {
        if (arr[i] < arr[left]) {
            swap(arr, ++last, i);
        }
    }


    swap(arr, left, last);
    qsort(arr, left, last - 1);
    qsort(arr, last + 1, right);
}


void *ec_realloc(void *ptr, unsigned long size)
{
    void *new_ptr;

    new_ptr = realloc(ptr, size);

    if (new_ptr == NULL) {
        puts("[Error] ec_realloc(): realloc() returned NULL.");
    }

    return new_ptr;
}
void *ec_malloc(unsigned long size)
{
    return ec_realloc(NULL, size);
}
