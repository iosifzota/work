#include <string.h>

#include "my-stdlib.h"
#include "stack.h"

/* Includes stdlib.h, stdio.h, ec_malloc, ec_realloc, get_line. */
#include "stdlib-stdio-ec.h"


/* Template for evaluated reverse polish expression. */
typedef struct {
	char *expr;
	long long value;
} polish_expr;

/* Polish calculator (reverse polish notation)
 * [INPUT]  : String
 * [OUTPUT] : Reverse polish expression
 *          & the result of evaluating the expression.
*/

/* calculator(): sets up storage for the calculator_body, and calls _body.
 * Afterwards frees the previously allocated memory and returns
 * the result received from calling _body.
 */
polish_expr calculator(char *);

/* calculator_body(): returns the evaluated expression and the result as polish_expr.
 * Handles bad input, arity errors, ignores white space. In case of error,
 * returns NULL for the expression, and 0 for the value.
 */
polish_expr calculator_body(char *, stack *, char *, char *);  /* Returns heap memory! (.expr) */



/* Takes a stack, a binary function, and a symbol (of the operator)
 * used for error checking and printing.
 */
int stackOperate(stack *, long long (*)(long long, long long), char );


/* Print arity error message for the given operator (%c). */
void err_op_arity(char);


/* Template for parsed tokens. */
typedef struct {
	char type;
	unsigned long long value;
} data;

#define NATURAL_NUMBER  'N'
#define LETTER          'L'
/* Returns a struct *data* with value and type of string contents.
 * If !NATURAL_NUMBER && !LETTER returns (negative number) as value and for type 0.
*/
data parse_token(char *);



/* Dinamically allocated stack (the vector). */
stack getStack(unsigned int);
/* Free allocated memory of stack (the vector). */
void freeStack(stack *);



/* Start calculator */
int init()
{
	char *string;

	/* Get a line (string) from stdin. */
    string = cin_getline();


	/* Pass string to calculator. */
	polish_expr expr = calculator(string);

    /* If string was not a valid expression, terminate. */
    if (NULL == expr.expr) {
        free(string);

        return -1;
    }

	/* Print the evaluated expresion and the result. */
	printf("\n%s = %lld\n", expr.expr, expr.value);


	/* Free memory */
	free(expr.expr);
	free(string);

	return 0;
}


int main()
{
	return init();
}


polish_expr calculator(char *string)
{
	unsigned long input_size = strlen(string) + 1;           /* + 1 to account for '\0'. */

	/* The polish expr. */
	polish_expr result;

	/* Used by _body for storing operands. */
	stack s = getStack(input_size);

	/* Used by _body for storing tokens of *string. */
	char *token = (char *) ec_malloc(input_size + 1);        /* + 1 for '\0'. */

	/* Setup storage for the evaluated expression. */
	char *expr = (char *) ec_malloc(strlen(string) + 2);     /* + 1 for '\0'; + 1 due to trailing [space]. */


    // Get result (and the evaluade expression)
	result = calculator_body(string, &s, token, expr);


	/* Free memory */
	freeStack(&s);
	free(token);

	return result;
}

polish_expr calculator_body(char *string, stack *st, char *token, char *expr)
{
    /* Init. needed for strncat to work (no segfaults). */
	expr[0] = '\0';

    polish_expr result = { NULL, 0 };


    /* Used for storing parsed token. */
	data input;

	/* Statistic used for validation of *expr
	 * and error reporting.
	 */
	char last_operation = 0;
    unsigned int err = 0;


    unsigned int index = 0, j;

	/* While there's more of the string to process. */
	while (
           string[index] && !err
		   )
	{

        /* Skip white space. */
        while (string[index] && strchr(" \t\n", string[index])) {
			index++;
		}


		/* Get token. */
        j = 0;
        while ( string[index] && !strchr(" \t\n", string[index]) ) {
            token[j++] = string[index++];
        }
        token[j] = '\0';
        /* GOT token! */



		/* Get data from token. */
		input = parse_token(token);


		/* If it's a natural number, push it on the stack. */
		if (input.type == NATURAL_NUMBER) {
			push(st, input.value);


			/* Forming of expr. */
            strncat(expr, token, strlen(token));
            strncat(expr, " ", 1);
		}

		else if (input.type == LETTER) {
			/* Operate */

			switch (input.value) {

			case '+':
				if (SUCCESS != stackOperate(st, add, input.value))
                    err++;

				last_operation = (char) input.value;


				/* Forming of expr. */
				strncat(expr, token, strlen(token));
				strncat(expr, " ", 1);
				break;


			case '-':
				if (SUCCESS != stackOperate(st, substract, input.value))
                    err++;

				last_operation = (char) input.value;


				/* Forming of expr. */
				strncat(expr, token, strlen(token));
				strncat(expr, " ", 1);
				break;


			case '*':
				if (SUCCESS != stackOperate(st, multiply, input.value))
                    err++;

				last_operation = (char) input.value;


				/* Forming of expr. */
				strncat(expr, token, strlen(token));
				strncat(expr, " ", 1);
				break;


			case '/':
				if (SUCCESS != stackOperate(st, divide, input.value))
                    err++;

				last_operation = (char) input.value;


				/* Forming of expr. */
				strncat(expr, token, strlen(token));
				strncat(expr, " ", 1);
				break;


			case '%':
				if (SUCCESS != stackOperate(st, mod, input.value))
                    err++;

				last_operation = (char) input.value;


				/* Forming of expr. */
				strncat(expr, token, strlen(token));
				strncat(expr, " ", 1);
				break;

			default:
				printf("\n[Error] Undefined operator: \"%s\"\n", token);
                err++;
			}
		}

		else if (*token) {
			/* If token is !natural_number, !letter and !empty : bad input! */

			printf("\n[Error] Bad input: \"%s\"\n", token);
            err++;
		}
	}


	/* If there were operands, but no operation(s). */
	if (!err && !last_operation && !isEmpty(st)) {
		printf("\n[Error] Please provide an operation.\n");
        err++;
	}

	/* If too many operands were given. */
	if (!err && last_operation && heightOf(st) > 1) {
		err_op_arity(last_operation);
        err++;
	}

	/* If err OR if input was null, show USAGE message. */
    if (err || !last_operation) {
		printf("Usage: a b [+-*/] | long a, b.\n");

        free(expr);

        return result;
	}


	/* Remove trailing [space]. */
	expr[strlen(expr)] = '\0';

	result.expr = expr;
    result.value =  pop(st);

	return result;
}


data parse_token(char *token)
{
	data d = { 0, 0 };

	long long value = 0;
	char type = '0';

	/* Convert token to long. */
	if ((value = strtol_(token)) >= 0) {
		type = NATURAL_NUMBER;
	}

	/* We're dealing with non-numbers then. */
	else if (strlen(token) == 1) {
		type = LETTER;
		value = *token;
	}
	/* Else not a number nor single letter. */


	d.value = (unsigned long long) value;
	d.type = type;

	return d;
}


int stackOperate(stack *s, long long (*operator)(long long, long long), char symbol)
{

    if (heightOf(s) < 2) {
        err_op_arity(symbol);

        return -1;
    }

	long long termL, termR, result;

    termR = pop(s);
    termL = pop(s);

    if ( (symbol == '/' || symbol == '%') && !termR ) {
        printf("\n[Error] Ilegal operation; trying to %c by 0: \"%lld %lld %c\".\n", symbol, termL, termR, symbol);

        return DOMAIN_ERR;
    }

    result = (*operator)(termL, termR);
    push(s, result);

    return SUCCESS;

}

void err_op_arity(char op)
{
	printf("\n[Error] The \"%c\" operator takes two operands: \"a b %c\" | long a, b.\n", op, op);
}


stack getStack(unsigned int size)
{
	stack temp;
    temp.n = 0;
	temp.st = (long long *) ec_malloc(sizeof(long long) * size);

	return temp;
}
void freeStack(stack *s)
{
	free(s->st);
}
