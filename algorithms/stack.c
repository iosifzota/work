#include "stack.h"

void push(Stack *s, int elem)
{
	s->st[s->n++] = elem;
}

int pop(Stack *s)
{
	return s->st[s->n-- - 1]; // -1 due to 0 .. n-1	indexing
}

unsigned int heightOf(Stack *s)
{
	return s->n;
}

unsigned int isEmpty(Stack *s)
{
	return 0 == heightOf(s);
}

unsigned int isFull(Stack *s, unsigned int size)
{
	return heightOf(s) >= size;
}
