#include <stdlib.h>
#include <stdio.h>

#define SUCCESS   0
#define INPUT_ERR 1
#define NOT_FOUND (-1)

#define NEWLINE puts("");

int strchr_(char *s, char c);

void *ec_malloc(size_t size);
void *ec_realloc(void *ptr, size_t size);

#define mm(size, type) (type *) ec_malloc(size * sizeof(type))


typedef struct Tnode Tnode;
struct Tnode {
  int value;
  Tnode *next;
};


// Print a list.
void print_list(Tnode *);

// Add a node
Tnode *add_node(Tnode *, int);

// Delete a node.
Tnode del_node(Tnode **, int);


// switch options
void option_delete(Tnode **);
void option_add(Tnode **);


int init()
{
    Tnode *list = NULL;

    char c;

    while ( (c = getchar()) != EOF ) {
        switch(c) {
        case 'q':
            return SUCCESS;
        case '0':
            return SUCCESS;
        case 'p':
            print_list(list);
            break;
        case 'n':
            option_add(&list);
            break;
        case 'a':
            option_add(&list);
            break;
        case 'r':
            option_delete(&list);
            break;
        case 'd':
            option_delete(&list);
            break;
        case 'c':
            system("clear");
            break;
        default:
            if ( !strchr_(" \n\0", c) ) {
                puts("Undefined option!");
            }
            break;
        }
    }

    return SUCCESS;
}


int tst() {


    return SUCCESS;
}

int main()
{
    return init();
    //return tet();
    //return SUCCESS;
}


void option_delete(Tnode **list)
{
    int value;

    // Read value to add.
    printf("Remove: ");
    scanf("%d", &value);

    printf("Removed %d.\n", del_node(list, value).value);
}

void option_add(Tnode **list)
{
    int value;

    // Read value to remove.
    printf("Value: ");
    scanf("%d", &value);

    *list = add_node(*list, value);

    printf("Added %d.\n", (*list)->value);
}

// Add a node to a list.
Tnode *add_node(Tnode *HEAD, int value)
{
    Tnode *new_node = mm(1, Tnode);

    new_node->value = value;
    new_node->next = HEAD;

    return new_node;
}

// Delte a node from a list.
Tnode del_node(Tnode **HEAD, int search_value)
{
    Tnode removed_node = (Tnode){0, NULL};

    if (!HEAD) {
        puts("[Error] **HEAD is NULL.");
        return removed_node;
    }

    if (!*HEAD) {
        puts("[Error] List is empty.");
        return removed_node;
    }

    /* Skip until there's no next node or value == nod_value */
    while ( (*HEAD)->next && (*HEAD)->value != search_value ) {
        HEAD = &(*HEAD)->next;
    }

    /* Check if the current node has the search value.
     * [Note] The current node is either the last OR the
     * node that has the search_value.
     */
    if ( (*HEAD)->value == search_value ) {

        /* Save the value of the node to be removed. */
        removed_node = **HEAD;
        free(*HEAD); // Free memory of node.

        *HEAD = (*HEAD)->next; // Fix linking.

        return removed_node;
    }

    printf("Value %d found in the list.\n", search_value);
    return removed_node;
}


// Print a list.
void print_list(Tnode *HEAD)
{
    if (!HEAD) {
        puts("(empty)");
        return;
    }

    while (HEAD) {
        printf("%d ", HEAD->value);

        HEAD = HEAD->next;
    }
    NEWLINE;
}


/* Similar to strchr but in case of success
 * it returns an int value, insted of a pointer.
 */
int strchr_(char *s, char c)
{
    while (*s && *s != c) {
        s++;
    }
    return *s;
}


void *ec_realloc(void *ptr, size_t size)
{
	void *new_ptr;

	new_ptr = realloc(ptr, size);

	if (!new_ptr) {
		perror("[Error] realloc() returned NULL\n");
		exit(-1);
	}

	return new_ptr;
}

void *ec_malloc(size_t size)
{
	return ec_realloc(NULL, size);
}
