#include "stdio_wrap.h"
#include "stdlib_wrap.h"
#include <string.h>

void aaab(char str[], int lo, int hi, int *as, int *bs);

int main()
{
    char *string;
    int len, as = 0, bs = 0;

    printf("String size: "); scanf("%d", &len);

    mem_arr(char, string, len + 1);
    scanf("%s", string);

    forIn(i, 0, (size_t)len - 1) {
        if(!strchr("ab", string[i])) {
            puts("[Error] Only a's and b's.");
            return -1;
        }
    }

    aaab(string, 0, strlen(string) - 1, &as, &bs);
    printf("a: %d\tb: %d\n", as, bs);

    free(string);
    return 0;
}

void aaab(char *str, int lo, int hi, int *as, int *bs)
{

    if (lo > hi) {
        return;
    }

    int mid = (lo +hi)/2;

    if (str[mid] == 'a') {
        *as += mid - lo + 1;
        return aaab(str, mid + 1, hi, as, bs);
    }

    *bs += hi - mid + 1;
    return aaab(str, lo, mid - 1, as, bs);
}
