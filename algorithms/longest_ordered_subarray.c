#include "stdio_wrap.h"
#include "stdlib_wrap.h"

#define INIT(input_file) do {                   \
        file_isEmpty_(input_file, return 0);    \
	FILE *fp = ec_fopen(input_file, "r");   \
                                                \
	fscanf(fp, "%d", &nelems);              \
        mem_arr(int, arr, nelems);              \
        mem_arr(int, result, nelems);           \
                                                \
	freadarr(fp, "%d ", arr, nelems);       \
        fclose(fp);                             \
    } while(0);

/*
v = { 6, 2, 3, 1, 8, 9, 7, 8, 10 }  

L = { 4, 5, 4, 4, 3, 2, 3, 2, 1 }
S = { 4, 2, 4, 4, 5, 8, 7, 8, 8 }

*/

unsigned int longest_ord_subarr(int [], int [], unsigned int);
int constr_solution(int arr[], int S[], int result[], int );

int ordered_max_search(int [], int [], unsigned int, int);
int max_search(int [], unsigned int);

#define INPUT_FILE "input.txt"

int main()
{
    int *arr, *result;
    unsigned int nelems, result_nelems;

    INIT(INPUT_FILE);

    result_nelems = longest_ord_subarr(arr, result, nelems);
    fprintarr(stdout, "%d ", result, result_nelems);

    free(arr); free(result);

    return 0;
}


unsigned int longest_ord_subarr(int arr[], int result[], unsigned int nelems)
{

    if (nelems < 1) {
        return 0;
    }

    int *L, *S, hi, max_index;

    /* Allocate memory */
    mem_arr(int, S, nelems);
    mem_arr(int, L, nelems);

    /* Setup length for highest_index(hi) element. */
    L[(hi = nelems - 1)] = 1;
    S[hi] = hi;

    /* Right to left calculate lengths. */
    for (int indexL = hi - 1; indexL >= 0; --indexL) { 
        /* Left to right, check for longest attachable length. */
        max_index = ordered_max_search(L, arr, nelems, indexL);

        L[indexL] = 1 + L[max_index];
        S[indexL] = max_index;
    }

    unsigned int result_nelems = constr_solution(arr, S, result, max_search(L, nelems));

    free(L); free(S);

    return result_nelems;
}

int constr_solution(int arr[], int S[], int result[], int sol_index)
{
    unsigned int result_nelems = 0;

    /* Write the beiginning elemnt to the result array. */
    result[result_nelems++] = arr[sol_index];

    /* Follow S array and fill result array. */
    while (S[sol_index] != sol_index) {
        sol_index = S[sol_index];

        result[result_nelems++] = arr[sol_index];
    }

    return result_nelems;
}


/* return:
 *  | index, L[index] == max{ L[i] | order[i] <= order[order_key_index], i = range(order_key_index + 1, nelems) }
 *  | otherwise order_key_index.
 */
int ordered_max_search(int search_v[], int order[], unsigned int nelems, int order_key_index)
{
    int max, max_index;
    max = 0;
    max_index = order_key_index;

    forIn (i, order_key_index + 1, nelems) {
        if (order[order_key_index] <= order[i] && search_v[i] > max)
        {
            max = search_v[i];
            max_index = i;
        }
    }

    return max_index;
}

int max_search(int search_v[], unsigned int nelems)
{
    int max, max_index;

    if (nelems < 1) {
        return -1;
    }

    max = 0;
    max_index = 0;

    forIn(search_index, 1, nelems) {
        if (search_v[search_index] > max) {
            max = search_v[search_index];
            max_index = search_index;
        }
    }

    return max_index;
}
