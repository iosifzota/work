#include "libs.h"

#ifndef EOF

#include <stdio.h>

#endif

#include <math.h>

void solve_eqx(int delta, int a, int b)
{
    int sign = 0;
    int x1, x2;

    if (delta < 0) {
    delta *= -1;
    sign = 1;
    }

    double rdelta = sqrt(delta);

    if ((long)rdelta == rdelta) {
        if (!sign) {
            x1 = (-b + rdelta) / 2 / a;
            x2 = (-b - rdelta) / 2 / a;

            printf("x1 = %d;\tx2 = %d\n", x1, x2);
        }
        else {
            printf("x1 = %d + %.2lfi;\tx2 = %d - %.2lfi\n", -b/2/a, rdelta/2/a, -b/2/a, rdelta/2/a);
        }
    }
    else {
        if (!sign) {
        printf("x1 = %d + rad(%d)/2;\tx2 = %d - rad(%d)/2\n", -b/2/a, delta, -b/2/a, delta);
        }
        else {
        printf("x1 = %d + rad(%d)i/2;\tx2 = %d - rad(%d)i/2\n", -b/2/a, delta, -b/2/a, delta);
        }
    }
}
