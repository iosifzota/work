#include "libs.h"

#ifndef EOF
#include <stdio.h>
#endif

#ifndef EXIT_SUCCESS
#include <stdlib.h>
#endif


void readarr(long arr[], int len)
{
     for (int i = 0; i < len; i++) {
         scanf_number(arr+i);
     }
}

void printarr(long arr[], int len)
{
     for (int i = 0; i < len; i++) {
         printf("%ld ", arr[i]);
     }
}

/* Validated read of one long number. */
int scanf_number(long *n)
{
    char number[1000];

    if (EOF == scanf("%s", number)) {
        printf("[Error] scanf_number(): scanf().");
        return SYSERR;
    }
         
    *n = strtol_(number);
    
    /* Validate conversion */
    if (
        1 == *n &&
        numberlen(*n) != strlen_(number) &&
        '1' != *number
        )
    {
        printf ("Please enter a valid integer.\n");

        return INVALID_INPUT;
    }

    return SUCCESS;
}

/* strtol wrapper
* Returns the number string converted to n.
* If conversion "failed", it returns 1.
*/
long strtol_(char *number)
{
    char *err = "";

    /* convert */
    long n = strtol (number, &err, 10);

    /* check */
    if (*err) {
        return INVALID_INPUT;
    }

    return n;
}


/* String length */
int strlen_(char *s)
{
    int nr = 0;
    for(; *s; s++, nr++) { ; }

    return nr;
}

/* Number(long) length */
int numberlen_body(long n, unsigned int len)
{
    for (; n; n /= 10, len++) { ; }
    
    return len;
}
int numberlen(long n)
{
    return numberlen_body(n/10 , 1);
}
