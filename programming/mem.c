#include <stdlib.h>
#include <stdio.h>

#define SUCCESS 0
#define NEWLINE puts("")


void *ec_malloc(size_t size);
void *ec_realloc(void *ptr, size_t size);

#define mm(size, type) (type *) ec_malloc(size * sizeof(type))

#define mem_matrix(matrix, lns, cols, type) ;\
    matrix = mm(lns, type*);\
    for (int i = 0; i < lns; ++i) {\
        matrix[i] = mm(cols, type);\
    }

#define free_matrix(matrix, lns) ;\
    for (int i = 0; i < lns; ++i) {\
        free(matrix[i]);\
    }\
    free(matrix);


void printmat(int **, unsigned int, unsigned int);
void readmat(int **, unsigned int, unsigned int);


int main()
{
    int **mat;

    int lns, cols;

    printf("Lines: ");
    scanf("%d", &lns);

    printf("Collums: ");
    scanf("%d", &cols);


    mem_matrix(mat, lns, cols, int);

    readmat(mat, lns, cols);
    printmat(mat, lns, cols);

    return SUCCESS;
}


void printmat(int **mat, unsigned int lns, unsigned int cols)
{
    NEWLINE;

    for (
        unsigned int ln_index = 0;
        ln_index < lns;
        ++ln_index
        )
    {
        for (
            unsigned int col_index = 0;
            col_index < cols;
            ++col_index
            )
        {
            printf("%d ",mat[ln_index][col_index]);
        }
        NEWLINE;
    }

    NEWLINE;
}



void readmat(int **mat, unsigned int lns, unsigned int cols)
{
    for (
        unsigned int ln_index = 0;
        ln_index < lns;
        ++ln_index
        )
    {
        for (
            unsigned int col_index = 0;
            col_index < cols;
            ++col_index
            )
        {
            scanf("%d", mat[ln_index] + col_index);
        }
    }
}




void *ec_realloc(void *ptr, size_t size)
{
	void *new_ptr;

	new_ptr = realloc(ptr, size);

	if (!new_ptr) {
		perror("[Error] realloc() returned NULL\n");
		exit(-1);
	}

	return new_ptr;
}

void *ec_malloc(size_t size)
{
	return ec_realloc(NULL, size);
}
