#include "libs.h"

#ifndef EOF
#include <stdio.h>
#endif

#ifndef EXIT_SUCCESS
#include <stdlib.h>
#endif

void showMenu();

int main()
{
    int c;

    showMenu();

    unsigned short output = 0;

    while ( (c = getchar()) != EOF ) {


        if (c != '\n') {

            switch (c) {
                // ax^2 + bx + c
                case '1':
                    NEWLINE;
                    test_eqx();
                    NEWLINE;

                    output = 1;
                    break;

                // fibonacci
                case '2':
                    NEWLINE;
                    test_fib();
                    NEWLINE;

                    output = 1;
                    break;

                // cmmdc
                case '3':
                    NEWLINE;
                    test_cmmdc();
                    NEWLINE;

                    output = 1;
                    break;

                // binary
                case '4':
                    NEWLINE;
                    test_bits();
                    NEWLINE;

                    output = 1;
                    break;

                // euclid divs
                case '5':
                    NEWLINE;
                    test_divs();
                    NEWLINE;

                    output = 1;
                    break;

                // digits
                case '6':
                    NEWLINE;
                    test_digits();
                    NEWLINE;

                    output = 1;
                    break;

                // octal
                case '7':
                    NEWLINE;
                    test_octal();
                    NEWLINE;

                    output = 1;
                    break;

                case '0':
                    return 0;

                case 'q':
                    return 0;

                case 'Q':
                    return 0;

                // clear screen & showMenu
                case 'c':
                    output = 0;
                    break;

                default:

                    printf("Invalid option.");
                    output = 0;

                    break;
            }


            if (output == 0) {
                #ifdef windows
                system("cls");
                #endif

                #ifdef linux
                if ( 0 != system("clear")) {
                    printf("[Error] main(): system()\n");
                }
                #endif

                showMenu();
            }

        }
    }
    return 0;
}

void showMenu()
{
    printf("Select option:\
            \n1. ax^2 + bx + c\
            \n2. Fibonacci\
            \n3. Cmmdc\
            \n4. Binary\
            \n5. Divs\
            \n6. Digits\
            \n7. Base 8\
            \n0. Exit");
    NEWLINE;
    NEWLINE;

    printf("$: ");
}
