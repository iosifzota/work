#include "libs.h"

#ifndef EXIT_SUCCESS
#include <stdlib.h>
#endif


long *fib_body(long arr[], int n)
{
    for (int i = 2; i < n; i++) {
        arr[i] = arr[i-1] + arr[i-2];
    }
    arr[n] = -1;

    return arr;
}

long *fib(int n) {
    if (n < 90) {
        long *arr = malloc(sizeof(long) * (n + 1));
        arr[0] = 0;
        arr[1] = 1;
        
        return fib_body(arr, n);
    }
    else {
        return NULL;
    }
}
