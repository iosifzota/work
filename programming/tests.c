#include "libs.h"

#ifndef EOF
#include <stdio.h>
#endif

#ifndef EXIT_SUCCESS
#include <stdlib.h>
#endif

void test_fib()
{
    long n;

    printf("fib: (f1 f2 f3 ... fn)\n");

    printf("n = ");
    if ( 0 != scanf_number(&n)) {
        printf("[Error] test_fib() while reading n");
        return;
    }

    long *arr;

    if ( NULL == (arr = fib(n)) ) {
            printf("[Warning] N is to big. Try again.\n");
            return test_fib();
        }

    for (int i = 0; arr[i] != -1; i++) {
        printf("%ld ", arr[i]);
    }
    NEWLINE;
}



void test_cmmdc()
{
    long a, b;

    printf("(a, b): ");
    if (EOF == scanf("%ld %ld", &a, &b)) {
        printf("[Error] test_cmmdc() while reading a, b");
        return;
    }

    printf("cmmdc(%ld, %ld) = %ld", a, b, cmmdc(a, b));

    NEWLINE;
}

void test_bits() {
    long n; int *bits = NULL;

    printf("n = ");
    if (0 != scanf_number(&n)) {
        printf("[Error] test_bits(): scanf_number()");
        return;
    }

    bits = get_bits(n);

    printf("binary: ");

    for (int i = 0; bits[i] != -1; i++) {
        printf("%d", bits[i]);
    }

    free(bits);

    NEWLINE;

}


void test_divs()
{
     long n;
     unsigned int nrdivs;

     printf("n = ");
     if (0 != scanf_number(&n)) {
         printf("[Error] test_digits(): scanf_number()");
         return;
     }

     long divs[1000];

     nrdivs = get_divisors(n, divs);

     NEWLINE;

     printarr(divs, nrdivs);

}

void test_digits()
{

    long n;

    printf("n = ");
    if (0 != scanf_number(&n)) {
        printf("[Error] test_digits(): scanf_number()");
        return;
    }

    int *digits = get_digits(n);

    for (int i = 0; digits[i] != -1; i++) {
        printf("%d ", digits[i]);
    }
}


void test_octal() {
    long n;

    printf("n = ");
    if (0 != scanf_number(&n)) {
        printf("[Error] test_8base(): scanf_number()");
        return;
    }

    int *digits = to_octal(n);

    int len = 0;
    while (digits[len++] != -1) { ; }

    for (int i = len - 2; i >= 0; i--) {
        printf("%d", digits[i]);
    }
}


void test_eqx()
{
    int a, b, c, delta, x1, x2;

    printf("ax^2 + bx + c\n");

    if (EOF == scanf("%d %d %d", &a, &b, &c)) {
        printf("[Error] test_eqx(): reading coefficients.");
        return;
    }

    delta = b*b + (-4)*a*c;

    if (delta == 0) {
        x1 = x2 = -b / 2 / a;

        printf("x = %d\n", x1);
    }
    else {
        solve_eqx(delta, a, b);
    }

}
