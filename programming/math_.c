#include "libs.h"

#ifndef EXIT_SUCCESS
#include <stdlib.h>
#endif

int *get_digits(long n)
{

    if (n < 0) {
        n *= -1;
    }

    int *digits = malloc( (numberlen(n) + 1) * sizeof(int) );


    unsigned int i = numberlen(n);

    digits[i--] = -1;

    /* If n is 0 */
    if (n == 0) {
        digits[i--] = 0;
    }

    /* Put the numbers in the array
     * from back to start.
     */
    for (; n; n /= 10) {
        digits[i--] = n % 10;
    }


    return digits;
}


int get_divisors(long n, long divisors[])
{
     if ( n == 0 ) {
         return n;
     }

     if ( n < 0 ) {
         n *= (-1);
     }

     int nth_divisor = 0;
     long last = (n > 2) ? n : 2;


     for (
          int candidate = 1;
          candidate < last;
          candidate++
          )
     {
         if ( 0 == n % candidate ) {

             divisors[nth_divisor++] = candidate;

             /* Get the div from the other "half" of n. */
             if ( (n / candidate) != candidate ) {
                 divisors[nth_divisor++] = (n / candidate);
             }

             last = n / candidate;
         }
     }

     return nth_divisor;
}

/* Convert n to octal.
 * Converts n to binary, then octal.
 */
int *to_octal(long n) {

    int *bits = NULL, *digits = NULL, lbit;

    /* Get binary of n */
    bits = get_bits(n);
    lbit = leftmost_bit(n);

    /* Create return vector */
    digits = malloc((lbit + 1) * sizeof(int));

    unsigned int i = 0;

    for (
        int j = lbit-1, digit = 0, counter = 1, p = 1;
        j >= 0;
        j--, counter++
        )
    {

        digit = digit + bits[j]*p;

        if (counter == 3 || (j - 1) < 0) {
            digits[i++] = digit;
            digit = 0;
            counter = 0;
            p = 1;
        }
        else {
            p *= 2;
        }
    }

    digits[i] = -1;

    return digits;
}

/* Returns the leftmost_bit position of n.
 * Treats negatives as positives.
 */
unsigned int leftmost_bit(long n)
{
    unsigned int lbit;
    long mask = (~0) & 1;

    if (n < 0) {
        n *= -1;
    }

    /* Bit by bit, until shifted mask > n */
    for ( lbit = 0; (mask << lbit) <= n; lbit++ ) { ; }

    return lbit;
}

/* Takes a number and returns an array with the bits of said number.
 * Treates negatives as positives.
 */
int *get_bits_body(long n, unsigned int lbit)
{
    long mask = (~0) & 1;
    unsigned int i = 0;

    // allocate memeory for bits + 1 (ENDofNumber signal -1)
    int *bits = malloc( (lbit+1) * sizeof(int) );

    if (lbit == 0) {
        bits[i++] = 0;
    }

    // from the left, mask bit by bit and fill array
    for (int pos = lbit-1; pos >= 0; pos--) {
        if (n & (mask << pos)) {
            bits[i++] = 1;
        }
        else {
            bits[i++] = 0;
        }
    }

    bits[i] = -1;

    return bits;
}
int *get_bits(long n) {

    if (n < 0) {
        n *= -1;
    }

    return get_bits_body(n, leftmost_bit(n));
}


/* Euclid's algorithm */
long cmmdc(long a, long b)
{
    int r = a % b;

    if (r == 0) {
        return b;
    }

    return cmmdc(b, r);
}
