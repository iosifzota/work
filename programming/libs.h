#define SUCCESS (0)
#define SYSERR  (2)
#define INVALID_INPUT (1)

#define NEWLINE printf("\n")


void readarr(long arr[], int len);
void printarr(long arr[], int len);

void solve_eqx(int delta, int a, int b);
void test_eqx();


long *fib(int n);
void test_fib();


long cmmdc(long a, long b);
void test_cmmdc();


int numberlen(long n);
int strlen_(char *s);

long strtol_(char *number);
int scanf_number(long *n);


unsigned int leftmost_bit(long n);

int *get_bits(long n);
void test_bits();


int get_divisors(long n, long divisors[]);
void test_divs();


int *get_digits(long n);
void test_digits();


int *to_octal(long n);
void test_octal();
