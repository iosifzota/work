#include "product.h"

#include <string.h>
#include <iostream>
#include <memory>

const int invalid_productid = -1;

namespace stc {
    Product::Product(const char* str)
    {
        m_id = invalid_productid;
        m_value = 0;
        strcpy(m_name, str);

        std::cout << "[Debug] Constructed: " << m_name << std::endl;
    }

    Product::~Product()
    {
        std::cout << "[Debug] Constructed: " << m_name << std::endl;    
    }
}

namespace dyn {
    Product::Product(const char* str)
    {
        m_id = invalid_productid;
        m_value = 0;

        m_name = new char[strlen(str)];        
        strcpy(m_name, str);

        std::cout << "[Debug] Constructed: " << m_name << std::endl;
    }

    Product::~Product()
    {
        std::cout << "[Debug] Constructed: " << m_name << std::endl;
        delete[] m_name;
    }

    Product::Product(const Product& other)
    {
        m_id = other.m_id;
        m_value = other.m_value;

        m_name = new char[strlen(other.m_name)];
        strcpy(m_name, other.m_name);
    }
}

