#include "product.h"
#include <memory>

int main()
{
    stc::Product product("Static table");

    std::shared_ptr<stc::Product> prod_ptr = std::make_shared<stc::Product>("Dynamic table");

    dyn::Product pro_d("Dynamic dyn table");
    
    return 0;
}
