#ifndef __PRODUCT_H
#define __PRODUCT_H

namespace stc {
    class Product
    {
    public:
        Product(const char* str = "undefined");
        ~Product();
    private:
        int m_id;
        float m_value;
        char m_name[100];
    };
}

namespace dyn {
    class Product
    {
    public:
        Product(const char* str = "undefined");
        Product(const Product& other);
        ~Product();
        // [TODO] Product operator = (const Product& other)
    private:
        int m_id;
        float m_value;
        char* m_name;
    };
}


#endif
