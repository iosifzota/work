;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

((nil
  (cmake-ide-dir . "build"))
 (c++-mode
  (flycheck-gcc-language-standard . "c++11")))

