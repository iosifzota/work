#ifndef __money_hh
#define __money_hh

#include <ostream>

enum Currency
{
    invalid_currency = -1,
    USD,
    EUR,
    RON,
    ROL,
    HUF,
    GBP,
    AUD
};

class Money
{
public:
    Money();

    static const double invalid_amount;

    static const char* currency_value(Currency);


    /* Return attribute value. */
    double amount() const;
    Currency currency() const;

    /* Change atribute value. */
    void change_amount(const double& invalid_amount);
    void change_currency(const Currency& value);

private:
    double m_amount;
    Currency m_currency;
};




#endif
