#include "money.hh"

#include <string.h>

const double Money::invalid_amount = 0;

Money::Money()
{
    m_amount = invalid_amount;
    m_currency = invalid_currency;
}


const char* Money::currency_value(Currency currency)
{
    if (currency == USD) {
        return "$";
    }
    else if (currency == RON) {
        return "lei";
    }

    return nullptr;
}

double Money::amount() const
{
    return m_amount;
}

Currency Money::currency() const
{
    return m_currency;
}

void Money::change_amount(const double& value)
{
    m_amount = value;
}

void Money::change_currency(const Currency& value)
{
    m_currency = value;
}
