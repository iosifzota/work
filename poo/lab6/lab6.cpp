#include <iostream>

#include "money.hh"

int main()
{
    std::cout << Money::invalid_amount << ' ';

    std::cout << Money::currency_value(Currency::RON) << '\n';


    Money tst;

    std::cout << tst.amount() << ' ' << tst.currency() << '\n';
    return 0;
}
