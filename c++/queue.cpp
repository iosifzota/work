#ifndef __QUEUE_CPP
#define __QUEUE_CPP

#include "queue.h"

#include <memory>
#include <iostream>

/* Creates a node, sets data field and returns shared_ptr to the node. */
template<typename T>
std::shared_ptr<typename queue<T>::Node> queue<T>::create_node(T data)
{
    std::shared_ptr<queue<T>::Node> new_node;

    new_node = std::make_shared<queue<T>::Node>();
    new_node->data = data;
    new_node->next = nullptr;
     
    return new_node;
}

template<typename T>
void queue<T>::pop()
{
    if (this->empty()) {
        std::cout << "(null)" << '\n';
        return;
    }

    this->head = this->head->next;
}

template<typename T>
void queue<T>::push(T data)
{
    std::shared_ptr<queue<T>::Node> node;

    node = create_node(data);

    if (this->empty()) {
        this->head = this->tail = node;
        return;
    }

    this->tail->next = node;
    this->tail = node;
}

template<typename T>
T queue<T>::front()
{
    if (this->empty()) {
        std::cout << "(null)" << '\n';
        return 0;
    }

    return this->head->data;
}

template<typename T>
bool queue<T>::empty()
{
    return (this->head == nullptr) ? true : false;
}

template<typename T>
void queue<T>::print()
{
    std::shared_ptr<queue<T>::Node> current_node;

    current_node = this->head;

    while(current_node != nullptr) {
        std::cout << current_node->data << '\n';
        current_node = current_node->next;
    }
}

#endif
