#ifndef __QUEUE_H
#define __QUEUE_H

#include <memory>

template<typename T>
class queue
{
public:
    struct Node {
        T data;
        std::shared_ptr<Node> next;
    };

    std::shared_ptr<Node> create_node(T);

    void pop();
    void push(T);
    T front();

    void print();

    bool empty();


private:
    std::shared_ptr<Node> head;
    std::shared_ptr<Node> tail;
};


#include "queue.cpp"

#endif
