#ifndef __STACK_H
#define __STACK_H

#include <memory>

template<typename T>
class stack
{
public:
    struct Node {
        T data;
        std::shared_ptr<Node> next;
    };

    std::shared_ptr<Node> create_node(T);

    void pop();
    void push(T);
    T top();

    void print();

    bool empty();


private:
    std::shared_ptr<Node> head;
};


#include "stack.cpp"

#endif
