#ifndef __MATRIX_CPP
#define __MATRIX_CPP

#include <iostream>
#include <memory>
#include <cstdlib>
#include "matrix.h"

template<typename T>
matrix<T>::matrix(unsigned int m, unsigned int n)
{
    if (m == 0 || n == 0) {
        std::cerr << "Cannot construct empty matrix. \n";
    }
    this->nrows = m;
    this->ncols = n;
    data = new T*[nrows];
    data[0] = new T[nrows * ncols];

    for (unsigned int i = 1; i < nrows; ++i) {
        data[i] = data[i - 1] + ncols;
    }
}

template<typename T>
unsigned int matrix<T>::rows()
{
    return this->nrows;
}

template<typename T>
unsigned int matrix<T>::cols()
{
    return this->ncols;
}

template<typename T>
void matrix<T>::read(std::istream& input)
{
    for (unsigned int i = 0; i < this->rows(); ++i) {
        for (unsigned int j = 0; j < this->cols(); ++j) {
            if (input >> this->data[i][j]) { ; }
            else  {
                std::cout << "Input error. \n";
                exit(-1);
            }
        }
    }
}

template<typename T>
void matrix<T>::read() { this->read(std::cin); }

template<typename T>
void matrix<T>::print(std::ostream& output)
{
    for (unsigned int i = 0; i < this->rows(); ++i) {
        for (unsigned int j = 0; j < this->cols(); ++j) {
            output << this->data[i][j] << " ";
        }
        output << std::endl;
    }
}

template<typename T>
void matrix<T>::print() { this->print(std::cout); }

template<typename T>
T& matrix<T>::operator()(const unsigned& row, const unsigned& col)
{
    if (row >= this->rows() || col >= this->cols()) {
        std::cerr << "[Error] Trying to access array out of bounds: @" << this << std::endl;
        exit(-1);                                                                          
    }
    return this->data[row][col];
}

template<typename T>
T& matrix<T>::operator()(const unsigned& row)
{
    if (row >= this->rows()) {
        std::cerr << "[Error] Trying to access array out of bounds: @" << this << std::endl;
        exit(-1);                                                                          
    }
    return this->data[row];
}

template<typename T>
matrix<T>::~matrix()
{
    delete this->data[0];
    delete this->data;
}

#endif
