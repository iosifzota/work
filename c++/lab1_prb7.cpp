#include <iostream>
#include <fstream>
#include <memory>
#include <vector>
#include <cstdlib>

#include "matrix.h"

template<typename T>
std::vector<T> sums(matrix<T>& mat);

int main()
{
    unsigned int rows = 0, cols = 0;

    std::fstream input;
    input.open("main.txt", std::ios::in);

    if (input >> rows) {
        if (input >> cols) { ; }
        else  {
            std::cout << "Input error. \n";
            exit(-1);
        }
    }
    else  {
        std::cout << "Input error. \n";
        exit(-1);
    }

    matrix<int> mat(rows, cols);

    mat.read(input);
    input.close(); // Close stream.

    std::vector<int> result = sums(mat);

    /* Print out sums */
    for (auto& row : result) {
        std::cout << row << " ";
    }
    std::cout << std::endl;

    return 0;
}

template<typename T>
std::vector<T> sums(matrix<T>& mat)
{
    T sum = 0;
    std::vector<T> sums;

    for (unsigned row = 0, col = 0; row < mat.rows() - row; col = ++row, sum = 0) {

        sum += mat(row, col);

        if (mat.rows()-1 - row > row) {
            sum += mat(mat.rows() - row - 1, mat.rows()-1 - col); // bottom, right
            sum += mat(row, mat.rows() - col - 1);                // top, right
            sum += mat(mat.rows() - row - 1, col);                // bottom, left

            for (col = row + 1; col < mat.rows() - row - 1; ++col) {
                sum += mat(row, col) + mat(mat.rows() - row - 1, mat.rows() - col - 1);
                sum += mat(col, row) + mat(mat.rows() - col - 1, mat.rows() - row - 1);
            }
        }

        sums.push_back(sum);
    }
    return sums;
}
