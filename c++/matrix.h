#ifndef __MATRIX_H
#define __MATRIX_H

#include <fstream>

template<typename T> class matrix {
private:
    T** data;
    unsigned int nrows, ncols;

public:

    matrix(unsigned int m, unsigned int n);
    ~matrix();

    unsigned int rows();
    unsigned int cols();

    void read(std::istream& input); 
    void read();

    void print(std::ostream& output); 
    void print();

    T& operator()(const unsigned& row, const unsigned& col); 
    T& operator()(const unsigned& row); 
};

#include "matrix.cpp"

#endif
