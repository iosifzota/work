#include "btree.h"

#include <fstream>
#include <iostream>
#include <memory>
#include <sstream>
#include <vector>

#define N_LINES(times) \
    for(int i = 0; i < times; ++i) std::cout << '\n';

/* Proccess data from istream mutating the istream. */
bool get_data_fn(std::istream&, int&);

/* Proccess data from istream just by peeking. */
bool peek_data_fn(std::istream&, int&);

/* Search a vector for key, backwards. */
bool search_vector(std::vector<int>, int);

/* Print data. */
void print_node(int data) { std::cout << data << ' '; }

int main()
{
    btree<int> btr;
    std::string str;
    std::ifstream input;

    input.open("test.txt");

    std::getline(input, str);
    std::istringstream preorder(str);

    std::getline(input, str);
    std::istringstream inorder(str);

    input.close();

    btr.read_traversal(preorder, inorder, get_data_fn, peek_data_fn, search_vector);
    btr.breadth_traversal(print_node); N_LINES(3);

    return 0;
}

bool search_vector(std::vector<int> v, int key)
{
    for (std::vector<int>::reverse_iterator itr = v.rbegin(); itr != v.rend(); ++itr) {
        if (*itr == key) {
            return true;
        }
    }

    return false;
}

bool get_data_fn(std::istream& input, int& data)
{
    if (input >> data) {
        return true;
    }

    return false;
}

bool peek_data_fn(std::istream& input, int& data)
{
    std::string token;

    if (input >> token) {
        std::stringstream convert(token);
        convert >> data;

        input.clear();
        input.seekg(-(int)(token.length()), std::ios::cur);
        return true;
    }

    return false;
}
