#ifndef __STACK_CPP
#define __STACK_CPP

#include "stack.h"

#include <memory>
#include <iostream>

/* Creates a node, sets data field and returns shared_ptr to the node. */
template<typename T>
std::shared_ptr<typename stack<T>::Node> stack<T>::create_node(T data)
{
    std::shared_ptr<stack<T>::Node> new_node;

    new_node = std::make_shared<stack<T>::Node>();
    new_node->data = data;
    new_node->next = nullptr;
     
    return new_node;
}

template<typename T>
void stack<T>::pop()
{
    if (this->empty()) {
        std::cout << "(null)" << '\n';
        return;
    }

    this->head = this->head->next;
}

template<typename T>
void stack<T>::push(T data)
{
    std::shared_ptr<stack<T>::Node> node;

    node = create_node(data);
    node->next = this->head;
    this->head = node;
}

template<typename T>
T stack<T>::top()
{
    if (this->empty()) {
        std::cout << "(null)" << '\n';
        return 0;
    }

    return this->head->data;
}

template<typename T>
bool stack<T>::empty()
{
    return (this->head == nullptr) ? true : false;
}

template<typename T>
void stack<T>::print()
{
    std::shared_ptr<stack<T>::Node> current_node;

    current_node = this->head;

    while(current_node != nullptr) {
        std::cout << current_node->data << '\n';
        current_node = current_node->next;
    }
}


#endif
