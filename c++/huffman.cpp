#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <memory>
#include <queue>
#include <functional>

template<typename T>
void swap(std::vector<T>&, unsigned, unsigned);

template<typename T>
void v_print(std::ostream&, const std::vector<T>&);


class huff_tree
{
public:
    struct Node {
        unsigned freq, key;
        char c;
        std::shared_ptr<huff_tree::Node> left, right;
    };

    std::shared_ptr<Node> create_node(unsigned freq, char c);

    void read_file(const char* file_path);

    void insert(unsigned freq, char c);
    void insert(std::shared_ptr<Node>);

    void create_tree();
    void create_tree_from_file(const char *file_path);

    void breadth_traversal(std::function<void(std::shared_ptr<Node>)> action);
    
    std::vector<int> search(char);
    bool search_body(std::shared_ptr<Node>, unsigned, char, std::vector<int>&);

    void print();
    std::shared_ptr<huff_tree::Node>& operator [] (const unsigned index);
    
private:
    std::vector< std::shared_ptr<Node> > data;
};

void huff_tree::create_tree_from_file(const char *file_path)
{
    this->read_file(file_path);
    this->print();
    this->create_tree();
}

void err_input();
void err_bounds();

int main()
{
    huff_tree hf;

    hf.create_tree_from_file("huff.txt");

    std::cout << '\n';
    hf.breadth_traversal([](std::shared_ptr<huff_tree::Node> node) {
            std::cout << node->c << "[" << node->freq << "] ";
        });
    std::cout << '\n';

    std::vector<int> res;
    res = hf.search('c');


    std::cout << '\n';
    v_print(std::cout, res);
    std::cout << '\n';

    return 0;
}

std::vector<int> huff_tree::search(char c)
{
    std::vector<int> code;

    if (search_body(this->data[0], 0, c, code)) {
        std::cout << "Found.\n";
    }
    else {
        std::cout << "Not found.\n";
    }

    return code;
}

bool huff_tree::search_body(
    std::shared_ptr<huff_tree::Node> current_node,
    unsigned current_node_sign, char c, std::vector<int>& code)
{
    if (current_node == nullptr) {
        return false;
    }

    code.push_back(current_node_sign);

    if (current_node->c == c) {
        return true;
    }

    if(this->search_body(current_node->left, 0, c, code)) {
        return true;
    }

    if(this->search_body(current_node->right, 1, c, code)) {
        return true;
    }

    code.pop_back();

    return false;
}

void huff_tree::breadth_traversal(
    std::function<void(std::shared_ptr<huff_tree::Node>)> visit
    )
{
    std::shared_ptr<huff_tree::Node> node;
    std::queue< std::shared_ptr<huff_tree::Node> > fringe;
    fringe.push(this->data[0]);
     
    do {
        node = fringe.front();
        fringe.pop();
     
        visit(node);
     
        if (node->left != nullptr) {
            fringe.push(node->left);
        }
     
        if (node->right != nullptr) {
            fringe.push(node->right);
        }
     
    } while (!fringe.empty());
}


void huff_tree::create_tree()
{
    std::shared_ptr<huff_tree::Node> left, right, new_parent;

    while (this->data.size() > 1) {
        /* Get right child. */
        right = this->data[this->data.size() - 1];
        this->data.pop_back();

        /* Get left child. */
        left = this->data[this->data.size() - 1];
        this->data.pop_back();

        /* Create new parent. */
        new_parent = this->create_node(right->freq + left->freq, 0);
        new_parent->left = left;
        new_parent->right = right;

        /* Insert the new parent. */
        this->insert(new_parent);
    }
}


/* Ordered insert */
void huff_tree::insert(unsigned int freq, char c)
{
    std::shared_ptr<huff_tree::Node> node;
    node = this->create_node(freq, c);

    this->insert(node);
}

void huff_tree::insert(std::shared_ptr<huff_tree::Node> node)
{
    int index;

    this->data.push_back(node);

    /* Sort in decreasing order. */
    for (
        index = this->data.size() - 2;
        index >= 0 && this->data[index]->key < node->key;
        --index
        )
    {
        this->data[index + 1] = this->data[index];
    }

    this->data[++index] = node;
}






void huff_tree::read_file(const char* file_path)
{
    std::string str;
    std::ifstream in(file_path);

    /* Frequencies */
    std::getline(in, str);
    std::istringstream freq(str);

    /* Characters. */
    std::getline(in, str);
    std::istringstream chars(str);

    in.close();

    unsigned fq;
    char c;

    while (freq >> fq && chars >> c) {
        this->insert(fq, c);
    }
}
















std::shared_ptr<huff_tree::Node>& huff_tree::operator[](const unsigned int index)
{
    if (index > this->data.size()) {
        err_bounds();
        std::cerr << "\tAttempted to access index: " << index
                  << ". The last available index is " << this->data.size() - 1 << "!\n";
        exit(-1);
    }

    return this->data[index];
}

std::shared_ptr<huff_tree::Node> huff_tree::create_node(unsigned freq, char c)
{
    std::shared_ptr<huff_tree::Node> node;

    node = std::make_shared<huff_tree::Node>();
    node->freq = freq;
    node->c = c;
    node->key = freq * 1000 + c;

    node->left = node->right = nullptr;

    return node;
}




void huff_tree::print()
{
    for(auto itr : this->data) {
        std::cout << itr->c << "[" << itr->freq << "] ";
    }
    std::cout << std::endl;
}


void err_input()
{
    std::cerr << "[Error] Not enough input.\n";
}

void err_bounds()
{
    std::cerr << "[Error] Trying to access vector out of bounds.\n";
}

/* Vector utils */
template<typename T>
void v_print(std::ostream& out, const std::vector<T>& v)
{
    for (auto itr : v) {
        out << itr << ' ';
    }
    out << '\n';
}


template<typename T>
void swap(std::vector<T>& v, unsigned i1, unsigned i2)
{
    if (i1 < 0 || i2 < 0 || i1 >= v.size() || i2 >= v.size()) {
        err_bounds();
    }

    T aux = v[i1];
    v[i1] = v[i2];
    v[i2] = aux;
}
