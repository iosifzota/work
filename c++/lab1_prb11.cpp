#include "btree.h"

#include <fstream>
#include <iostream>
#include <memory>
#include <sstream>
#include <vector>

#define N_LINES(times) \
    for(int i = 0; i < times; ++i) std::cout << '\n';

int get_data_fn(std::istream&, int&);

/* Print data. */
void print_node(int data) { std::cout << data << ' '; }

int main()
{
    btree<int> btr;

    std::ifstream input;

    input.open("prb11.txt");
    btr.read_preorder(input, get_data_fn); N_LINES(2);
    input.close();

    btr.breadth_traversal(print_node); N_LINES(1);

    std::cout << "Depth: " << btr.preorder_traversal([](int x) { return x; });
        

    return 0;
}


int get_data_fn(std::istream& input, int& data)
{
    std::string token;

    if (input >> token) {
        std::cout << token << ' ';

        if (token[0] == ')') {
            return 0;
        }

        if (token[0] == '(') {
            return 2;
        }

        std::stringstream convert(token);
        convert >> data;

        return 1;
    }

    std::cout << "EOF" << '\n';

    return 0;
}
