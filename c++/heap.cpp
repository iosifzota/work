#include <iostream>
#include <vector>

/*
  Heap:
    Prerequisites:
        // (Maybe not, because i'll sill need comp_fn.) Struct Key { size_t operator () (const T key) const {  [x]aaa, x = freq, aaa = ascii }  }
        sturct heap_fn: bool operator () (const T key) const { ... }


    State:
        container:  std::vector<T>
        method: stif_down(index);


 */

struct my_heap {
    bool operator () (const int key1, const int key2) const {
        return key1 >= key2;
    }
};

template<typename T, class heap_fn>
class heap
{
private:
    /* The actual nodes */
    std::vector<T> nodes;
public:

    void cmp(T key1, T key2) {
        heap_fn cmp_fn;

        if (cmp_fn(key1, key2)) {
            std::cout << "First >=\n";
        }
        else {
            std::cout << "Second >\n";
        }
    }

};

int main()
{
    heap<int, my_heap> test;

    test.cmp(7, 4);

    return 0;
}
