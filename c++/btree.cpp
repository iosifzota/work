#ifndef __BTREE_CPP
#define __BTREE_CPP
     
#include "btree.h"
#include "stack.h"
#include "queue.h"
     
#include <fstream>
#include <iostream>
#include <functional>
#include <memory>
#include <queue>
#include <stack>
#include <cstdlib>
#include <vector>


     

/*
  ===============================
  LAB1 - prb. 12
  ===============================
*/
/*
  get_data_fn must process the input (mutate) and return false for EOF.
  peek_data_fn must process the input (peek) and return false for EOF.
 */
template<typename T>
void btree<T>::read_traversal(
    std::istream& preorder, std::istream& inorder,
    std::function<bool(std::istream&, T&)> get_data_fn,
    std::function<bool(std::istream&, T&)> peek_data_fn,
    std::function<bool(std::vector<T>, T)> search_vector)
{
    std::vector<T> prev_nodes;
    this->root = this->read_traversal_body(
        preorder, inorder,
        get_data_fn, peek_data_fn,
        search_vector, prev_nodes);
}

template<typename T>
std::shared_ptr<typename btree<T>::Node> btree<T>::read_traversal_body(
    std::istream& preorder, std::istream& inorder,
    std::function<bool(std::istream&, T&)> get_data_fn,
    std::function<bool(std::istream&, T&)> peek_data_fn,
    std::function<bool(std::vector<T>, T)> search_vector,
    std::vector<T>& prev_data)
{
    T data;
     
    if (get_data_fn(preorder, data) == false) {
        return nullptr;
    }
     
    std::shared_ptr<btree<T>::Node> node;
     
    node = this->create_node(data);
    node->left = node->right = nullptr;
     
    prev_data.push_back(node->data);
     
    /* Check if there's more data. */
    if (peek_data_fn(inorder, data) == false) {
        return node;
    }
     
    /* Resolve left child. */
    if (node->data != data) {
        node->left = this->read_traversal_body(
            preorder, inorder,
            get_data_fn, peek_data_fn,
            search_vector, prev_data);
    }
    else {
        if (get_data_fn(inorder, data) == false) {
            std::cout << "WEIRD! Input error." << '\n';
        }
    }
     
    /* Check if there's more data. */
    if (peek_data_fn(inorder, data) == false) {
        return node;
    }
     
    /* Resolve right child. */
    if (search_vector(prev_data, data) == false) {
        node->right = this->read_traversal_body(
            preorder, inorder,
            get_data_fn, peek_data_fn,
            search_vector, prev_data);
    }
    else {
        if (get_data_fn(inorder, data) == false) {
            std::cout << "WEIRD! Input error." << '\n';
        }
    }
     
    return node;
}
 /*
  ===============================
  LAB1 - End of prb. 12
  ===============================
*/    
     
     
     
     

     
     
     

     
     
/*
  ===============================
  LAB1 - prb. 11
  ===============================
*/
/* Read tree.
 Tree format:

 ( 0 ( 1 ( 3 ( 7 ) ( 8 ) ) ( 4 ( 9 ) ( 10 ) ) ) ( 2 ( 5 ( 11 ) ( 12 ) ) ( 6 ( 13 ) ( 14 ) ) ) )
  
 =>               0
        1                   2
   3        4         5           6
 7   8    9   10   11   12     13   14

*/
     
/* Creates a tree from a tree "expression".
 * get_data_fn must process the input and return 0 for ")" or EOF, 1 for data, 2 for ")".
 */
template<typename T>
void btree<T>::read_preorder(
    std::istream& input,
    std::function<int(std::istream&, T&)> get_data_fn)
{
    this->root = this->read_preorder_body(input, get_data_fn);
}

template<typename T>
std::shared_ptr<typename btree<T>::Node>
btree<T>::read_preorder_body(
    std::istream& input,
    std::function<int(std::istream&, T&)> get_data_fn)
{
    T data;
    int status = 0;

    /* Skip initial (. */
    while ((status = get_data_fn(input, data)) == 2) { ; }

    /* If ) or no more input. */
    if (!status) {
        return nullptr;
    }
     
    std::shared_ptr<btree<T>::Node> node;
     
    /* Wrap data. */
    node = this->create_node(data); 
    node->left = node->right = nullptr;


    /* Check if node is a terminal node. */
    if (!(status = get_data_fn(input, data))) {
        return node;
    }

    /* [DEBUG] */
    if (status == 1) {
        std::cout << "WEIRD! Should not have read data value. \n";
        exit(-1);
    }
     
    /* Resolve left and right child. */
    node->left = this->read_preorder_body(input, get_data_fn);
    node->right = this->read_preorder_body(input, get_data_fn);

    /* Check if node ends properly. */
    if ((status = get_data_fn(input, data))) {
        std::cout << "Input is not properly formated!! \n";
        exit(-1);
    }
     
    return node;
}


/* Traverse tree in preorder, applying an =action= to each node data
 *    and returns the MAX_DEPTH.
 */
template<typename T>
int btree<T>::preorder_traversal(
    std::function<void(T& data)> action)
{
    if (this->root == nullptr) {
        std::cout << "(null)" << '\n';
        return -1;
    }
     
    std::shared_ptr<btree<T>::Node> current_node;
    stack< std::shared_ptr<btree<T>::Node> > fringe;
     
    fringe.push(this->root);
    this->root->depth = 0;
     
    int max_depth = 0;
     
    do {
        current_node = fringe.top();
        fringe.pop();
     
        action(current_node->data);
     
        if (current_node->depth > max_depth) {
            max_depth = current_node->depth;
        }
     
        if (current_node->right != nullptr) {
            current_node->right->depth = current_node->depth + 1;
            fringe.push(current_node->right);
        }
     
        if (current_node->left != nullptr) {
            current_node->left->depth = current_node->depth + 1;
            fringe.push(current_node->left);
        }
     
    } while (!fringe.empty());
     
    return max_depth;
}
/*
  ===============================
  LAB1 - End of prb. 11
  ===============================
 */



/* Creates a node, sets data field and returns shared_ptr to the node. */
template<typename T>
std::shared_ptr<typename btree<T>::Node> btree<T>::create_node(T data)
{
    std::shared_ptr<btree<T>::Node> new_node = std::make_shared<btree<T>::Node>();
     
    new_node->data = data;
     
    return new_node;
}

/* Breadth traversal, applying =action= to each node data. */
template<typename T>
std::shared_ptr<typename btree<T>::Node>
btree<T>::breadth_traversal(std::function<void(T& data)> action)
{
    if (this->root == nullptr) {
        std::cout << "(null)" << '\n';
        return nullptr;
    }
     
    std::shared_ptr<btree<T>::Node> current_node;
    queue< std::shared_ptr<btree<T>::Node> > fringe;
    fringe.push(this->root);
     
    do {
        current_node = fringe.front();
        fringe.pop();
     
        action(current_node->data);
     
        if (current_node->left != nullptr) {
            fringe.push(current_node->left);
        }
     
        if (current_node->right != nullptr) {
            fringe.push(current_node->right);
        }
     
    } while (!fringe.empty());
     
    return current_node;
}
     
#endif
