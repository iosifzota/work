#include <iostream>
#include <functional>


struct Hash {
    int operator() (const int& value) const {
        std::cout << value << '\n';
        return value;
    }

};


template<typename T, typename Tkey>
class Test
{
private:

public:
    std::function<Tkey(T)> get_key;
    std::function<int(T, T)> cmp;

    Test (std::function<Tkey(T)> _get_key, std::function<int(T, T)> _cmp)
    {
        get_key = _get_key;
        cmp = _cmp;
    }

    void print(int value) {
        std::cout << value << '\n';
    }

};


int main()
{
    Test<int, int> t([](int x) { return x; },  [](int x, int y) { return x + y; });

    std::cout << t.cmp(2, 3);

    return 0;
}
