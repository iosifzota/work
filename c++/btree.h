#ifndef __BTREE_H
#define __BTREE_H
 
#include <fstream>
#include <functional>
#include <vector>
#include <memory>
 
template<typename T>
class btree
{
public:
    struct Node {
        T data;
        int depth;
        std::shared_ptr<Node> left, right;
    };
 
    /* Create node, fill in data and return. */
    std::shared_ptr<Node> create_node(T data);
 

    /* Create a tree from preorder and inorder traversals. */
    std::shared_ptr<Node> read_traversal_body(
        std::istream&, std::istream&,
        std::function<bool(std::istream&, T&)>,
        std::function<bool(std::istream&, T&)>,
        std::function<bool(std::vector<T>, T)>,
        std::vector<T>&);
 
    void read_traversal(
        std::istream&, std::istream&,
        std::function<bool(std::istream&, T&)>,
        std::function<bool(std::istream&, T&)>,
        std::function<bool(std::vector<T>, T)>
        );
 
    /* Read a tree expression. */
    std::shared_ptr<Node> read_preorder_body(std::istream&, std::function<int(std::istream&, T&)>);
    void read_preorder(std::istream&, std::function<int(std::istream&, T&)>);
 
    /* Preorder traversal. Returns depth. */
    int preorder_traversal(std::function<void(T& data)>);
 
    /* Breadth traversal. Return ptr to the last node. */
    std::shared_ptr<Node> breadth_traversal(std::function<void(T&)>);
 
private:
    std::shared_ptr<Node> root;
};
 
#include "btree.cpp"
 
#endif
